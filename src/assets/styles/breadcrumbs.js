export default `
  /* for breadcrumbs*/
  .breadcrumbs {
    margin-bottom: 1.3125rem;
  }

  ul.breadcrumb {
    list-style: none;
  }

  ul.breadcrumb li {
    display: inline;
    font-size: 0.625rem;
  }

  ul.breadcrumb li+li:before {
    padding: 0 .25rem;
    color: #0099FF;
    content: "/";
  }

  ul.breadcrumb li a {
    color: #A8DCFF;
    text-decoration: none;
  }

  @media (min-width: 1290px) {
    .breadcrumbs,
    .c-select-order-type {
      padding-left: 1rem;
    }
  }
`;