export default `
  .label-and-select-dropdown {
    display: flex;
    align-items: center;
    position: relative;
  }

  .msg-type-dropdown-opts {
    display: none;
    position: absolute;
    width: 14rem;
    background: #F5F5F5;
    padding: 1rem;
    top: 2rem;
    margin-left: -1rem;
  }

  .msg-type-dropdown-opts.show {
    display: block;
    box-shadow: 0px 0px 7px rgb(0 0 0 / 35%);
  }

  .msg-type-dropdown-opts li {
    padding: .5rem 0;
  }

  .msg-type-dropdown-opts li button {
    font-size: 1rem;
    width: 100%;
    text-align: left;
  }

  .edit-selection label {
    font-size: 1.125rem;
    color: #000;
  }

  .edit-selection-step2 {
    display: none;
  }

  .edit-selection-step2.show {
    display: block;
  }

  .edit-selection select {
    max-width: 12.5rem;
    padding-left: 1rem;
  }
`;