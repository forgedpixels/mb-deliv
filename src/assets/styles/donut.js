export default `
.donut-metric {
  position: relative;
  justify-content: center;
  display: flex;
  flex-wrap: wrap;
}

.donut-chart {
  display: flex;
  border-radius: 50%;
  width: 4.375rem;
  height: 4.375rem;
  align-items: center;
  justify-content: center;
}

.donut-chart span {
  font-weight: bold;
  font-size: 1.5rem;
}

.donut-chart svg {
  position: absolute;
  width: 4.375rem;
  height: 4.375rem;
}
@media (min-width: 1290px) {
  .circle-chart__circle {
    animation: circle-chart-fill 1s reverse; /* 1 */ 
    transform: rotate(-90deg); /* 2, 3 */
    transform-origin: center; /* 4 */
  }

  .circle-chart__info {
    animation: circle-chart-appear 1s forwards;
    opacity: 0;
    transform: translateY(0.3em);
  }
}

  
  @keyframes circle-chart-fill {
    to { stroke-dasharray: 0 100; }
  }
  
  @keyframes circle-chart-appear {
    to {
      opacity: 1;
      transform: translateY(0);
    }
  }
`;