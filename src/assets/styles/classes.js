export default `
.page {
  width: 100%;
}

.page > header {
  position: relative;
  width: 100%;
  z-index: 3;
}

@media (min-width: 1290px) {
  .page > header {
    height: 7rem;
  }
}

.font-light {
  font-weight: 300;
}

.select-dropdown-absolute {
  position: absolute;
  z-index: 1;
  margin-top: .5rem;
  display: none;
}

.pos-abs-all-right {
  right: 0;
}

.pos-abs-all-left {
  left: 0;
}

.select-dropdown-absolute.show {
  display: block;
}

.select-dropdown-styled {
  background: #F5F5F5;
  padding: .75rem 1.5rem;
  color: #000;
}

.select-dropdown-styled li button,
.select-dropdown-styled li.selected {
  padding: .5rem 0;
  cursor: pointer;
}

.select-dropdown-styled-unfixed {
  box-shadow: 0px 0px 7px rgba(0, 0, 0, 0.35);
}

.select-dropdown-styled:before {
  position: relative;
  display: block;
  content: ' ';
  width: 1.9rem;
  height: .2rem;
  margin: 0 auto;
  background: #E5E5E5;
}

.select-dropdown-styled-unfixed:before {
  display: none;
}

@media (min-width: 1290px) {
  .page > header {
      position: relative;
  }

  .menu-box-shadow-on-lg {
    box-shadow: 0px 0px 7px rgba(0, 0, 0, 0.35);
  }
  .select-dropdown-styled.open {
    position: absolute;
    bottom: auto;
    top: 2.5rem;
    width: 16rem;
    left: auto;
  }

  .select-dropdown-styled.open:before {
    display: none;
  }
}

/* reusable classes across components */
.component-bg {
  background: rgba(4,28,44, .75);
}

.clipboard-bg {
  background: rgba(4,28,44, .75) url(src/assets/images/icons/clipboard.svg) 15px 25px/50px no-repeat;
}

.alerts-bg {
  background: rgba(4,28,44, .75) url(src/assets/images/icons/alerts.svg) 4px 10px/50px no-repeat;
}

.news-bg {
  background: rgba(4,28,44, .75) url(src/assets/images/icons/newspaper.svg) 10px 15px/50px no-repeat;
}

.mail-bg {
  background: rgba(4,28,44, .75) url(src/assets/images/icons/mail.svg) 15px 25px/50px no-repeat;
}

.numbers-bg {
  background: rgba(4,28,44, .75) url(src/assets/images/icons/numbers.svg) 15px 25px/50px no-repeat;
}

.fav-priority-bg {
  background: rgba(4,28,44, .75) url(src/assets/images/icons/star-priority.svg) 6px 10px/45px no-repeat;
}

@media (min-width: 1290px) {
  .fav-priority-bg {
    background-size: 30px;
    background-position: 8px 12px;
  }
}

.component-header {
  font-size: 1.5rem;
  font-weight: bold;
  margin-bottom: 1rem;
}

.content-max-and-center {
  max-width: 1290px;
  margin: 0 auto;
}

.content-to-ends {
  display: flex;
  justify-content: space-between;
}

.content-to-bottom {
  width: 100%;
  position: absolute;
  bottom: 1.5rem;
  left: 0;
}

.content-center-vert {
  align-items: center;
}

.content-base-vert {
  align-items: baseline;
}

.content-base-top {
  align-items: start;
}

.header-view-all-link {
  text-decoration: underline;
  color: #fff;
  font-size: 0.75rem;
}

.sub-txt {
  display: block;
  font-weight: 300;
}

.bright-blue {
  color: #0099FF;
}

.text-and-chevron-label > span:first-child {
  margin-right: .5rem;
}

.text-and-chevron {
  display: inline-block;
}

.text-and-chevron > svg {
  margin-left: .5rem;
}

.text-elipsis-3-line {
  line-height: 1.4rem;
  display: -moz-box;
  display: -webkit-box;
  -webkit-line-clamp: 3;
  -moz-line-clamp: 3;
  line-clamp: 3;
  -webkit-box-orient: vertical;
  -moz-box-orient: vertical;
  box-orient: vertical;
  overflow: hidden;
}

.txt-bold { font-weight: bold; }

.btn {
  background: none;
  border: none;
  outline: none;
  display: inline-block;
}

.arrow-link {
  display: flex;
  color: #fff;
  align-items: center;
}

.arrow-link svg {
  display: inline-block;
  margin-left: 0.625rem;
}

.primary-cta-btn {
  background: #0099FF;
  color: #fff;
  text-transform: uppercase;
  border: none;
  width: 100%;
  letter-spacing: 2px;
}

.cancel-btn {
  background: transparent;
  border: none;
  width: 100%;
  text-decoration: underline;
  font-size: .75rem;
}

.outline-btn {
  border: 0.5px solid #041C2C;
  box-sizing: border-box;
  border-radius: 68px;
  color: #000;
  padding: .3rem .625rem;
  font-size: .67rem;
  margin-bottom: 1rem;
}

.icon-circle {
  display: inline-block;
  border-radius: 50%;
  margin: 0 .25rem;
}

.icon-circle-alert {
  background: #F19D9D;
}

.icon-circle-small {
  width: 0.9375rem;
  height: 0.9375rem;
}

@media (min-width: 1290px) {
  .alerts-bg {
    background-position: 0px 4px;
    background-size: 17%;
  }
  
  .news-bg {
    background-position: 5px 9px;
    background-size: 11%;
  }
  
  .mail-bg {
    background-position: 13px 22px;
    background-size: 11%;
  }
}
`;