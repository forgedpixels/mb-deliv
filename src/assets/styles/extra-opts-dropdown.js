export default `
  .msg-opts-menu {
    position: relative;
  }

  .quick-opts {
    display: flex;
    justify-content: flex-end;
  }

  .quick-opts button {
    width: 1.35rem;
    height: 1.35rem;
    margin-left: 1.25rem;
    cursor: pointer;
    text-align: right;
  }

  .quick-opts button svg {
    height: 100%;
    width: auto;
  }

  .quick-opts button.dots {
    margin-left: .5rem;
    width: 1rem;
  }

  @media (min-width: 1290px) {
    .quick-opts button.dots {
      width: 1.15rem;
      text-align: center;
    }
  }

  .extra-opts {
    display: none;
    position: fixed;
    bottom: 0;
    left: 0;
    width: 100%;
    background: #F5F5F5;
  }

  .extra-opts.show {
    display: block;
  }

  .extra-opts button {
    display: flex;
    font-size: 1rem;
    width: 100%;
  }

  .extra-opts button svg {
    width: 1rem;
    height: 1rem;
    margin-right: .5rem;
  }

  .extra-opts button:last-child {
    margin-bottom: 0;
  }

  .extra-opts .mail svg path {
    fill: #0099FF;
  }

  .extra-opts .calendar svg path {
    stroke: #0099FF;
  }

  ol > li {
    background: #fff;
    padding: 1.875rem;
    color: #041C2C;
  }

  ol > li:nth-child(even) {
    background: #e6f5ff;
  }

  ol > li:nth-child(odd) {
    background: #d8f0ff;
  }

  ol > li:first-child {
    background: #fff;
  }

  @media (min-width: 1290px) {
    .msgs-opts-menu {
      position: absolute;
      right: 1rem;
      position: absolute;
      right: 1rem;
      width: 2rem;
    }

    .quick-opts {
      flex-wrap: wrap;
    }

    .quick-opts button {
      margin-bottom: 1rem;
      margin-left: 0;
    }

    .extra-opts {
      position: absolute;
      left: auto;
      right: 0;
      top: 100%;
      width: 16rem;
      padding: 0;
    }

    .extra-opts div:before {
      display: none;
    }

    ol > li {
      padding-right: 4.875rem;
    }

    .msg-nav-opts {
      position: absolute;
      top: 0;
      right: 0;
    }
  }
`;