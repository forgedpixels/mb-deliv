export default `
.attach-btn {
  display: flex;
  padding-bottom: 1.67rem;
  letter-spacing: 2px;
}

.attach-btn svg {
  margin-right: .3rem;
}

.attachments {
  margin-bottom: 1.67rem;
}

.attachments li {
  color: #000;
}

.file-name {
  font-weight: bold;
}
`;