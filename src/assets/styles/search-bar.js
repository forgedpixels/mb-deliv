export default `
  .form__group {
    position: relative;
    max-width: 290px;
  }

  .form__field {
    z-index: 1;
    position: relative;
    width: 100%;
    border: 0;
    border-bottom: 2px solid #0099FF;
    outline: 0;
    font-size: 1.5rem;
    color: #fff;
    background: transparent;
    transition: border-color 0.2s;
    padding: 0 2rem 0.375rem 0;
  }

  .form__icon {
    position: absolute;
    right: 0;
    bottom: 0;
    width: 1.5rem;
    height: auto;
  }

  .form__icon svg {
    width: 100%;
  }

  .form__field:valid + .form__label {
    display: none;
  }
  .form__field::placeholder {
    color: transparent;
  }
  .form__field:placeholder-shown ~ .form__label {
    font-size: 1.5rem;
    cursor: text;
  }

  .form__label {
    position: absolute;
    top: 0;
    display: block;
    transition: 0.2s;
    font-size: 1rem;
    color: #fff;
  }

  .form__field:focus {
    border-width: 3px;
    border-color: #B1FDD8;
    color: #fff;
  }
  .form__field:focus ~ .form__label {
    position: absolute;
    display: block;
    transition: 0.2s;
    font-size: 1rem;
    color: transparent;
  }

  /* reset input */
  .form__field:required, .form__field:invalid {
    box-shadow: none;
  }

  .current-order-no {
    font-size: 1.5rem;
    font-weight: bold;
  }

  @media (min-width: 1290px) {
    .current-order-no,
    .form__field,
    .form__label,
    .form__field:focus ~ .form__label,
    .form__field:placeholder-shown ~ .form__label {
      font-size: 3rem;
    }

    .form__group {
      margin-top: -.5rem;
    }

    .form__field {
      padding-right: 2.625rem;
      margin-bottom: 1rem;
    }

    .form__icon {
      width: 2.125rem;
      bottom: 1.5rem;
    }
  }
`;