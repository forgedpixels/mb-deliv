export default `

  .data-table {
    height: 100%;
    background: #fff;
  }

  .data-table table {
    position: relative;
    width: 100%;
    padding: 1rem;
    background: #fff;
    color: #041C2C;
  }

  .data-table th {
    text-transform: uppercase;
  }

  .data-table th td {
    font-weight: bold;
  }

  .data-table tr,
  .data-table td{
    position: relative;
  }

  .data-table td,
  .data-table th {
    text-align: left;
    padding-bottom: 1rem;
  }

  @media (min-width: 1290px) {
    td,th {
      font-size: 1.5rem;
    }
  }
`;