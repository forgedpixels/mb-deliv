export default `
/* reusable classes across components */
* {
  margin: 0;
  padding: 0;
  box-sizing: border-box;
}

textarea:focus,
input:focus {
  outline-color: #e6f5ff;
}

li {
  list-style: none;
}

a {
  text-decoration: none;
}

button {
  outline: 0;
  background: none;
  border: none;
  font-family: proxima-nova, sans-serif !important;
}

select {
  // A reset of styles, including removing the default dropdown arrow
  appearance: none;
  // Additional resets for further consistency
  background-color: transparent;
  border: none;
  padding: 0 1em 0 0;
  margin: 0;
  width: 100%;
  font-family: inherit;
  font-size: inherit;
  cursor: inherit;
  line-height: inherit;
}

input,
textarea {
  border: 1px solid #000;
  padding: 0.7rem;
  font-family: proxima-nova, sans-serif !important;
  font-size: 1rem;
}

textarea {
  display: block;
  width: 100%;
  min-height: 5.625rem;
}
`;