export default `
  <svg width="43" height="43" viewBox="0 0 43 43" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path d="M33.9816 23.6623C33.9816 23.6623 30.6253 29.6345 22.5448 29.7181C20.2507 29.7471 17.9849 29.2096 15.9482 28.1532C13.9115 27.0968 12.1669 25.5543 10.8691 23.6623C12.1348 21.7385 13.8728 20.1716 15.917 19.1113C17.9612 18.0511 20.2432 17.5329 22.5448 17.6065C24.8028 17.5938 27.0289 18.1401 29.0245 19.1968C31.02 20.2534 32.723 21.7876 33.9816 23.6623Z" stroke="#B1FDD8" stroke-miterlimit="10"/>
    <path d="M22.4501 27.4128C25.158 27.4128 27.3533 25.2176 27.3533 22.5096C27.3533 19.8017 25.158 17.6064 22.4501 17.6064C19.7421 17.6064 17.5469 19.8017 17.5469 22.5096C17.5469 25.2176 19.7421 27.4128 22.4501 27.4128Z" fill="#B1FDD8"/>
    <path d="M20.0123 14.7454L19.5703 10.4932" stroke="#B1FDD8" stroke-miterlimit="10" stroke-linecap="round"/>
    <path d="M15.9096 15.9994L13.9746 12.2607" stroke="#B1FDD8" stroke-miterlimit="10" stroke-linecap="round"/>
    <path d="M11.6215 18.5736L8.80859 15.5576" stroke="#B1FDD8" stroke-miterlimit="10" stroke-linecap="round"/>
    <path d="M24.7734 14.728L25.502 10.5176" stroke="#B1FDD8" stroke-miterlimit="10" stroke-linecap="round"/>
    <path d="M29.2344 16.1073L31.241 12.3926" stroke="#B1FDD8" stroke-miterlimit="10" stroke-linecap="round"/>
    <path d="M33.2715 18.4959L36.1143 15.5576" stroke="#B1FDD8" stroke-miterlimit="10" stroke-linecap="round"/>
  </svg>
`;