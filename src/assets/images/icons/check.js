export default `<svg width="29" height="21" viewBox="0 0 29 21" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M9.21603 16.5682L2.3411 10.038L0 12.2461L9.21603 21L29 2.20805L26.6754 0L9.21603 16.5682Z" fill="#0099FF"/>
</svg>`;
