export default `
  <svg width="14" height="13" viewBox="0 0 14 13" fill="none" xmlns="http://www.w3.org/2000/svg">
    <line x1="0.5" y1="1.5" x2="13.5" y2="1.5" stroke="#0099FF" stroke-linecap="round"/>
    <line x1="0.5" y1="6.5" x2="13.5" y2="6.5" stroke="#0099FF" stroke-linecap="round"/>
    <line x1="0.5" y1="11.5" x2="13.5" y2="11.5" stroke="#0099FF" stroke-linecap="round"/>
    <circle cx="5.5" cy="11.5" r="1.5" fill="#0099FF"/>
    <circle cx="3.5" cy="1.5" r="1.5" fill="#0099FF"/>
    <circle cx="10.5" cy="6.5" r="1.5" fill="#0099FF"/>
  </svg>
`;