export default `
  <svg width="16" height="15" viewBox="0 0 16 15" fill="none" xmlns="http://www.w3.org/2000/svg">
    <rect x="1.5" y="3.5" width="13" height="11" stroke="#0099FF"/>
    <rect x="0.5" y="0.5" width="15" height="3" stroke="#0099FF"/>
    <rect x="5.5" y="6.5" width="5" height="1" rx="0.5" stroke="#0099FF"/>
  </svg>
`;