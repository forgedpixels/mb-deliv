export default `
  <svg width="20" height="21" viewBox="0 0 20 21" fill="none" xmlns="http://www.w3.org/2000/svg">
  <path d="M0 9.51992L20 9.51992V11.5582L0 11.5582L0 9.51992Z" fill="#0099FF"/>
  <path d="M11 0.347656L11 20.7305H9L9 0.347656L11 0.347656Z" fill="#0099FF"/>
  </svg>
`;