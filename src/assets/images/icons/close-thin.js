export default `
<svg width="37" height="36" viewBox="0 0 37 36" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M10.5483 8.3853L28.5483 26.3853L27.3189 27.6147L9.31889 9.6147L10.5483 8.3853Z" fill="white"/>
<path d="M28.5483 9.6147L10.5483 27.6147L9.31889 26.3853L27.3189 8.3853L28.5483 9.6147Z" fill="white"/>
</svg>
`;