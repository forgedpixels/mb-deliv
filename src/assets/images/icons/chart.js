export default `
<svg width="32" height="23" viewBox="0 0 32 23" fill="none" xmlns="http://www.w3.org/2000/svg">
  <path d="M5.49913 17.9983L13.3753 11.0495C13.7369 10.7305 14.2823 10.7108 14.6714 11.0025L17.3161 12.9861C17.7103 13.2818 18.2642 13.2571 18.6248 12.9278L29.4997 2.99831" stroke="white"/>
  <circle cx="3.5" cy="19.5" r="3" stroke="white"/>
  <circle cx="29.5" cy="2.5" r="2.5" fill="white"/>
</svg>
`;