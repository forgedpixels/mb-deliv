export default `
<svg width="23" height="24" viewBox="0 0 23 24" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M0.5 4C0.5 2.61929 1.61929 1.5 3 1.5H20C21.3807 1.5 22.5 2.61929 22.5 4V21C22.5 22.3807 21.3807 23.5 20 23.5H3C1.61929 23.5 0.5 22.3807 0.5 21V4Z" stroke="white"/>
<rect x="6" width="12" height="4" rx="1" fill="white"/>
<rect x="4.38819" y="9.1792" width="14.6464" height="0.776372" rx="0.388186" stroke="white" stroke-width="0.776372"/>
<rect x="4.38819" y="13.6011" width="14.6464" height="0.776372" rx="0.388186" stroke="white" stroke-width="0.776372"/>
<rect x="4.38623" y="18.3882" width="14.6464" height="0.776372" rx="0.388186" stroke="white" stroke-width="0.776372"/>
</svg>
`;