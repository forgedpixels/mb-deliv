export default `
<svg width="19" height="26" viewBox="0 0 19 26" fill="none" xmlns="http://www.w3.org/2000/svg">
  <path d="M0.562069 25.5L1.50034 17.5H17.4997L18.4379 25.5H0.562069Z" stroke="white"/>
  <path d="M9 1C9 0.447715 9.44772 0 10 0C10.5523 0 11 0.447715 11 1L11 13H9L9 1Z" fill="white"/>
  <path d="M1 18C1 15.2386 3.23858 13 6 13L13 13C15.7614 13 18 15.2386 18 18L1 18Z" fill="white"/>
  <rect x="13.7354" y="20.6279" width="4.63887" height="0.529256" rx="0.264628" transform="rotate(90 13.7354 20.6279)" stroke="white" stroke-width="0.529256"/>
  <rect x="9.73537" y="21.8545" width="3.41042" height="0.529256" rx="0.264628" transform="rotate(90 9.73537 21.8545)" stroke="white" stroke-width="0.529256"/>
  <rect x="5.92873" y="20.6279" width="4.63887" height="0.529256" rx="0.264628" transform="rotate(90 5.92873 20.6279)" stroke="white" stroke-width="0.529256"/>
</svg>`
;