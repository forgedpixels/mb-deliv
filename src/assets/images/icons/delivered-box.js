export default `
  <svg width="25" height="20" viewBox="0 0 25 20" fill="none" xmlns="http://www.w3.org/2000/svg">
  <rect x="1.35" y="1.35" width="22.3" height="17.3" stroke="white" stroke-width="1.3" stroke-linecap="round" stroke-linejoin="round"/>
  <rect x="9.65" y="2.65" width="5.7" height="4.92222" fill="white" stroke="white" stroke-width="1.3" stroke-linecap="round" stroke-linejoin="round"/>
  </svg>
`;