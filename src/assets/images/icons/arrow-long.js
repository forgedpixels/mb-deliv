export default `
<svg width="40" height="25" viewBox="0 0 40 25" fill="none" xmlns="http://www.w3.org/2000/svg">
<path fill-rule="evenodd" clip-rule="evenodd" d="M34.1759 11.5868L0.309002 11.4155L0.25 13.984H33.5859L24.2636 23.0023L26.3287 25L39.25 12.5571L39.191 12.5L37.1849 10.5594L26.3287 0L24.2636 1.99772L34.1759 11.5868Z" fill="white"/>
</svg>
`;
