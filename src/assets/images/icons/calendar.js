export default `
<svg width="24" height="25" viewBox="0 0 24 25" fill="none" xmlns="http://www.w3.org/2000/svg">
  <path d="M0.5 9.92969C0.5 9.10126 1.17157 8.42969 2 8.42969H21.2911C22.1196 8.42969 22.7911 9.10126 22.7911 9.92969V22.5544C22.7911 23.3828 22.1196 24.0544 21.2911 24.0544H2C1.17157 24.0544 0.5 23.3828 0.5 22.5544V9.92969Z" stroke="white"/>
  <path d="M0 5.39648C0 4.29191 0.895431 3.39648 2 3.39648H21.2911C22.3957 3.39648 23.2911 4.29191 23.2911 5.39648V9.44183H0V5.39648Z" fill="white"/>
  <rect x="5.04639" y="7.5415" width="6.78031" height="0.776372" transform="rotate(-90 5.04639 7.5415)" stroke="white" stroke-width="0.776372"/>
  <rect x="17.4683" y="7.5415" width="6.78031" height="0.776372" transform="rotate(-90 17.4683 7.5415)" stroke="white" stroke-width="0.776372"/>
</svg>`;