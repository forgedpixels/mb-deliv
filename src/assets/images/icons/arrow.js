export default `
<svg width="29" height="26" viewBox="0 0 29 26" fill="none" xmlns="http://www.w3.org/2000/svg">
<path fill-rule="evenodd" clip-rule="evenodd" d="M24.3732 11.6137L0.0423882 11.4422L0 14.0145H23.9494L17.252 23.0461L18.7356 25.0468L28.0186 12.5854L27.9762 12.5283L26.535 10.5847L18.7356 0.00976562L17.252 2.01044L24.3732 11.6137Z" fill="white"/>
</svg>
`;
