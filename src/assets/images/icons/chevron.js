export default `
  <svg width="16" height="10" viewBox="0 0 16 10" fill="none" xmlns="http://www.w3.org/2000/svg">
  <path d="M14.3916 9.31934L6.48683 1.41454L7.90104 0.000321997L15.8058 7.90512L14.3916 9.31934Z" fill="white"/>
  <path d="M-6.18173e-08 7.90512L7.90228 0.00283874L9.3165 1.41705L1.41421 9.31933L-6.18173e-08 7.90512Z" fill="white"/>
  </svg>
`;