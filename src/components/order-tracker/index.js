import resetCss from '../../assets/styles/reset.js';
import cssClasses from '../../assets/styles/classes.js';
import styles from './styles.js';
import htmlTemplate from './template.js';
import truckIcon from '../../assets/images/icons/truck.js';
import eyeIcon from '../../assets/images/icons/eye.js';
import deliveredIcon from '../../assets/images/icons/delivered-box.js';
import checkIcon from '../../assets/images/icons/check.js';

const mockData = {
  images: {
    truckIcon,
    eyeIcon,
    deliveredIcon,
    checkIcon
  },
  orderStatus: {
    status: "submitted",
    truckPath: [
      {
        stopName: 'beginning',
        status: 'complete'
      },
      {
        stopName: 'middle',
        status: 'complete'
      },
      {
        stopName: '3rd',
        status: 'incomplete'
      },
      {
        stopName: 'end',
        status: 'incomplete'
      }
    ],
  }
  
};

class OrderTracker extends HTMLElement {
  constructor() {
    super();
    this.attachShadow({ mode: "open" });
    this.bgStyle = null;
    this.template = document.createElement("template");
    this.template.innerHTML = `
    <style>
      ${resetCss}
      ${cssClasses}
      ${styles}
    </style>

    ${htmlTemplate(this.parseData(mockData))}
    `;
  }

  parseData(data) {
    const bgStyle = this.getAttribute("bgStyle");
    const getStatusClass = (status) => {
      switch(status) {
        case 'ready to view':
          return 'status-ready';
        case 'delivered':
          return 'status-delivered';
        case 'submitted':
          return 'status-submitted';
        default:
      }
    };

    let output = {
      ...data
    };
    if (data.orderStatus.truckPath) {
      const stopsIncomplete = data.orderStatus.truckPath.filter(stop => stop.status === 'incomplete');
      output.incompleteStops = stopsIncomplete.length
    }

    output.statusClass = getStatusClass(data.orderStatus.status);
    output.trackerStyleClass = bgStyle ? 'tracker-on-white' : ''

    return output;
  }

  connectedCallback() {
    this.shadowRoot.appendChild(this.template.content.cloneNode(true));
  }
}

customElements.define("order-tracker", OrderTracker);
