const createLine = (data) => {
  if (data.orderStatus.status === 'ready to view') {
    return `
      <span class="status-icon"></span>
      <span class="text-progress text-filled text-only-step">Ready for viewing ${data.images.eyeIcon}</span>
      <span class="status-icon status-filled"></span>
    `;
  } else if (data.orderStatus.status === 'delivered') {
    return `
      <span class="status-icon status-filled"></span>
      <span class="text-progress text-filled text-only-step">Delivered ${data.images.deliveredIcon}</span>
      <span class="status-icon status-filled"></span>
    `;
  } else if (data.orderStatus.status === 'submitted') {
    return `
      <span class="status-icon status-filled"></span>
      <span class="text-progress text-filled text-only-step">Submitted ${data.images.checkIcon}</span>
      <span class="status-icon"></span>
    `;
  } else if (data.incompleteStops >= 2) {
    return `
    <span class="status-icon status-filled"></span>
    <span class="text-progress text-filled"><span>In Transit</span></span>
    <div class="expanded">
      <span class="status-icon status-filled status-current">${data.images.truckIcon}</span>
      <span class="text-progress"><span>${data.incompleteStops} stops away</span></span>
      <span class="status-icon status-unfilled"></span>
    </div>
    `;
  } else {
    return `
      <div class="expanded">
        <span class="status-icon status-filled"></span>
        <span class="text-progress text-filled"><span>In Transit</span></span>
        <span class="status-icon status-filled status-current">${data.images.truckIcon}</span>
      </div>
      <span class="text-progress">1 stop away</span>
      <span class="status-icon status-unfilled"></span>
    `;
  }
}

export default data => `
  <div class="status-tracker ${data.statusClass} ${data.trackerStyleClass}">
    ${createLine(data)}
  </div>
`;