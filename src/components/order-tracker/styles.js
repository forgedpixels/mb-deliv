export default `
  .status-icon {
    position: relative;
    display: inline-block;
    width: 1rem;
    height: 1rem;
    border-radius: 50%;
    border: 2px solid #EFCB9B;
  }
  .status-ready .status-icon {
    border: 2px solid #B1FDD8;
  }

  .status-delivered .status-icon, 
  .status-submitted .status-icon{
    border: 2px solid #fff;
  }

  .status-ready .text-progress,
  .status-delivered .text-progress,
  .status-submitted .text-progress {
    position: relative;
    display: flex;
    justify-content: center;
    align-items: center;
    text-align: center;
    font-size: .5rem;
    white-space: nowrap;
    text-transform: uppercase;
    font-weight: bold;
    width: calc(100% - 3rem);
    color: #fff;
  }

  .status-ready .text-progress {
    color: #B1FDD8;
  }

  .status-ready .text-progress svg,
  .status-delivered .text-progress svg,
  .status-submitted .text-progress svg {
    top: -2.25rem;
    position: absolute;
  }

  .status-submitted .text-progress svg path {
    fill: #fff;
  }

  .text-only-step.text-filled:after,
  .text-only-step.text-filled:before {
    content: '';
    border-top: 1px solid;
    margin: 0 5px 0 0;
    color: #fff;
    flex: 1 0 26px;
  }

  .text-progress.text-filled:after {
    margin: 0 0 0 5px;
  }
  
  .status-ready .status-icon.status-filled {
    background: #B1FDD8;
  }

  .status-delivered .status-icon.status-filled,
  .status-submitted .status-icon.status-filled {
    background: #fff;
  }

  /* WHEN ON WHITE BG */
  .tracker-on-white.status-delivered .status-icon,
  .tracker-on-white.status-submitted .status-icon {
    border: 2px solid #000;
  }

  .tracker-on-white.status-delivered .status-icon.status-filled,
  .tracker-on-white.status-submitted .status-icon.status-filled {
    background: #000;
  }

  .tracker-on-white .text-only-step.text-filled:after,
  .tracker-on-white .text-only-step.text-filled:before,
  .tracker-on-white.status-delivered .text-progress,
  .tracker-on-white.status-submitted .text-progress {
    color: #000;
  }

  .tracker-on-white .status-delivered .status-submitted .text-progress svg path {
    fill: #000;
  }

  /* END WHITE BG AREA */


  .status-icon.status-filled {
    background: #EFCB9B;
  }

  .status-icon.status-current svg {
    top: -2.75rem;
    position: absolute;
    left: -1rem;
  }

  .status-tracker {
    display: flex;
    width: 100%;
    justify-content: space-between;
    max-width: 320px;
    align-items: center;
    letter-spacing: 2px;
  }

  .expanded {
    width: 100%;
    max-width: 210px;
    display: flex;
    justify-content: space-between;
    align-items: center;
  }

  .text-progress {
    align-items: center;
    text-align: center;
    font-size: .5rem;
    white-space: nowrap;
    text-transform: uppercase;
    font-weight: bold;
    color: #EFCB9B;
  }

  .expanded .text-progress {
    max-width: 170px;
    width: calc(100% - 42px);
    position: relative;
  }

  .expanded .text-progress span {
    text-align:center;
    display: grid;
    grid-template-columns: 1fr max-content 1fr;
    grid-gap: 0.375rem;
    justify-content:center;
    align-items: center;
  }

  .expanded .text-progress span:before,
  .expanded .text-progress span:after {
    content: " ";
    display: block;
    border-bottom: 1px solid #fff;
  }

  .expanded .status-icon:first-child {
    margin-right: .375rem;
  }

  .expanded .status-icon:last-child {
    margin-left: .375rem;
  }

  .text-progress.text-filled:after {
    margin: 0 0 0 5px;
  }

  /* Animation Fade Ins */
  .status-tracker > .status-icon:nth-child(1) {
    opacity: 1;
    animation-name: fadeIn;
    animation-duration: 1s;
    animation-iteration-count: 1;
  }

  .status-tracker > .text-progress:nth-child(2) {
    opacity: 1;
    animation-name: fadeIn2;
    animation-duration: 1.5s;
    animation-iteration-count: 1;
  }

  .expanded > .status-icon:nth-child(1) {
    opacity: 1;
    animation-name: fadeIn3;
    animation-duration: 2s;
    animation-iteration-count: 1;
  }

  .expanded > .text-progress:nth-child(2) {
    opacity: 1;
    animation-name: fadeIn4;
    animation-duration: 2.5s;
    animation-iteration-count: 1;
  }

  .expanded > .status-icon:nth-child(3) {
    opacity: 1;
    animation-name: fadeIn5;
    animation-duration: 3s;
    animation-iteration-count: 1;
  }

  @keyframes fadeIn {
    from { opacity: 0; }
    to { opacity: 1; }
  }

  @keyframes fadeIn2 {
    0%   { opacity: 0; }
    50%  { opacity: 0; }
    100% { opacity: 1; }
  }

  @keyframes fadeIn3 {
    0%   { opacity: 0; }
    75%  { opacity: 0; }
    100% { opacity: 1; }
  }

  @keyframes fadeIn4 {
    0%   { opacity: 0; }
    80%  { opacity: 0; }
    100% { opacity: 1; }
  }

  @keyframes fadeIn5 {
    0%   { opacity: 0; }
    83.4%  { opacity: 0; }
    100% { opacity: 1; }
  }

  /* If less then 2 stops away */
  .expanded:first-child > .status-icon:nth-child(1) {
    opacity: 1;
    animation-name: fadeIn;
    animation-duration: 1s;
    animation-iteration-count: 1;
  }

  .expanded:first-child > .text-progress:nth-child(2) {
    opacity: 1;
    animation-name: fadeIn2;
    animation-duration: 1.5s;
    animation-iteration-count: 1;
  }

  .expanded:first-child > .status-icon:nth-child(3),
  .status-ready .status-icon.status-filled,
  .status-delivered .status-icon.status-filled:nth-child(3),
  .status-submitted .status-icon:nth-child(3) {
    opacity: 1;
    animation-name: fadeIn3;
    animation-duration: 2s;
    animation-iteration-count: 1;
  }

  .expanded + .text-progress:nth-child(2) {
    opacity: 1;
    animation-name: fadeIn4;
    animation-duration: 2.5s;
    animation-iteration-count: 1;
  }

  .expanded + .text-progress + .status-icon:nth-child(3) {
    opacity: 1;
    animation-name: fadeIn5;
    animation-duration: 3s;
    animation-iteration-count: 1;
  }
`;