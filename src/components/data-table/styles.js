export default `
  .sort-data-table {
    font-size: 0.875rem;
    padding: 1rem 1rem 0 1rem;
    align-items: center;
  }

  .edit-data .select-icon svg path{
    fill: #C4C4C4;
  }

  .edit-data button,
  .sort-data button {
    display: flex;
    justify-content: space-between;
  }

  .edit-data button {
    align-items: flex-end;
  }

  .edit-data button svg {
    margin-right: .34rem;
  }

  .edit-data button.editing {
    color: #041C2C;
  }

  .edit-data button.editing svg path {
    fill: #041C2C;
  }

  .sort-data,
  .edit-data {
    position: relative;
  }

  .edit-data .batch-data-opts button {
    min-width: 110px;
  }

  .sort-data button.selected svg {
    transform: none;
  }

  .sort-data svg,
  .edit-data .select-icon svg {
    transform: rotate(180deg);
  }

  .sort-data button {
    font-size: 0.875rem;
  }

  .sort-data button svg {
    width: .75rem;
  }

  .sort-data svg path {
    fill: #000;
  }

  .select-opt {
    width: .5rem;
    height: .5rem;
    border-radius: 50px;
    border: 2px solid #D8D8D8;
    vertical-align: middle;
  }

  tr.back-ordered td {
    padding-bottom: 2.75rem;
  }

  tr.back-ordered td div {
    position: absolute;
    left: 0;
    min-width: 20rem;
    padding-top: .5rem;
    font-size: 1rem;
    color: #8A8A8A;
  }

  tr th:first-child {
    font-size: 0;
  }

  td.selected svg {
    max-width: 14px;
    height: auto;
  }

  .case-txt-lng {
    display: none;
  }

  .approval-status {
    font-weight: 300;
    position: absolute;
    left: -3rem;
    color: #CECECE;
  }

  .approval-amt {
    position: relative;
  }

  .approval-amt:before {
    content: ' ';
    width: 1.5rem;
    height: 2px;
    background: #cecece;
    transform: rotate(45deg);
    position: absolute;
    top: 48%;
    left: -40%;
  }

  @media (min-width: 1290px) {
    td,th {
      font-size: 1.5rem;
    }

    tr.selected td,
    tr:hover td {
      font-weight: bold;
    }

    tr.selected td:last-child:after,
    tr:hover td:last-child:after {
      content: ' ';
      width: 3rem;
      height: .2rem;
      position: absolute;
      right: -1rem;
      margin-top: .7rem;
    }

    tr:hover td:last-child:after {
      background: #0099FF;
    }

    tr.selected td:last-child:after {
      background: #000;
    }
    
    tr:hover td {
      color: #0099FF;
      cursor: pointer;
    }

    .case-txt-lng {
      display: inline-block;
    }

    .case-txt-shrt {
      display: none;
    }

    td:nth-child(1) {
      width: 20px;
    }

    td:nth-child(2) {
      width: 80px;
    }

    td:nth-child(3) {
      width: 280px;
    }

    td:nth-child(4) {
      width: 135px;
    }

    td:nth-child(5) {
      width: 80px;
    }
  }
`;