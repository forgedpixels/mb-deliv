import resetCss from '../../assets/styles/reset.js';
import cssClasses from '../../assets/styles/classes.js';
import tableClasses from '../../assets/styles/table.js';
import chevronIcon from '../../assets/images/icons/chevron.js';
import checkIcon from '../../assets/images/icons/check.js';
import orderEditIcon from '../../assets/images/icons/order-edit.js'
import styles from './styles.js';
import htmlTemplate from './template.js';

let data = {
  images: {
    chevronIcon,
    orderEditIcon,
    checkIcon
  },
  orderData: [
    {
      type: 'Frozen',
      orderObjs: [
        {
          mri: 10006,
          name: 'Muffins-Frozen',
          bulkAmt: 2,
          num: '55',
          batchSelected: true
        },
        {
          mri: 10201,
          name: 'Nuggets-Frozen',
          bulkAmt: 2,
          num: '72'
        },
      ]
    },
    {
      type: 'Chilled',
      orderObjs: [
        {
          mri: 10002,
          name: 'Sausage Patty',
          bulkAmt: 2,
          num: '56',
          backOrdered: true,
          backOrderDate: '12.11.20'
        },
        {
          mri: 10005,
          name: 'Beef-Patty',
          bulkAmt: 2,
          num: '12'
        },
        {
          mri: 10005,
          name: 'McRib-Patty',
          bulkAmt: 3,
          num: '721'
        },
      ]
    },
    {
      type: 'Toys',
      orderObjs: [
        {
          mri: 10002,
          name: 'hulk',
          bulkAmt: 2,
          num: '56',
          selectedToView: true,
          waitingApproval: {
            amt: 2
          }
        },
        {
          mri: 10005,
          name: 'iron man',
          bulkAmt: 2,
          num: '12'
        },
        {
          mri: 10005,
          name: 'bugs bunny',
          bulkAmt: 3,
          num: '721'
        },
      ]
    },
    {
      type: 'Dry',
      orderObjs: [
        {
          mri: 10012,
          name: 'Roll',
          bulkAmt: 2,
          num: '12'
        },
        {
          mri: 10011,
          name: 'Napkins',
          bulkAmt: 2,
          num: '9'
        },
      ]
    },
    {
      type: 'Promo',
      orderObjs: [
        {
          mri: 10012,
          name: 'coupone 1',
          bulkAmt: 2,
          num: '72'
        },
        {
          mri: 10011,
          name: 'coupon 2',
          bulkAmt: 2,
          num: '72'
        },
      ]
    },
    {
      type: 'Ops Supply',
      orderObjs: [
        {
          mri: 10012,
          name: 'Roll',
          bulkAmt: 2,
          num: '72'
        },
        {
          mri: 10011,
          name: 'Napkins',
          bulkAmt: 2,
          num: '72'
        },
      ]
    }
  ]
};

const template = document.createElement("template");
const style = `
  <style>
    ${resetCss}
    ${cssClasses}
    ${tableClasses}

    ${styles}
  </style>
`;

const getTableData = (data) => {
  const tableType = localStorage.getItem('table-view');
  const urlParams = new URLSearchParams(window.location.search);
  let newTableData;

  if (tableType === 'All') {
    let orderObjs = [];

    let orders = data.orderData.map((val) => {
      return [
        ...orderObjs,
        val.orderObjs
      ]
    });
  
    newTableData = orders.flat(Infinity);
  } else {
    let selectedData = data.orderData.filter(data => {
      return data.type === tableType
    });
    newTableData = selectedData[0].orderObjs;
  }

  return {
    ...data,
    orderData: newTableData,
    showSortOpts: urlParams.get('design') === '2' ? 'show' : '' // NOTE: state logic in LWC, this is for display purposes
  }
};


template.innerHTML = /*html*/`
  ${style}
  ${htmlTemplate(getTableData(data))}
`;

class DataTable extends HTMLElement {
  constructor() {
    super();
    this.attachShadow({ mode: "open" });
    localStorage.removeItem('order-selected');
    this.tableView = localStorage.getItem('table-view');

    window.addEventListener('storage', () => {
      const tableView = localStorage.getItem('table-view');
      // When local storage changes, dump the list to
      // the console.
      if (this.tableView !== tableView) {
        this.tableView = tableView;
        this.render();
      }
    }, false);
  }

  connectedCallback() {
    this.shadowRoot.appendChild(template.content.cloneNode(true));
    
    let buttons = this.shadowRoot.querySelectorAll("td");

    buttons.forEach((elem) => {
      elem.onclick = () => this.showOrderDetails();
    });
  }

  showOrderDetails() {
    localStorage.setItem('order-selected', 'show');
    window.dispatchEvent( new Event('storage') );
  }

  render() {
    this.shadowRoot.innerHTML = `
      ${style}
      ${htmlTemplate(getTableData(data))}
    `;
  }
}

customElements.define("c-data-table", DataTable);
