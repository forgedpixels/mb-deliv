const createRows = (data) => {
  return data.orderData.map((val) => {
    const isBatchSelected = val => {
      if (val.batchSelected) {
        return `<td class="selected">${data.images.checkIcon}</td>`;
      }
      return `<td><button class="select-opt" aria-label="select"></button></td>`;
    };

    const getApprovalAmt = val => {
      if (val.waitingApproval) {
        return `<td>${val.bulkAmt} <span class="case-txt-lng">Cases</span><span class="case-txt-shrt">CS</span> <span class="approval-status"><span class="icon-circle icon-circle-small icon-circle-alert"></span><span class="approval-amt">${val.waitingApproval.amt}</span></span></td>`;
      } else if (val.backOrdered) {
        return `<td>B/O</td>`;
      }
      return `<td>${val.bulkAmt} <span class="case-txt-lng">Cases</span><span class="case-txt-shrt">CS</span></td>`;
    }

    const getMRICell = val => {
      if (val.backOrdered) {
        return `<td>${val.mri}<div>Expected Delivery <span class="bright-blue">${val.backOrderDate}</span></div></td>`;
      }
      return `<td>${val.mri}</td>`;
    }

    return `
    <tr class="font-light ${val.selectedToView ? 'selected' : ''} ${val.backOrdered ? 'back-ordered' : ''}">
      ${isBatchSelected(val)}
      ${getMRICell(val)}
      <td>${val.name}</td>
      ${getApprovalAmt(val)}
      <td>${val.backOrdered ? '--' : val.num}</td>
    </tr>
  `
}).join('');
};

export default (data = {}) => `
  <div class="data-table">
    <div class="sort-data-table content-to-ends">
      <div class="edit-data">
        <button class="${data.showSortOpts ? 'editing': ''}">${data.images.orderEditIcon} <span class="select-icon">${data.images.chevronIcon}</span> ${data.showSortOpts ? 2: ''}</button>
        <div class="batch-data-opts menu-box-shadow-on-lg select-dropdown-styled select-dropdown-styled-unfixed select-dropdown-absolute pos-abs-all-left ${data.showSortOpts ? 'show': ''}">
          <ul>
            <li><button>Move to top</button></li>
            <li><button>Move to bottom</button></li>
            <li><button>Message planner</button></li>
          </ul>
        </div>
      </div>

      <div class="sort-data">
        <button class="text-and-chevron-label ${data.showSortOpts ? 'selected': ''}">
          <span>Sort by:</span>
          <span class="font-light text-and-chevron">User ${data.images.chevronIcon}</span>
        </button>
        <div class="sort-data-opts menu-box-shadow-on-lg select-dropdown-styled select-dropdown-styled-unfixed select-dropdown-absolute pos-abs-all-right ${data.showSortOpts ? 'show': ''}">
          <ul>
            <li><button>User</button></li>
            <li><button>Opt 1</button></li>
            <li><button>Opt 2</button></li>
          </ul>
        </div>
      </div>
    </div>

    <table>
      <tr>
        <th>Select Opt</th>
        <th>MRI</th>
        <th>Name</th>
        <th>Bulk</th>
        <th>#</th>
      </tr>
      ${createRows(data)}
    </table>
  </div>
`;