export default `
  .alerts-container {
    position: relative;
    margin: 0 -1.75rem;
    height: 100%;
  }

  ul {
    display: flex;
    overflow-x: auto;
    padding: .75rem 1.75rem;
  }

  ul li {
    box-sizing: border-box;
    margin-right: 0.6875rem;
    padding: 1.8125rem;
    min-width: 236px;
  }

  ol {
    display: none;
    top: 1rem;
    left: 1rem;
  }

  ol li {
    margin-right: 0.5625rem;
  }

  ol li:last-child {
    margin-right: 0;
  }

  ol li button {
    background: #C4C4C4;
    border-radius: 50%;
    width: 0.4375rem;
    height: 0.4375rem;
  }

  ol li:first-child button {
    background: #0099FF;
  }

  .component-bg p {
    margin-bottom: 2rem;
  }

  .arrow-link {
    justify-content: flex-end;
  }

  .store-status,
  .alert-num {
    color: #F19D9D;
    text-transform: uppercase;
    font-weight: bold;
  }

  .alerts-total {
    font-weight: normal;
  }

  @media (min-width: 1290px) {
    .component-bg {
      padding: 1.25rem;
    }

    .component-header > div{
      padding-top: 1.5rem;
    }

    .alerts-container {
      margin: 0;
    }

    ul {
      padding: 0;
    }

    ol {
      position: absolute;
      display: flex;
    }

    ul li.alerts-bg {
      display: none;
      width: 100%;
      margin: 0;
      box-sizing: content-box;
      min-width: auto;
      background-position: 0px 30px;
    }

    ul li:first-child {
      display: block;
    }
  }
`;