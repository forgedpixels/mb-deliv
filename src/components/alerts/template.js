const createDots = (data = {alerts: []}) => {
  return data.alerts.map((val, i) => `
    <li><button></button></li>
  `).join('');
};

const createAlerts = (data = {alerts: []}) => {
  return data.alerts.map((val, i) => `
    <li class="component-bg alerts-bg">
      <header class="component-header content-to-ends">
        <div>
          <span>Alerts</span>
          <span class="alert-num sub-txt">${i+1} <span class="font-light">/ ${data.alerts.length}</span></span>
        </div>

        ${ i === 0 ? `<a href="" class="header-end-link header-view-all-link">View all</a>` : ''}
      </header>
      
      <span class="store-status">Store Rank ${val.storeRank}</span>
      <p class="font-light">${val.desc}</p>

      <div class="fix-it">
        <a href="" class="arrow-link">Let's fix it ${data.images.arrowIcon}</a>
      </div>
    </li>
  `).join('');
};

export default (data = {}) => `
  <div class="alerts-container">
    <ol>
      ${createDots(data)}
    </ol>
    <ul>
      ${createAlerts(data)}
    </ul>
  </div>
`;