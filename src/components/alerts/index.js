import resetCss from '../../assets/styles/reset.js';
import cssClasses from '../../assets/styles/classes.js';
import arrowIcon from '../../assets/images/icons/arrow.js';
import styles from './styles.js';
import htmlTemplate from './template.js';


const mockData = {
  images: {
    arrowIcon
  },
  alerts: [
    {
      storeRank: 'low',
      desc: 'Watch your stock count, and double check for accuracy to avoid alerts, overall waste...'
    },
    {
      storeRank: 'low',
      desc: 'Watch your stock count, and double check for accuracy to avoid alerts, overall waste...'
    },
    {
      storeRank: 'low',
      desc: 'Watch your stock count, and double check for accuracy to avoid alerts, overall waste...'
    },
    {
      storeRank: 'low',
      desc: 'Watch your stock count, and double check for accuracy to avoid alerts, overall waste...'
    },
  ]
};

const template = document.createElement("template");
const style = `
  <style>
    ${resetCss}
    ${cssClasses}

    ${styles}
  </style>
`;

template.innerHTML = /*html*/`
  ${style}
  ${htmlTemplate(mockData)}
`;

class Alerts extends HTMLElement {
  constructor() {
    super();
    this.attachShadow({ mode: "open" });
  }

  connectedCallback() {
    this.shadowRoot.appendChild(template.content.cloneNode(true));
  }
}

customElements.define("c-alerts", Alerts);
