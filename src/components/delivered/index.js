import resetCss from '../../assets/styles/reset.js';
import cssClasses from '../../assets/styles/classes.js';
import styles from './styles.js';
import htmlTemplate from './template.js';
import arrowIcon from '../../assets/images/icons/arrow-long.js';

const mockData = {
  images: {
    arrowIcon
  },
  deliveredData: {
    date: '08.10.20',
    orderNo: 7281164,
    deliverTime: '15:29',
    desc: 'We need a stock recount on 20007 Chicken Patty.'
  }
};

const template = document.createElement("template");
const style = `
  <style>
    ${resetCss}
    ${cssClasses}

    ${styles}
  </style>
`;

template.innerHTML = /*html*/`
  ${style}
  ${htmlTemplate(mockData)}
`;

class Delivered extends HTMLElement {
  constructor() {
    super();
    this.attachShadow({ mode: "open" });
  }

  connectedCallback() {
    this.shadowRoot.appendChild(template.content.cloneNode(true));
  }
}

customElements.define("c-delivered", Delivered);
