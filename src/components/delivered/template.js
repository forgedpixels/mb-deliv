export default (data = {}) => `
  <div class="component-bg clipboard-bg">
    <header class="component-header">
      <span class="delivered-date">${data.deliveredData.date}</span>
      <span class="delivered-orderno sub-txt">${data.deliveredData.orderNo}</span>
    </header>
    
    <div class="delivered-time"><strong>DELIVERED</strong> ${data.deliveredData.deliverTime}</div>
    <div class="delivered-desc font-light">${data.deliveredData.desc}</div>

    <a href="" class="arrow-link">View order ${data.images.arrowIcon}</a>
  </div>
`;