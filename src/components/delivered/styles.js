export default `
  .component-bg {
    padding: 1.8125rem;
    height: 100%;
  }

  .arrow-link {
    justify-content: flex-end;
  }

  .delivered-desc {
    margin-bottom: .75rem;
  }

  @media (min-width: 1290px) {
    .component-bg {
      padding: 1.25rem;
    }

    .clipboard-bg {
      background-size: 30px;
    }
  }
`;