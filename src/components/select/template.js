
const createSelectOpts = (data = []) => {
  return data.opts.categories.map((val) => {
    return `
      <li ${data.opts.selected === val ? ` class="selected"`: ``} data-val="${val}">
        <button class="font-light">${val}</button>
      </li>
    `;
  }).join('');
};

export default (data) => `
  <div class="select-opts">
    <div class="currently-selected">
      <span>${data.opts.selected}<span class="icon-chevron">${data.images.chevronIcon}</span> </span>
    </div>

    <ul class="all-opts font-light">
      ${createSelectOpts(data)}
    </ul>
  </div>
`;