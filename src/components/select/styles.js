export default `
  ul li {
    list-style: none;
    margin-bottom: 1rem;
  }

  li button {
    font-size: 1.5rem;
    color: #fff;
    width: 100%;
    text-align: left;
  }

  li.selected button {
    font-weight: bold;
  }

  .currently-selected {
    font-size: 1.5rem;
  }

  .currently-selected.hide {
    display: none;
  }

  .all-opts {
    display: none;
  }

  .all-opts.show {
    display: block;
  }

  @media (min-width: 1290px) {
    .currently-selected {
      display: none;
    }

    .all-opts {
      display: flex;
    }

    .all-opts li {
      padding-right: 2.25rem;
    }

    .all-opts li:last-child {
      padding-right: 0;
    }

    li.selected button {
      border-bottom: 2px solid #0099FF;
    }
  }

`;