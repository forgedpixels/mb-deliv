import resetCss from '../../assets/styles/reset.js';
import cssClasses from '../../assets/styles/classes.js';
import chevronIcon from '../../assets/images/icons/chevron.js';
import styles from './styles.js';
import htmlTemplate from './template.js';

let data = {
  images: {
    chevronIcon
  },
  opts: {
    selected: 'All',
    categories: [
      'All',
      'Frozen',
      'Chilled',
      'Dry',
      'Promo',
      'Toys',
      'Ops Supply'
    ]
  }
};

const template = document.createElement("template");
const style = `
  <style>
    ${resetCss}
    ${cssClasses}

    ${styles}
  </style>
`;

template.innerHTML = /*html*/`
  ${style}
  ${htmlTemplate(data)}
`;

class Select extends HTMLElement {
  constructor() {
    super();
    this.attachShadow({ mode: "open" });
  }

  connectedCallback() {
    this.shadowRoot.appendChild(template.content.cloneNode(true));
    this.setupListeners();
  }

  setupListeners() {
    this.shadowRoot.querySelector(".currently-selected").onclick = () => this.toggleClass();
    let buttons = this.shadowRoot.querySelectorAll("li button");

    buttons.forEach((elem) => {
      elem.onclick = () => this.selectVal(elem.parentElement.getAttribute('data-val'));
    })
  }

  removeListeners() {
    this.shadowRoot.querySelector(".currently-selected").onclick = null
    let buttons = this.shadowRoot.querySelectorAll("li button");
    buttons.forEach((elem) => {
      elem.onclick = null
    })
  }

  toggleClass() {
    this.shadowRoot.querySelector(".currently-selected").classList.toggle('hide');
    this.shadowRoot.querySelector(".all-opts").classList.toggle('show');
  }

  firstToUppercase(word) {
    return word.charAt(0).toUpperCase() + word.slice(1); 
  }


  selectVal(selectedVal) {
    data.opts.selected = selectedVal;

    localStorage.setItem('table-view', this.firstToUppercase(selectedVal));
    window.dispatchEvent( new Event('storage') );

    this.removeListeners();
    this.render();
    this.setupListeners();
  }

  render() {
    this.shadowRoot.innerHTML = `
      ${style}
      ${htmlTemplate(data)}
    `;
  }
}

customElements.define("c-select-order-type", Select);
