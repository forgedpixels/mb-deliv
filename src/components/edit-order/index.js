import resetCss from '../../assets/styles/reset.js';
import cssClasses from '../../assets/styles/classes.js';
import donutCss from '../../assets/styles/donut.js';
import selectInputCss from '../../assets/styles/select-input.js';
import styles from './styles.js';
import htmlTemplate from './template.js';
import chevronIcon from '../../assets/images/icons/chevron.js';
import arrowIcon from '../../assets/images/icons/arrow-long.js';
import checkIcon from '../../assets/images/icons/check.js';
import closeIcon from '../../assets/images/icons/close.js';

localStorage.removeItem('editing-order');

const urlParams = new URLSearchParams(window.location.search);

const mockData = {
  images: {
    arrowIcon,
    chevronIcon,
    checkIcon,
    closeIcon
  },
  showEditOrderOpts: urlParams.get('design') === '2' ? 'show' : '',
  order: {
    orderNo: 10018,
    stockDetail: {
      onHand: '3 CS',
      proposed: '1 CS',
      promo: true,
      inTransit: '1 CS',
      confirmed: '4 CS',
      buildTo: '4 CS',
      proposalPercent: 99,
    },
    product: {
      name: 'Bacon Roll',
      imgPath: './src/assets/images/stock/baconRoll.png',
      desc: 'It’s a classic – delicious bacon with a choice of ketchup or brown sauce, served on a soft, white roll.',
      nutrition: [
        {
          type: 'kj',
          amt: 1478,
          percent: 18
        },
        {
          type: 'kcal',
          amt: 351,
          percent: 18
        },
        {
          type: 'fat',
          amt: 11,
          percent: 16
        },
      ],
    },
  }
};

const parseData = (data) => {
  const optSelected = true; // NOTE: to be changed by whichever state logic is used in LWC
  const parsed = {
    ...data,
    optSelected
  };
  return parsed;
};

const template = document.createElement("template");
const style = `
  <style>
    ${resetCss}
    ${cssClasses}
    ${donutCss}
    ${selectInputCss}

    ${styles}
  </style>
`;

const getTemplate = data => {
  const parsed = parseData(data);

  return htmlTemplate(parsed);
}

template.innerHTML = /*html*/`
  ${style}
  ${getTemplate(mockData)}
`;

class EditOrder extends HTMLElement {
  constructor() {
    super();
    this.attachShadow({ mode: "open" });
  }

  connectedCallback() {
    this.shadowRoot.appendChild(template.content.cloneNode(true));
  }
}

customElements.define("c-editorder", EditOrder);
