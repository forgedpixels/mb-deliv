export default `
  .edit-order {
    background: #fff;
    height: 100%;
    padding: 1.375rem;
    color: #041C2C;
    text-align: left;
    overflow: auto;
  }

  .edit-order-select-opts {
    display: flex;
    justify-content: flex-start;
    border-bottom: .5px solid #000;
    padding-bottom: 1rem;
    font-size: 1.125rem;
    position: relative;
  }

  .msg-type-dropdown-opts {
    display: none;
  }

  .select-option {
    position: relative;
  }

  .select-option ul button {
    min-width: 110px;
    text-align: left;
  }

  .edit-order h2,
  .option-update,
  .edit-order-select-opts {
    margin-bottom: 1.4rem;
  }

  .edit-order h2 {
    font-size: 1.34rem;
  }

  .select-option > button > .font-light {
    margin-left: .5rem;
  }

  .select-option svg {
    margin-left: .5rem;
    transform: rotate(180deg);
  }

  .select-option svg path {
    fill: #000;
  }

  .option-update {
    margin-bottom: 1.6875rem;
  }

  .option-update input {
    margin-left: 1.35rem;
    text-align: center;
  }

  .comments {
    margin-bottom: 2.6875rem;
  }

  .comments label {
    display: block;
    margin-bottom: .35rem;
  }

  .submit-opts {
    display: flex;
    justify-content: flex-start;
  }

  @media (min-width: 1290px) {
    .submit-opts input {
      max-width: 9rem;
    }
  }
`;