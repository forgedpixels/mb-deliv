export default data => `
<div class="edit-order">
  <h2>Edit your order</h2>

  <div class="edit-selection edit-selection-step1">
    <div class="edit-order-select-opts">
      <span>Select an option</span>
      <div class="select-option">
        <button>
          <span class="font-light">Select</span>
          ${data.images.chevronIcon}
        </button>

        <div class="msg-type-dropdown-opts menu-box-shadow-on-lg select-dropdown-absolute select-dropdown-styled-unfixed select-dropdown-styled pos-abs-all-right ${data.showEditOrderOpts}">
          <ul>
            <li><button>General Concern</button></li>
            <li><button>Adjust quantity</button></li>
          </ul>
        </div>
      </div>
    </div>

    
    
    <div class="edit-selection edit-selection-step2 ${data.optSelected ? 'show': ''}">
      <div class="option-update">
        <div>
          <label for="">Enter new case quantity</label>
          <input type="text" value="6" size=3 />
        </div>
      </div>
      
      <div class="comments">
        <label for="comments">Comments</label>
        <textarea name="comments"></textarea>
      </div>

      <div class="submit-opts">
        <input type="submit" value="Update" class="btn primary-cta-btn" />
        <input type="button" value="Cancel" class="btn cancel-btn" />
      </div>
    </div>
  </div>
</div>
`;