export default `
  .component-bg {
    padding: 1.8125rem;
    height: 100%;
  }

  .inbox-stats {
    display: block;
  }

  .unread {
    font-weight: normal;
  }

  .msg > div {
    padding-top: 1rem;
  }

  .msgs li {
    padding-bottom: 1rem;
    border-bottom: 1px solid rgba(229, 229, 229, 0.25);
  }

  .msgs li:last-child {
    border-bottom: 0;
  }

  .msgs p {
    font-size: 0.875rem;
  }

  .content-to-bottom {
    padding: 0 1.8125rem;
  }

  
  .flip-container {
    perspective: 1000px;
    min-height: 700px;
  }
    /* flip the pane when hovered */
    .flip-container.flip .flipper,
    .flip-container.flip .flipper {
      transform: rotateY(180deg);
    }
    
  
  .flip-container,
  .front,
  .back {
    width: 100%;
    height: 100%;
  }
  
  /* flip speed goes here */
  .flipper {
    transition: 0.6s;
    transform-style: preserve-3d;
    height: 100%;
    position: relative;
    min-height: 700px;
  }
  
  /* hide back of pane during swap */
  .front,
  .back {
    backface-visibility: hidden;
    position: absolute;
    top: 0;
    left: 0;
    min-height: 700px;
  }
  
  /* front pane, placed above back */
  .front {
    z-index: 2;
    /* for firefox 31 */
    transform: rotateY(0deg);
  }
  
  /* back, initially hidden pane */
  .back {
    transform: rotateY(180deg);
  }
`;