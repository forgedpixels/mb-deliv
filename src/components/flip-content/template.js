const createMsgs = (data = {msgs: []}) => {
  return data.msgs.map((val) => `
  <li>
  <div class="msg">
    <div class="content-to-ends content-base-vert">
      <span class="msg-from"><strong class="msg-type">${val.type}</strong> <span class="msg-sender font-light">${val.from}</span></span>
      <button>
      ${data.images.msgReplyIcon}
      </button>
    </div>
    <p class="font-light">${val.msg}</p>
  </div>
</li>
  `).join('');
};


export default (data = {}) => `
<div class="flip-container">
  <div class="flipper">
    <div class="front">
      <!-- front content -->
      <c-messages-snapshot></c-messages-snapshot>
    </div>
    <div class="back">
      <!-- back content -->
      <c-priorities></c-priorities>
    </div>
  </div>
</div>
`;