import resetCss from '../../assets/styles/reset.js';
import cssClasses from '../../assets/styles/classes.js';
import addIcon from '../../assets/images/icons/add.js';
import arrowIcon from '../../assets/images/icons/arrow-long.js';
import msgReplyIcon from '../../assets/images/icons/msg-reply.js';
import flipIcon from '../../assets/images/icons/flip.js';
import styles from './styles.js';
import htmlTemplate from './template.js';

const mockData = {
  images: {
    addIcon,
    arrowIcon,
    msgReplyIcon,
    flipIcon
  },
  msgs: [
    {
      status: 'read',
      type: 'PLANNER',
      from: 'D. GOODWIN',
      msg: 'Leverage agile frameworks to provide a robust synopsis for level...'
    },
    {
      status: 'new',
      type: 'STORE 305',
      from: 'Queensway',
      msg: 'Bring to the table win-win survival strategies to ensure proactive...'
    },
    {
      status: 'new',
      type: 'REP',
      from: 'H.HART',
      msg: 'Override the digital divide with additional clickthroughs from DevOps.'
    },
    {
      status: 'new',
      type: '07.28.20',
      from: '7281130',
      msg: 'User generated content in real-time will have multiple touchpoints for offshoring.'
    },
    {
      status: 'read',
      type: '07.24.20',
      from: '7281116',
      msg: 'Capitalize on low hanging fruit to identify a ballpark value added activity to...'
    },
    {
      status: 'read',
      type: '07.15.20',
      from: '7281116',
      msg: 'Capitalize on low hanging fruit to identify a ballpark value added activity to...'
    },
    {
      status: 'new',
      type: '07.10.20',
      from: '7281116',
      msg: 'Capitalize on low hanging fruit to identify a ballpark value added activity to...'
    }
  ]
};

const template = document.createElement("template");
const style = `
  <style>
    ${resetCss}
    ${cssClasses}

    ${styles}
  </style>
`;
const parseData = data => {
  return {
    ...data,
    msgs: data.msgs.slice(0, 5)
  }
}

template.innerHTML = /*html*/`
  ${style}
  ${htmlTemplate(parseData(mockData))}
`;

class FlipContent extends HTMLElement {
  constructor() {
    super();
    this.attachShadow({ mode: "open" });
  }

  connectedCallback() {
    this.shadowRoot.appendChild(template.content.cloneNode(true));
    let buttons = this.shadowRoot.querySelectorAll("footer button");

    buttons.forEach((elem) => {
      elem.onclick = () => this.toggleFlip();
    });

    window.addEventListener('flippanel', () => {
      this.toggleFlip();
    }, false);
  }

  toggleFlip() {
    this.shadowRoot.querySelector(".flip-container").classList.toggle('flip')
  }
}

customElements.define("c-flipcontent", FlipContent);
