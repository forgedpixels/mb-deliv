export default `
  .messages {
    background: #fff;
    color: #041C2C;
    padding: 1rem 2rem;
  }

  .messages.selected .sort-messages {
    display: none;
  }

  .sort-messages {
    display: flex;
    margin-bottom: 1.5rem;
    font-size: 0.875rem;
    justify-content: flex-end;
  }

  .sort-messages svg {
    transform: rotate(180deg);
  }

  .sort-messages svg path {
    fill: #000;
  }

  .sort-msg-opts {
    display: none;
    position: fixed;
    bottom: 0;
    left: 0;
    width: 100%;
    background: #F5F5F5;
    z-index: 2;
  }

  .sort-msg-opts.open {
    display: block;
  }

  .sort-msg-opts button {
    width: 100%;
    text-align: left;
  }

  .sort-msg-opts button,
  .sort-msg-opts li.selected .sort-name {
    cursor: pointer;
    font-size: 1rem;
  }

  .sort-msg-opts li.selected .sort-name {
    font-weight: bold;
    text-decoration: underline;
  }

  .msg-sender,
  .msg-received-time {
    position: relative;
    font-size: 1.125rem;
  }


  .msg-received-time {
    text-align: right;
    max-width: 7rem;
  }

  .msg-received-time {
    color: #919ba1;
  }

  a.reply-to {
    display: block;
    font-size: 1rem;
    text-decoration: underline;
  }
  

  .msg-sender {
    margin-bottom: 1rem;
    text-transform: uppercase;
  }

  .msg-overview {
    font-size: .875rem;
  }

  .sender-type,
  .msg-subject {
    font-weight: bold;
  }

  ol li {
    transition: .35s;
  }

  .message-list li header {
    position: relative;
  }

  .message-list li {
    padding-top: 1.5rem;
    padding-bottom: 1.5rem;
    border-bottom: 2px solid #E5E5E5;
  }

  .message-list li:first-child {
    padding-top: 0;
  }

  .message-list li:last-child {
    border-bottom: 0;
  }

  .msg-preview {
    position: relative;
    line-height: 1.4rem;
    display: -moz-box;
    display: -webkit-box;
    -webkit-line-clamp: 3;
    -moz-line-clamp: 3;
    line-clamp: 3;
    -webkit-box-orient: vertical;
    -moz-box-orient: vertical;
    box-orient: vertical;
    overflow: hidden;
  }

  @media (min-width: 1290px) {
    .msg-sender,
    .msg-received-time {
      font-size: 1.5rem;
      background: #fff;
    }

    .message-list li:first-child {
      padding-top: 0.75rem;
    }

    .msg-sender {
      padding-right: .5rem;
    }
    
    .msg-received-time {
      padding: 0 .5rem;
      max-width: 9rem;
    }

    .msg-overview {
      font-size: 1.125rem;
    }

    .msg-preview {
      position: relative;
      line-height: 1.4rem;
      display: -moz-box;
      display: -webkit-box;
      -webkit-line-clamp: 2;
      -moz-line-clamp: 2;
      line-clamp: 2;
      -webkit-box-orient: vertical;
      -moz-box-orient: vertical;
      box-orient: vertical;
      overflow: hidden;
      max-width: 33.5rem;
    }

    ol:hover li,
    .messages.selected ol li {
      opacity: .3;
    }

    ol:hover li:hover,
    ol:hover li:hover .msg-received-time,
    .messages.selected ol li.selected,
    .messages.selected ol li.selected .msg-received-time {
      color: #0099FF;
      opacity: 1;
    }

    .messages.selected ol li.selected {
      color: #000;
    }

    ol:hover li:hover,
    ol:hover li:hover .msg-received-time {
      cursor: pointer;
    }
    
    ol:hover li:hover header:before,
    ol li.selected header:before {
      content: ' ';
      border: 1px solid #0099FF;
      width: 100%;
      position: absolute;
      top: .75rem;
      right: -2rem
    }
  }

  .see-all {
    display: none;
    text-align: center;
    margin-top: 1.5rem;
  }

  .see-all a {
    font-size: 1rem;
    text-decoration: underline;
    color: #000;
  }


  /* styles when this component is in the header nav */
  .msgs-in-header.messages {
    padding: 1rem 0;
  }

  .msgs-in-header.messages .sort-messages {
    display: none;
  }

  .msgs-in-header.messages .msg-sender,
  .msgs-in-header.messages .msg-received-time {
    font-size: 1.125rem;
  }
  
  .msgs-in-header.messages .msg-overview {
    font-size: 0.875rem;
  }

  .msgs-in-header.messages ol:hover li:hover header:before,
  .msgs-in-header.messages ol li.selected header:before {
    display: none;
  }

  .msgs-in-header.messages .see-all {
    display: block;
  }
`;