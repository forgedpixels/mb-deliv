const createMsgs = (data = {msgs: []}) => {
  return data.msgs.map((val, i) => `
  <li class="${i === 0 ? data.selectedMsgClass : ''}"> <!-- in LWC check which element is actually selected -->
    <header class="content-to-ends">
      <div class="msg-sender">
        <span class="sender-type">${val.senderType}</span> <span class="sender-name font-light">${val.senderName}</span>
      </div>
      <div class="msg-received-time">
        <span class="time font-light">${val.time}</span>
        ${i === 3 ? `<a class="reply-to bright-blue" href="mailto:${val.senderEmail}">Reply</a>` : ''} <!-- i === 3 To be TRUE if it matches certain criteria, to be done in LWC -->
      </div>
    </header>
    <section class="msg-overview font-light">
      <span class="msg-subject">${val.subjectName}</span>
      <p class="msg-preview">${val.msgBody}</p>
    </section>
  </li>
  `).join('');
};

const getSortOpts = data => {
  return data.sortOpts.map((val) => {
    if (val.selected) {
      return `
        <li class="selected">
          <span><span class="sort-name">${val.sortName}</span> ${val.sortName !== 'All' ? `<span class="bright-blue">${val.amt}</span>` : ''}</span>
        </li>
      `;
    }
    return `
      <li>
        <button><span class="sort-name">${val.sortName}</span> ${val.sortName !== 'All' ? `<span class="bright-blue">${val.amt}</span>` : ''}</button>
      </li>
    `
  }).join('');
}

const showSeeAll = show => {
  if (show) {
    return `
    <div class="see-all">
      <a href="#">See All</a>
    </div>`;
  }
  return '';
}

export default (data = {}) => `
  <div class="messages ${data.selectedMsgClass} ${data.inHeaderClass}">
    <div class="sort-messages">
      <button class="text-and-chevron-label">
        <span>Sort by:</span>
        <span class="text-and-chevron">All ${data.images.chevronIcon}</span>
      </button>
      <div class="sort-msg-opts select-dropdown-styled menu-box-shadow-on-lg ${data.sortOptsOpenClass}">
        <ul>
          ${getSortOpts(data)}
        </ul>
      </div>
    </div>
    <div class="message-list">
      <ol>
        ${createMsgs(data)}
      </ol>
      ${showSeeAll(data.showViewAll)}
    </div>
  </div>
`;