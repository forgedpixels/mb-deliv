import resetCss from '../../assets/styles/reset.js';
import cssClasses from '../../assets/styles/classes.js';
import chevronIcon from '../../assets/images/icons/chevron.js';
import styles from './styles.js';
import htmlTemplate from './template.js';

let mockData = {
  images: {
    chevronIcon
  },
  sortOpts: [
    {
      sortName: 'All',
    },
    {
      sortName: 'Collection Request',
      amt: 1
    },
    {
      sortName: 'Delivery satisfaction',
      selected: true,
      amt: 3
    },
    {
      sortName: 'Restaurant complaint',
      amt: 72
    },
    {
      sortName: 'Unassigned',
      amt: 11
    }
  ],
  msgs: [
    {
      senderType: 'MB Planner',
      senderName: 'D. Goodwin',
      senderEmail: 'test@test.com',
      time: '8:19a',
      subjectName: 'General Concern - Test 2 28 09',
      msgBody: 'Your Martin Brower UK call has been allocated Reference Number: REQ3448678. We have logged your call and. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Enim ut tellus elementum sagittis vitae. Praesent semper feugiat nibh sed pulvinar proin gravida. '
    },
    {
      senderType: 'Product Complaint System',
      senderName: '',
      senderEmail: 'test@test.com',
      time: '6:21a',
      subjectName: 'Quality Feedback Product Complaint',
      msgBody: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sit amet nisl purus in mollis nunc sed id. Sed nisi lacus sed viverra tellus in hac. Habitant morbi tristique senectus et netus et malesuada. Convallis convallis tellus id interdum velit laoreet id. Commodo elit at imperdiet dui accumsan. Ipsum faucibus vitae aliquet nec ullamcorper. Pulvinar elementum integer enim neque volutpat ac tincidunt vitae semper. Donec enim diam vulputate ut pharetra sit. Nisl purus in mollis nunc sed id. Netus et malesuada fames ac turpis egestas. Mauris vitae ultricies leo integer malesuada nunc vel. Risus nullam eget felis eget. Pellentesque diam volutpat commodo sed egestas egestas fringilla. Varius morbi enim nunc faucibus a pellentesque sit amet. Duis at consectetur lorem donec massa sapien faucibus et molestie. In est ante in nibh mauris cursus mattis molestie a.'
    },
    {
      senderType: '1222',
      senderName: 'Keighley (2)',
      senderEmail: 'test@test.com',
      time: '6:21a',
      subjectName: 'Deliver & Drive for Restaurant 1222',
      msgBody: 'Order: 98495301 / Delivery date: 21.10.20'
    },
    {
      senderType: '1222',
      senderName: 'Keighley (3)',
      senderEmail: 'test@test.com',
      time: 'Recieved 3 days ago',
      subjectName: 'Deliver & Drive for Restaurant 1222',
      msgBody: 'Order: 98495301 / Delivery date: 21.10.20'
    }
  ]
};

class MailMsgs extends HTMLElement {
  constructor() {
    super();
    const style = `
      <style>
        ${resetCss}
        ${cssClasses}

        ${styles}
      </style>
    `;
    this.attachShadow({ mode: "open" });
    this.inheader = this.getAttribute("inheader");
    this.template = document.createElement("template");
    this.template.innerHTML = `
    ${style}

    ${htmlTemplate(this.parseData(mockData))}
    `;
  }

  parseData(data) {
    const urlParams = new URLSearchParams(window.location.search);
    const inHeader = this.getAttribute("inheader");
    
    let newData = {
      ...data,
      inHeaderClass: inHeader ? 'msgs-in-header' : '',
      selectedMsgClass: urlParams.get('design') === '2' || urlParams.get('design') === '3' ? 'selected' : '',
      sortOptsOpenClass: urlParams.get('design') === '1' ? 'open' : '',
    };

    if (inHeader) {
      newData.showViewAll = data.msgs.length > 3,
      newData.msgs = data.msgs.slice(0,3); // NOTE only show max 3 in hover state
    }

    return newData;
  }
  

  connectedCallback() {
    this.shadowRoot.appendChild(this.template.content.cloneNode(true));
  }
}

customElements.define("c-mailmsgs", MailMsgs);
