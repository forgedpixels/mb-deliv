import resetCss from '../../assets/styles/reset.js';
import cssClasses from '../../assets/styles/classes.js';
import breadcrumbCss from '../../assets/styles/breadcrumbs.js';
import styles from './styles.js';
import htmlTemplate from './template.js';

const createMockData = () => {
  let mockData = {
    breadcrumbs: [
      {
        linkName: "Home",
        linkUrl: "http://home.com"
      },
    ]
  };

  if (window.location.href.indexOf('orders-selected.html') >= 1) {
    mockData.breadcrumbs = [
      ...mockData.breadcrumbs,
      {
        linkName: 'Orders',
        linkUrl: "https://test.com"
      },
      {
        linkName: '7281169',
        linkUrl: "https://test.com"
      }
    ]
  } else if (window.location.href.indexOf('orders.html') >= 1) {
    mockData.breadcrumbs = [
      ...mockData.breadcrumbs,
      {
        linkName: 'Orders',
        linkUrl: "https://test.com"
      }
    ]
  } else if (window.location.href.indexOf('messages.html') >= 1|| window.location.href.indexOf('message.html') >= 1) {
    mockData.breadcrumbs = [
      ...mockData.breadcrumbs,
      {
        linkName: "Messages",
        linkUrl: "http://home.com"
      },
    ];
  }

  return mockData;
};

const template = document.createElement("template");

template.innerHTML = /*html*/`
  <style>
    ${resetCss}
    ${cssClasses}
    ${breadcrumbCss}

    ${styles}
  </style>
  
  ${htmlTemplate(createMockData())}
`;

class Breadcrumbs extends HTMLElement {
  constructor() {
    super();
    this.attachShadow({ mode: "open" });
  }

  connectedCallback() {
    this.shadowRoot.appendChild(template.content.cloneNode(true));
  }
  
}

customElements.define("c-breadcrumbs", Breadcrumbs);
