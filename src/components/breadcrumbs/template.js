const createBreadcrumbs = (data = {breadcrumbs: []}) => {
  return data.breadcrumbs.map((val, i) => {
    if (data.breadcrumbs.length === i + 1) {
      return `<li><a href="#">${val.linkName}</a></li>`;
    }
    return `<li>${val.linkName}</li>`;
  }).join('');
};

export default data => `
  <div class="breadcrumbs content-max-and-center">
    <nav>
      <ul class="breadcrumb">
        ${createBreadcrumbs(data)}
      </ul>
    </nav>
  </div>
`;