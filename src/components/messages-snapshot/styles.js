export default `
  .component-bg {
    padding: 1.8125rem;
    height: 100%;
    min-height: 590px;
  }

  .inbox-stats {
    display: block;
  }

  .unread {
    font-weight: normal;
  }

  .msg > div {
    padding-top: 1rem;
  }

  .msgs li {
    padding-bottom: 1rem;
    border-bottom: 1px solid rgba(229, 229, 229, 0.25);
  }

  .msgs li:last-child {
    border-bottom: 0;
  }

  .msgs p {
    font-size: 0.875rem;
    position: relative;
    display: -moz-box;
    display: -webkit-box;
    -webkit-line-clamp: 2;
    -moz-line-clamp: 2;
    line-clamp: 2;
    -webkit-box-orient: vertical;
    -moz-box-orient: vertical;
    box-orient: vertical;
    overflow: hidden;
  }

  .content-to-bottom {
    padding: 0 1.8125rem;
  }
  
  .component-bg {
    position: relative;
  }

  footer {
    position: absolute;
    bottom: 0;
    width: 100%;
    left: 0;
    padding: 1.25rem;
  }
`;