const createMsgs = (data = {msgs: []}) => {
  return data.msgs.map((val) => `
  <li>
  <div class="msg">
    <div class="content-to-ends content-base-vert">
      <span class="msg-from"><strong class="msg-type">${val.type}</strong> <span class="msg-sender font-light">${val.from}</span></span>
      <button>
      ${data.images.msgReplyIcon}
      </button>
    </div>
    <p class="font-light">${val.msg}</p>
  </div>
</li>
  `).join('');
};


export default (data = {}) => `
  <div class="component-bg mail-bg">
    <header class="component-header content-to-ends content-base-vert">
      <span>
        Messages
        <span class="inbox-stats">
          <span class="bright-blue">16 New</span> / <span class="unread">3 Unread</span>
        </span>
      </span>
      
      <a href="" class="new-msg">${data.images.addIcon}</a>
    </header>
    
    <div class="msgs">
      <ul>
        ${createMsgs(data)}
      </ul>
    </div>

    <footer class="content-to-ends ">
      <button>
        ${data.images.flipIcon}
      </button>

      <a href="" class="arrow-link">Message Centre ${data.images.arrowIcon}</a>
    </footer>
  </div>
`;