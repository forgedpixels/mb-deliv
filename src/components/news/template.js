export default (data) => `
  <div class="component-bg news-bg">
    <header class="component-header content-to-ends content-base-vert">
      <span>News</span>

      <a href="" class="header-end-link header-view-all-link">View all</a>
    </header>
    
    <p>${data.news.desc}</p>

    <div class="news-data">
      <span class="news-network">${data.news.network}</span> - <span class="news-date">${data.news.date}0</span>
    </div>
  </div>
`;