export default `
  .component-bg {
    padding: 1.8125rem;
    height: 100%;
  }

  .component-bg > p {
    margin-bottom: 0.375rem;
    text-transform: uppercase;
    font-size: 0.875rem;
  }

  .news-data {
    font-size: 0.75rem;
  }

  @media (min-width: 1290px) {
    .component-bg {
      padding: 1.25rem;
    }
  }
`;