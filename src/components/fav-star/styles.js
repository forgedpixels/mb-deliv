export default `
  .fav-star path {
    transition: .4s;
  }

  .fav-star.selected path {
    fill: #e2e288;
    fill-opacity: 1;
  }
`;