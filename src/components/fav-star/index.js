import resetCss from '../../assets/styles/reset.js';
import cssClasses from '../../assets/styles/classes.js';
import styles from './styles.js';
import htmlTemplate from './template.js';
import favIcon from '../../assets/images/icons/fav-star.js';

const mockData = {
  images: {
    favIcon
  },
  orderNo: 123
};

const template = document.createElement("template");
const style = `
  <style>
    ${resetCss}
    ${cssClasses}

    ${styles}
  </style>
`;

template.innerHTML = /*html*/`
  ${style}
  ${htmlTemplate(mockData)}
`;

class FavStar extends HTMLElement {
  constructor() {
    super();
    this.attachShadow({ mode: "open" });
  }

  connectedCallback() {
    this.shadowRoot.appendChild(template.content.cloneNode(true));
    this.shadowRoot.querySelector(".fav-star button").onclick = () => this.toggleFav();
  }

  toggleFav() {
    this.saveAndRemoveOrderNo();
    this.shadowRoot.querySelector(".fav-star").classList.toggle('selected')
  }

  saveAndRemoveOrderNo() {
    const orderNo = this.shadowRoot.querySelector(".fav-star button").getAttribute('data-orderno');
    if (localStorage.getItem('orderNo')) {
      localStorage.removeItem('orderNo');
    } else {
      localStorage.setItem('orderNo', orderNo);
    }
  }
}

customElements.define("c-favstar", FavStar);
