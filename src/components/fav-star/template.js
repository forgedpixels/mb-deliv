export default (data = {}) => `
  <div class="fav-star">
    <button data-orderno="${data.orderNo}">
      ${data.images.favIcon}
    </button>
  </div>
`;