import resetCss from '../../assets/styles/reset.js';
import cssClasses from '../../assets/styles/classes.js';
import extraOptsDropdown from '../../assets/styles/extra-opts-dropdown.js';
import donutCss from '../../assets/styles/donut.js';
import emptyStyles from './styles.js';
import msgDetailStyles from './style-msg-detail.js';
import replyStyles from './styles-reply.js';
import dataTemplate from './data-template.js';
import defaultTemplate from './default-template.js';
import paperClipIcon from '../../assets/images/icons/paperclip.js';
import closeIcon from '../../assets/images/icons/close.js';
import closeThinIcon from '../../assets/images/icons/close-thin.js';
import chevronIcon from '../../assets/images/icons/chevron.js';
import replyIcon from '../../assets/images/icons/reply.js';
import heartIcon from '../../assets/images/icons/heart.js';
import dotIcon from '../../assets/images/icons/dot-menu.js';
import printerIcon from '../../assets/images/icons/printer.js';
import shareIcon from '../../assets/images/icons/share.js';
import archiveIcon from '../../assets/images/icons/archive.js';
import calendarIcon from '../../assets/images/icons/calendar.js';
import filterIcon from '../../assets/images/icons/filter.js';
import mailIcon from '../../assets/images/icons/mail.js';

localStorage.removeItem('editing-order');

const mockData = {
  images: {
    paperClipIcon,
    closeIcon,
    chevronIcon,
    closeThinIcon,
    replyIcon,
    heartIcon,
    dotIcon,
    printerIcon,
    shareIcon,
    archiveIcon,
    filterIcon,
    calendarIcon,
    mailIcon
  },
  msg: {
    msgConversation: [
      {
        senderType: 'MB Planner',
        senderName: 'D. Goodwin',
        to: [
          {
            name: 'John Doe',
            emailAddress: 'john@test.com'
          },
          {
            name: 'Jane Doe',
            emailAddress: 'jane@test.com'
          }
        ],
        time: '8:19a',
        ticketLink: 'http://mbsite.com/?ticket=123',
        subjectName: 'General Concern - Test 2 28 09',
        msgBody: '<p>Your Martin Brower UK call has been allocated Reference Number:-  Req3448676</p><p>We have logged your call and have allocated it to Anu Subash. The description of the problem is as follows: </p><p>PlainText : Test 2 28.09 {-}MBSYNC|822515{-}</p><p>You will receive a confirmation e-mail when the issue has been resolved.  </p><p>We will endeavour to resolve your query as quickly as possible and expect this to be done no later than 14/10/2020 03:56 PM</p>'
      },
      {
        senderType: 'MB No Reply Martin-Brower',
        senderName: '',
        to: [
          {
            name: 'D. Goodwin',
            emailAddress: 'john@test.com'
          }
        ],
        time: 'Yesterday',
        subjectName: 'General Concern - Test 2 28 09',
        msgBody: '<p>super small sample size</p><p>yeah this is a test :D</p>'
      },
      {
        senderType: 'MB Planner',
        senderName: 'John Doe',
        to: [
          {
            name: 'D. Goodwin',
            emailAddress: 'john@test.com'
          }
        ],
        time: 'Nov 20, 2020 (8 days ago)',
        subjectName: 'General Concern - Test 2 28 09',
        msgBody: '<p>super small sample size</p><p>yeah this is a test :D</p>'
      },
      {
        senderType: 'MB Planner',
        senderName: 'John Doe',
        to: [
          {
            name: 'D. Goodwin',
            emailAddress: 'john@test.com'
          }
        ],
        time: 'Nov 15, 2020 (12 days ago)',
        subjectName: 'General Concern - Test 2 28 09',
        msgBody: '<p>super small sample size</p><p>yeah this is a test :D</p>'
      }
    ]
  }
};


const template = document.createElement("template");
const style = `
  <style>
    ${resetCss}
    ${cssClasses}
    ${donutCss}
    ${extraOptsDropdown}

    ${emptyStyles}
    ${msgDetailStyles}
    ${replyStyles}
  </style>
`;

const parseData = data => {
  const urlParams = new URLSearchParams(window.location.search);
  const hasTicketLink = data.msg.msgConversation.filter(val => val.ticketLink) // NOTE: assuming not all the messages have a ticket link

  let newData = {
    ...data,
    replyingToConvo: urlParams.get('design') === '3',
    ticketLink: hasTicketLink[0] ? hasTicketLink[0].ticketLink : false
  }

  // NOTE: to be grabbed from whatever state management you are using, example is using '3rd person' in the conversation from mockData object
  if (urlParams.get('design') === '3') {
    newData['replyingTo'] = {
      senderType: 'MB Planner',
      senderName: 'John Doe',
      to: [
        {
          name: 'D. Goodwin',
          emailAddress: 'john@test.com'
        }
      ],
      time: 'Nov 20, 2020 (8 days ago)',
      subjectName: 'General Concern - Test 2 28 09',
      msgBody: '<p>super small sample size</p><p>yeah this is a test :D</p>'
    }
  }
  return newData;
};

const getTemplate = data => {
  const urlParams = new URLSearchParams(window.location.search);

  if (urlParams.get('design') === '2' || urlParams.get('design') === '3') {
    data.showExtraOpts = urlParams.get('design') === '2' ? 'show' : '';
    return dataTemplate(data)
  }
  
  return defaultTemplate;
}

template.innerHTML = /*html*/`
  ${style}
  ${getTemplate(parseData(mockData))}
`;

class MsgDetail extends HTMLElement {
  constructor() {
    super();
    this.attachShadow({ mode: "open" });
  }

  connectedCallback() {
    this.shadowRoot.appendChild(template.content.cloneNode(true));
  }
}

customElements.define("c-msg-detail", MsgDetail);
