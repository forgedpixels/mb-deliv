export default `
  .msg-nav-opts {
    display: flex;
    padding: 0 1.35rem;
    justify-content: flex-end;
    margin-top: -3.25rem;
    position: absolute;
    top: 0;
    right: 0;
  }

  .msg-nav-opts button {
    margin-left: 1rem;
    cursor: pointer;
  }

  .msg-nav-opts .prev svg,
  .msg-nav-opts .next svg {
    width: 1.25rem;
    height: 1.25rem;
    opacity: .5;
  }

  .msg-nav-opts .prev svg{
    transform: rotate(-90deg)
  }
  .msg-nav-opts .next svg {
    transform: rotate(90deg)
  }

  .sent-to {
    display: none;
  }

  .ticket-btn {
    display: none;
  }

  ol > li .msg-body .ticket-btn {
    display: inline-block;
  }

  .conversation-reply-content + ol > li .ticket-btn, 
  .conversation-reply-content + .msg-nav-opts + ol > li .msg-opts-menu,
  .conversation-reply-content + .msg-nav-opts + ol > li .msg-body .ticket-btn {
    display: none;
  }

  .conversation-reply-content + .msg-nav-opts + ol > li .msg-sender,
  .conversation-reply-content + .msg-nav-opts + ol > li .msg-subject {
    font-size: 1.25rem;
  }

  .msg-received-time {
    font-size: .67rem;
  }

  .msg-received-time,
  .msg-subject,
  .msg-body p {
    margin-bottom: 1rem;
  }

  .msg-subject {
    display: block;
    margin-bottom: .67rem;
  }

  li .msg-sender,
  li .msg-subject {
    font-size: .75rem;
  }

  li:first-child .msg-sender,
  li:first-child .msg-subject {
    font-size: 1.5rem;
  }

  .sender-type {
    font-weight: bold;
  }

  @media (min-width: 1290px) {
    .msg-opts-menu {
      position: absolute;
      right: 1rem;
      width: 2rem;
    }

    ol > li .msg-body .ticket-btn {
      display: none;
    }

    ol > li {
      padding-right: 4.875rem;
    }

    .extra-opts {
      right: 2rem;
      top: calc(100% - 2.5rem);
    }

    .msg-detail {
      position: relative;
      text-align: left;
      font-size: 1rem;
    }

    .msg-detail header {
      display: flex;
      justify-content: space-between;
      margin-bottom: 2.5rem;
    }

    .msg-received-time {
      order: 2;
      font-size: .875rem;
    }

    li:first-child .ticket-btn {
      display: inline-block;
    }

    .conversation-reply-content + .msg-nav-opts + ol > li .ticket-btn {
      display: none;
    }

    .ticket-link,
    li:first-child .ticket-link,
    .msg-subject,
    .sender-type.has-sender-name {
      display: none;
    }

    .sent-to {
      display: block;
      font-size: 0.875rem;
    }

    .sent-to a {
      color: #000;
    }

    .sender-name,
    .sender-type {
      font-size: 1.125rem;
      font-weight: bold;
    }
  }
`;