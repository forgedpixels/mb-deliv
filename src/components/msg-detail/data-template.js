const getSentTo = sentTo => {
  return sentTo.map((val, i) => `
    <a href="mailto:${val.emailAddress}">${val.name}</a>
  `).join('');
}

const getReplyTo = data => {
  if (data.replyingToConvo) {
    return `
      <section class="conversation-reply-content">
        <div>
          ${data.ticketLink ? `<a href="${data.ticketLink}" class="ticket-btn outline-btn">View Ticket</a>` : ''}
          <h1>Replying to ${data.replyingTo.senderType} ${data.replyingTo.senderName}</h1>

          <c-msg-compose-area></c-msg-compose-area>
        </div>
      </section>
    `;
  }
  return '';
}

const getConversation = data => {
  return data.msg.msgConversation.map((val, i) => `
    <li>
      ${i === 0 ? `
        <div class="msg-opts-menu">
          <div class="quick-opts">
            <button><c-favstar></c-favstar></button>
            <button>${data.images.replyIcon}</button>
            <button>${data.images.heartIcon}</button>
            <button class="dots">${data.images.dotIcon}</button>
          </div>
          <div class="extra-opts ${data.showExtraOpts}">
            <div class="menu-box-shadow-on-lg select-dropdown-styled">
              <ul>
                <li>
                <button class="forward">${data.images.replyIcon} <span>Forward</span></button>
                </li>
                <li>
                <button>${data.images.printerIcon} <span>Print</span></button>
                </li>
                <li>
                <button class="mail">${data.images.mailIcon} <span>Mark as unread</span></button>
                </li>
                <li>
                <button>${data.images.archiveIcon} <span>Archive</span></button>
                </li>
                <li>
                <button>${data.images.shareIcon} <span>Share</span></button>
                </li>
                <li>
                <button class="calendar">${data.images.calendarIcon} <span>Add event</span></button>
                </li>
                <li>
                <button>${data.images.filterIcon} <span>Filter Similar</span></button>
                </li>
              </ul>
            </div>
          </div>
        </div>
      ` : ''}
      ${val.ticketLink ? `<a href="${val.ticketLink}" class="ticket-btn outline-btn">View Ticket</a>` : ''}
      <header>
        <div class="msg-received-time">
          <span class="time font-light">${val.time}</span>
        </div>
        <div class="msg-sender">
          <span class="sender-type ${val.senderName ? 'has-sender-name' : ''}">${val.senderType}</span> <span class="sender-name font-light">${val.senderName}</span>
          <span class="sent-to">To ${getSentTo(val.to)}</span>
        </div>
      </header>
      <section class="msg-body font-light">
        <span class="msg-subject">General Concern - Test 2</span>
        ${val.ticketLink ? `<a href="${val.ticketLink}" class="ticket-btn outline-btn">View Ticket</a>` : ''}
        <div class="msg-body-txt">
          ${val.msgBody}
        </div>
      </section>
    </li>
  `).join('');
};

export default data => `
  <div class="msg-detail">
    ${getReplyTo(data)}
    <div class="msg-nav-opts">
      <button class="prev">${data.images.chevronIcon}</button>
      <button class="next">${data.images.chevronIcon}</button>
      <button class="close">${data.images.closeThinIcon}</button>
    </div>
    <ol>
      ${getConversation(data)}
    </ol>
  </div>
`;