export default `
  .conversation-reply-content {
    background: #fff;
    padding: 0 1.875rem;
    color: #000;
  }

  .conversation-reply-content > div {
    padding-top: 1.875rem;
    padding-bottom: 1.5625rem;
    border-bottom: 1px dashed #000000;
  }

  .conversation-reply-content .ticket-btn {
    display: inline-block;
  }

  .conversation-reply-content h1 {
    font-size: 1rem;
  }
`;