export default `
  .msg-empty {
    background: #F5F5F5;
    display: flex;
    align-items: center;
    height: 100%;
    justify-content: center;
    color: #D8D8D8;
    text-align: center;
  }

  .msg-empty p {
    font-size: 2.25rem;
    font-weight: bold;
    max-width: 13rem;
    padding: 2rem 0;
  }

  .close-btn {
    position: absolute;
    top: 4rem;
    right: .85rem;
  }

  h1 {
    font-size: 1.5rem;
    margin-bottom: 1rem;
  }


  @media (min-width: 1290px) {
    .close-btn {
      top: 1rem;
      right: 1rem;
    }
    
    .msg-empty,
    .msg-detail {
      /*margin-top: -5rem;*/
    }

    .edit-order {
      margin-top: 2rem;
      padding-top: 0;
      background: #fff;
    }
  }
`;