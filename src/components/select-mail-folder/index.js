import resetCss from '../../assets/styles/reset.js';
import cssClasses from '../../assets/styles/classes.js';
import chevronIcon from '../../assets/images/icons/chevron.js';
import styles from './styles.js';
import htmlTemplate from './template.js';

let data = {
  images: {
    chevronIcon
  },
  opts: {
    selected: 'All',
    categories: [
      'All',
      'Unread',
      'Favorites',
      'Archived',
    ]
  }
};

const template = document.createElement("template");
const style = `
  <style>
    ${resetCss}
    ${cssClasses}

    ${styles}
  </style>
`;

template.innerHTML = /*html*/`
  ${style}
  ${htmlTemplate(data)}
`;

class SelectMailFolder extends HTMLElement {
  constructor() {
    super();
    this.attachShadow({ mode: "open" });
  }

  connectedCallback() {
    this.shadowRoot.appendChild(template.content.cloneNode(true));
  }
}

customElements.define("c-select-mail-folder", SelectMailFolder);
