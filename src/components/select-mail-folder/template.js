
const createSelectOpts = (data = []) => {
  return data.opts.categories.map((val) => {
    return `
      <li ${data.opts.selected === val ? ` class="selected"`: ``} data-val="${val}">
        <button class="font-light">${val}</button>
      </li>
    `;
  }).join('');
};

export default (data) => `
  <div class="select-opts">
    <ul class="all-opts font-light">
      ${createSelectOpts(data)}
    </ul>
  </div>
`;