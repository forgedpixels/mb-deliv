export default `
  .has-status-icon div {
    position: absolute;
    font-weight: bold;
    top: -27%;
  }

  .has-status-icon span {
    display: none;
  }

  .has-status-icon svg {
    width: 2.25rem;
    height: auto;
  }

  .status-ready-to-view {
    color: #98E8C1;
  }

  .status-ready-to-view svg path {
    stroke: #98E8C1;
  }

  .status-out-for-delivery {
    color: #EFCB9B;
  }

  @media (min-width: 1290px) {
    .has-status-icon div {
      position: relative;
      top: 0;
    }

    .has-status-icon span {
      display: inline-block;
    }

    .has-status-icon svg {
      width: 2.7rem;
    }
  

    .has-status-icon svg {
      position: absolute;
      left: -2.75rem;
      top: -.5rem;
    }

    th:nth-child(2) {
      width: 160px;
    }
  }
`;