import resetCss from '../../assets/styles/reset.js';
import cssClasses from '../../assets/styles/classes.js';
import tableClasses from '../../assets/styles/table.js';
import chevronIcon from '../../assets/images/icons/chevron.js';
import truckIcon from '../../assets/images/icons/truck.js';
import eyeIcon from '../../assets/images/icons/eye.js';
import checkIcon from '../../assets/images/icons/check.js';
import orderEditIcon from '../../assets/images/icons/order-edit.js'
import styles from './styles.js';
import htmlTemplate from './template.js';

let mockData = {
  images: {
    chevronIcon,
    orderEditIcon,
    checkIcon,
    truckIcon,
    eyeIcon
  },
  allOrdersData: [
    {
      date: "11.25.20",
      number: 7281169,
      status: 'Ready to view',
      lastUpdated: 'Nov 3'
    },
    {
      date: "11.22.20",
      number: 7281159,
      status: 'Submitted',
      lastUpdated: 'Nov 3'
    },
    {
      date: "11.21.20",
      number: 7181159,
      status: 'Closed',
      lastUpdated: 'Yesterday'
    },
    {
      date: "11.18.20",
      number: 7281139,
      status: 'Delivered',
      lastUpdated: 'Nov 1'
    },
    {
      date: "11.18.20",
      number: 7281139,
      status: 'Submitted',
      lastUpdated: 'Oct 31'
    },
    {
      date: "11.18.20",
      number: 7281139,
      status: 'Out for Delivery',
      lastUpdated: 'Oct 28'
    }
  ]
};

const template = document.createElement("template");
const style = `
  <style>
    ${resetCss}
    ${cssClasses}
    ${tableClasses}

    ${styles}
  </style>
`;

template.innerHTML = /*html*/`
  ${style}
  ${htmlTemplate(mockData)}
`;

class AllOrdersTable extends HTMLElement {
  constructor() {
    super();
    this.attachShadow({ mode: "open" });
    localStorage.removeItem('order-selected');
  }

  connectedCallback() {
    this.shadowRoot.appendChild(template.content.cloneNode(true));
  }
}

customElements.define("c-all-order-table", AllOrdersTable);
