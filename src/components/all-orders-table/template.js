const createRows = (data) => {
  const getStatusRow = status => {
    let className;
    let icon = '';
    switch (status) {
      case 'Ready to view': 
        className = 'status-ready-to-view has-status-icon';
        icon = data.images.eyeIcon
        break;
      case 'Out for Delivery': 
        className = 'status-out-for-delivery has-status-icon';
        icon = data.images.truckIcon
        break;
      default:
    }
    return `<td class="${className}"><div>${icon}<span>${status}</span></div></td>`;
  };

  return data.allOrdersData.map((val) => {
    return `
    <tr class="font-light">
      <td>${val.date}</td>
      <td>${val.number}</td>
      ${getStatusRow(val.status)}
      <td>${val.lastUpdated}</td>
    </tr>
  `
}).join('');
};

export default (data = {}) => `
  <div class="all-orders-table data-table">
    <table>
      <tr>
        <th>Date</th>
        <th>Number</th>
        <th>Status</th>
        <th>Updated</th>
      </tr>
      ${createRows(data)}
    </table>
  </div>
`;