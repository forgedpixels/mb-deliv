export default `
  ul {
    display: flex;
  }

  ul li {
    list-style: none;
  }

  li button {
    color: #fff;
    width: 100%;
    text-align: left;
    font-size: 1rem;
  }

  li.selected button {
    font-weight: bold;
    border-bottom: 2px solid #0099FF;
  }

  .currently-selected {
    font-size: 1.5rem;
  }



  .all-opts {
    display: flex;
    justify-content: space-between;
    max-width: 80%;
    width: 100%;
  }

  @media (min-width: 1290px) {

    .all-opts {
      width: 100%;
      max-width: 550px;
    }
  
    li button {
      font-size: 1.5rem;
    }

    .currently-selected {
      display: none;
    }
  }

`;