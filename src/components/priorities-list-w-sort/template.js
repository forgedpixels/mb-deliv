const createPriorities = data => {
  return data.priorities.map((val, i) => `
  <li class="${i=== 9 ? 'editing' : ''}"> <!-- NOTE: EDITING CLASS SHOULD BE ADDED ONLY ON MOBILE. SHOULD COME FROM USER SWIPE TO BE IMPLEMENTED IN LWC FW -->
  <div class="priority-content">
    <header>    
      <h3 class="${val.priorityType}-priority">${val.starred ? `<span class="starred-icon">${data.images.favStarIcon}</span>` : ''}${val.priorityType === 'high' ? data.images.importantIcon : ''}${val.name}</h3>
      <div class="msg-received">
        <span>${val.time}</span>

        <div class="priority-opts-dots">
          <button>
          ${data.images.dotMenuIcon}
          </button>
          <div class="priority-drop-opts menu-box-shadow-on-lg select-dropdown-styled menu-box-shadow-on-lg ${data.data.isShowingPriorOpts ? 'show': ''}">
            <ul>
              <li class="path-fill-icon"><button>${data.images.favStarIcon}<span>My Priority</button></span></li>
              <li class="high-priority-icon"><button>${data.images.importantIcon}<span>High Priority</button></span></li>
              <li class="med-priority-icon"><button>${data.images.medImportantIcon}<span>Medium Priority</button></span></li>
              <li><button><span class="circle-icon"></span><span>Low Priority</button></span></li>
              <li class="path-fill-icon"><button>${data.images.mailIcon}<span>Message planner</button></span></li>
              <li class="path-fill-icon"><button>${data.images.calendarIcon}<span>Create an event</button></span></li>
            </ul>
          </div>
        </div>
      </div>
    </header>
    <p class="text-elipsis-3-line font-light">${val.desc}</p>

    <footer>
      <a class="cta font-light" href="${val.ctaLink}">${val.cta}</a>
      <a href="" class="btn-dismiss font-light">Dismiss</a>
    </footer>
  </div>

  <div class="priority-opts">
    <button>
      <span class="icon">${data.images.importantIcon}</span>
      <span>Priority</span>
    </button>
    <button>
      <span class="icon">${data.images.closeThinIcon}</span>
      <span>Dismiss</span>
    </button>
    <button>
      <span class="icon icon-more">${data.images.dotMenuIcon}</span>
      <span>More</span>
    </button>
  </div>
</li>
  `).join('');
};

export default (data = {}) => `
<div class="priority-notify">
  <div class="sort-priorities">
    <button class="text-and-chevron-label ${data.data.isShowingSort ? 'selected': ''}">
      <span>Sort by:</span>
      <span class="font-light text-and-chevron">Latest ${data.images.chevronIcon}</span>
    </button>
    <div class="extra-opts menu-box-shadow-on-lg select-dropdown-styled menu-box-shadow-on-lg ${data.data.isShowingSort ? 'show': ''}">
      <ul>
        <li><button>Date</button></li>
        <li><button>Priority</button></li>
        <li><button>Sort</button></li>
      </ul>
    </div>
  </div>
  <div class="priority-notified ${data.data.showUndoBtn ? 'show': ''}">
    <div>
      <span>Priority Removed</span>
      <button class="outline-btn">Undo</button>
    </div>
    ${data.images.closeThinIcon}
  </div>
</div>

  <div class="priority-container">
    <c-priorities-list></c-priorities-list>
  </div>
`;