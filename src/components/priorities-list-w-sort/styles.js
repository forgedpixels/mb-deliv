export default `
a {
  color: #fff;
}

.priority-notify {
  padding: 0 1rem .5rem 1.625rem;
  display: flex;
  width: 100%;
  justify-content: space-between;
  align-items: center;
}

.sort-priorities {
  font-size: 0.875rem;
  width: 36%;
}

.sort-priorities > button {
  color: #fff;
}

.priority-notified {
  display: flex;
  visibility: hidden;
  width: 64%;
  background: #979797;
  align-items: center;
  padding: .75rem .5rem;
  font-size: .75rem;
  justify-content: space-between;
  text-transform: uppercase;
}
.priority-notified.show {
  visibility: visible;
}
.priority-notified .outline-btn {
  margin-bottom: 0;
  color: #fff;
  border-color: #fff;
  margin-left: .25rem;
}

.priority-notified > div span {
  font-weight: bold;
  display: inline-block;
  margin-bottom: .5rem;
}

.priority-notified svg {
  width: 1.5rem;
  height: 1.5rem;
}

.sort-priorities footer button {
  color: #fff;
}

.sort-priorities .extra-opts {
  position: fixed;
  bottom: 0;
  left: 0;
  width: 100%;
  background: #F5F5F5;
  z-index: 2;
}

.sort-priorities .extra-opts.show {
  z-index: 2;
}

.priority-container {
  overflow-x: hidden;
  overflow-y: scroll;
  height: calc(100% - 12.75rem);
}


@media (min-width: 1290px) {

  .priority-container,
  .priority-notify {
    padding: 0 3.33rem;
  }

 .priority-notify {
   padding-bottom: .5rem;
 }


.sort-priorities .extra-opts {
  position: absolute;
  bottom: inherit;
  left: auto;
  top: inherit;
  right: inherit;
  width: 10rem;
  background: #F5F5F5;
  z-index: 2;
  padding: .75rem 1.5rem;
}

.sort-priorities .extra-opts:before {
  display: none;
}
}
`;