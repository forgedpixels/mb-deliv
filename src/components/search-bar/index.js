import resetCss from '../../assets/styles/reset.js';
import cssClasses from '../../assets/styles/classes.js';
import searchBarCss from '../../assets/styles/search-bar.js';
import searchIcon from '../../assets/images/icons/search.js';
import styles from './styles.js';
import htmlTemplate from './template.js';

const data = {
  images: {
    searchIcon
  },
  orderNo: window.location.href.indexOf('orders.html') >= 1 ? 'Orders' : 7281169 // for demo purposes, order number would come from some sort of State Management, if not defined, show Search text
};

const template = document.createElement("template");
const style = `
  <style>
    ${resetCss}
    ${cssClasses}
    ${searchBarCss}

    ${styles}
  </style>
`;

template.innerHTML = /*html*/`
  ${style}
  ${htmlTemplate(data)}
`;

class SearchBar extends HTMLElement {
  constructor() {
    super();
    this.attachShadow({ mode: "open" });
  }

  connectedCallback() {
    this.shadowRoot.appendChild(template.content.cloneNode(true));
  }
}

customElements.define("c-searchbar", SearchBar);
