const createEvtInfo = (evts = []) => 
  evts.map((val) => `
    <div class="evts">
      <span class="evt-title"><span class="evt-type">${val.evtType}</span> <span class=""evt-region">${val.evtRegion}</span></span>
      <h1 class="font-light">Midsommer McRib</h1>
      <p class="font-light">${val.evtDesc}</p>
    </div>
  `).join('');
;

export default (data = {}) => `
<div class="component-bg clipboard-bg">
  <div class="order-time">
    <span class="order-time-by">Today by ${data.order.orderTime}</span>
    <span class="order-number font-light">${data.order.orderNo}</span>
  </div>

  <div class="order-tracker">
    <order-tracker ${data.bgStyle ? `bgStyle=${data.bgStyle}` : ''}></order-tracker>
  </div>

  <div class="order-links bright-blue">
    <a class="txt-bold">View Order</a>
    <a>Track It</a>

    <div class="toggle-content-btn-container">
      <button class="toggle-content-btn btn">
        ${data.images.chevronIcon}
      </button>
    </div>
  </div>

  <div class="mobile-hidden extra-info ${data.headerUseClass}">
    <div>
      ${createEvtInfo(data.evts)}
      <div class="evts evts-all">
        <a href="" class="arrow-link">View all ${data.images.arrowIcon}</a>
      </div>
    </div>
  </div>

  <div class="fav-star">
    <c-favstar></c-favstar>
  </div>
</div>`;