import resetCss from '../../assets/styles/reset.js';
import cssClasses from '../../assets/styles/classes.js';
import styles from './styles.js';
import htmlTemplate from './template.js';
import arrowIcon from '../../assets/images/icons/arrow-long.js';
import chevronIcon from '../../assets/images/icons/chevron.js';

class SingleOrder extends HTMLElement {
  constructor() {
    super();
    this.attachShadow({ mode: "open" });

    // NOTE: set as property by parent, or if you're able to control without using shadow-dom, can just be done in CSS
    // this.bgClass = 'component-bg';
    this.bgStyle = this.getAttribute("bgStyle");
    this.headerUse = this.getAttribute("headerUse");

    this.template = document.createElement("template");
    this.template.innerHTML = /*html*/`
      ${this.getStyles()}
      ${htmlTemplate(this.getMockData())}
    `;
  }

  getStyles() {
    return `
      <style>
        ${resetCss}
        ${cssClasses}

        ${styles}
        
        .component-bg {
          background: ${this.bgStyle};
        }
      </style>
    `;
  }

  getMockData = () => {
    const bgStyle = this.getAttribute("bgStyle");
    let mockData = {
      images: {
        arrowIcon,
        chevronIcon
      },
      order: {
        orderTime: '3:14p',
        orderNo: 7281169
      },
      evts: [
        {
          evtType: 'Promotion',
          evtRegion: 'National',
          evtDesc: 'Promotion running 08.21.20 - 09.21.20. Verify stock count and allow for increase. '
        },
        {
          evtType: 'Event',
          evtRegion: '',
          evtDesc: 'Promotion running 08.21.20 - 09.21.20. Verify stock count and allow for increase. '
        },
        {
          evtType: 'Event',
          evtRegion: 'South Region',
          evtDesc: 'Promotion running 08.21.20 - 09.21.20. Verify stock count and allow for increase. '
        }
      ]
    };

    if (this.headerUse) {
      mockData.headerUseClass = 'evt-limit-amt';
    }

    // NOTE to be passed in as a prop to Single order Component to display dark styles of order tracker
    if (bgStyle) {
      mockData.bgStyle = bgStyle;
    }
  
    return mockData;
  }

  connectedCallback() {
    this.shadowRoot.appendChild(this.template.content.cloneNode(true));
    this.shadowRoot.querySelector(".toggle-content-btn").onclick = () => this.toggleContent();
  }

  toggleContent() {
    this.shadowRoot.querySelector(".toggle-content-btn").classList.toggle('enabled');
    this.shadowRoot.querySelector(".extra-info").classList.toggle('mobile-hidden')
  }
}

customElements.define("single-order", SingleOrder);
