export default `
  .component-bg {
    position: relative;
    padding: 8.75rem 1.95rem 0;
    height: 100%;
    background-image: none;
  }

  .order-time {
    font-size: 2.7rem;
    margin-bottom: 1.875rem;
  }

  .order-tracker {
    margin-bottom: 1.75rem;
  }

  .order-time > span { display: block; }

  .order-time-by { font-weight: bold}

  .order-links a {
    display: block;
    font-size: 1.5rem;
    text-transform: uppercase;
  }

  .toggle-content-btn-container {
    text-align: right;
  }

  .toggle-content-btn {
    width: 2.625rem;
    height: 2.625rem;
    transform: rotate(180deg);
  }

  .toggle-content-btn.enabled {
    transform: rotate(0deg);
  }

  /*Extra info*/

  .extra-info {
    max-height: 0;
    transition: all .5s;
    overflow-y: hidden;
  }

  .extra-info:not(.mobile-hidden) {
    max-height: 31rem;
  }

  .extra-info > div {
    max-height: 0;
    display: flex;
    flex-wrap: wrap;
    transition: all .5s;
  }

  .extra-info:not(.mobile-hidden) > div {
    max-height: 31rem;
  }

  .evts {
    width: 50%;
    padding: 0 0.625rem 2rem 0;
    font-size: 1rem;
    box-sizing: border-box;
  }

  .evts h1 {
    font-size: 1.5rem;
    margin-bottom: 1.25rem;
  }

  .evt-title {
    font-size: .625rem;
    text-transform: uppercase;
    margin-bottom: 0.625rem;
  }

  .evt-type {
    font-weight: bold;
  }

  .extra-info p {
    font-weight: 0.875;
  }

  .evts-all {
    display: flex;
    align-items: center;
    justify-content: center;
  }

  .fav-star {
    display: none;
  }

  @media (min-width: 1290px) {
    .component-bg {
      padding-top: 2.25rem;
    }

    .clipboard-bg {
      background: rgba(4,28,44, .75) url(src/assets/images/icons/clipboard.svg) 15px 25px no-repeat;
    }

    .order-links {
      margin-bottom: 2rem;
    }

    .toggle-content-btn-container {
      display: none;
    }

    .mobile-hidden {
      display: flex;
      flex-wrap: wrap;
    }

    .extra-info,
    .extra-info > div {
      max-height: none;
    }

    .evts {
      width: 33.33%;
    }

    .extra-info.evt-limit-amt .evts {
      width: 50%;
      display: none;
    }

    .extra-info.evt-limit-amt .evts:nth-child(1),
    .extra-info.evt-limit-amt .evts:nth-child(2) {
      display: block;
    }
    

    .evts-all {
      width: 100%;
      justify-content: flex-end;
    }

    .fav-star {
      display: block;
      position: absolute;
      top: 1rem;
      right: 1rem;
    }
  }
`
;