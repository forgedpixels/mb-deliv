
const createChart = (metrics = []) => {
  return metrics.map((val) => `
    <div>
      <div class="donut-chart item css">
          <svg class="circle-chart" viewbox="0 0 33.83098862 33.83098862" width="200" height="200" xmlns="http://www.w3.org/2000/svg">
          <circle class="circle-chart__circle" stroke="#00acc1" stroke-width="2" stroke-dasharray="${val.health},100" stroke-linecap="solid" fill="none" cx="16.91549431" cy="16.91549431" r="15.91549431" />
        </svg>
        <span>${val.health}%</span>
      </div>
      <span class="donut-type">${val.metricType}</span>
    </div>
  `).join('');
};

export default (data = {}) => `
  <div class="component-bg">
    <div>
      <div class="qc-data">
        <c-storeinfo></c-storeinfo>
      </div>
      <div class="charts-container">
        <div class="charts numbers-bg">
          ${createChart(data.storeData.metrics)}
        </div>
      </div>
      <div class="qc-more-info">
        <a href="" class="bright-blue"><span>Let's dig in</span> ${data.images.arrowLong}</a>
      </div>
    </div>
  </div>
`;