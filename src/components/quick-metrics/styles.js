export default `
  .component-bg {
    display: flex;
    overflow-x: auto;
    flex-wrap: wrap;
    background: transparent;
  }

  .component-bg > div {
    display: flex;
    flex-wrap: wrap;
    width: 100%;
  }

  .qc-data {
    display: none;
  }

  .charts-container {
    padding: 0 1.75rem 0 1.75rem;
  }

  .charts {
    display: flex;
    justify-content: center;
    order: 0;
    padding: 2.75rem 1.75rem;
    margin-bottom: 1.125rem;
  }

  .charts > div {
    position: relative;
    justify-content: center;
    display: flex;
    flex-wrap: wrap;
  }

  .donut-chart {
    margin-bottom: 0.625rem;
  }

  /*chart colors*/
  .charts > div:nth-child(1) .circle-chart__circle,
  .charts > div:nth-child(4) .circle-chart__circle {
    stroke: #EFCB9B;
  }
  .charts > div:nth-child(2) .circle-chart__circle{
    stroke: #0099FF;
  }
  .charts > div:nth-child(3) .circle-chart__circle{
    stroke: #F19D9D;
  }

  .charts > div:nth-child(1) .donut-chart,
  .charts > div:nth-child(4) .donut-chart {
    color: #EFCB9B;
  }
  .charts > div:nth-child(2) .donut-chart {
    color: #0099FF;
  }
  .charts > div:nth-child(3) .donut-chart {
    color: #F19D9D;
  }

  .donut-type {
    display: inline-block;
    white-space: nowrap;
    padding: 0 .5rem;
    width: 100%;
    text-align: center;
    font-size: 1.125rem;
  }

  .qc-more-info {
    font-size: 1.5rem;
  }

  .qc-more-info a {
    display: none;
    font-weight: bold;
    text-transform: uppercase;
  }

  .qc-more-info svg path {
    fill: #0099FF;
  }

  @media (min-width: 1290px) {
    .component-bg {
      background: rgba(4,28,44, .93);
    }

    .component-bg > div {
      margin: 0 auto;
      justify-content: space-between;
      max-width: 1450px;
      align-items: center;
      padding: .6875rem .25rem .1875rem;
      background: transparent url(src/assets/images/icons/numbers.svg) 15px 25px/50px no-repeat;
      flex-wrap: nowrap;
    }

    .qc-data {
      display: block;
    }
  
    .donut-chart {
      order: 2;
      margin-bottom: 0;
    }

    .qc-data,
    .charts {
      background: none;
      padding: 0;
      margin-bottom: 0;
    }

    .qc-data {
      width: auto;
      order: 0;
    }

    .charts-container {
      padding: 0;
    }

    .qc-more-info a {
      display: flex;
    }

    .charts > div {
      flex-wrap: nowrap;
      align-items: center;
      padding: 0 0 0 2.25rem;
    }

    .charts > div:first-child {
      padding-left: 0;
    }
  }
  
  @keyframes circle-chart-fill {
    to { stroke-dasharray: 0 100; }
  }
  
  @keyframes circle-chart-appear {
    to {
      opacity: 1;
      transform: translateY(0);
    }
  }
`;