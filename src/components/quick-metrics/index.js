import resetCss from '../../assets/styles/reset.js';
import cssClasses from '../../assets/styles/classes.js';
import donutCss from '../../assets/styles/donut.js';
import arrowLong from '../../assets/images/icons/arrow-long.js';
import styles from './styles.js';
import htmlTemplate from './template.js';

const mockData = {
  images: {
    arrowLong
  },
  storeData: {
    metrics: [
      {
        health: 95,
        metricType: 'Order Health'
      },
      {
        health: 99,
        metricType: 'Proposal Accuracy'
      },
      {
        health: 15,
        metricType: 'Late Adds'
      },
      {
        health: 10,
        metricType: 'Order Modifications'
      },
    ]
  }
};

const template = document.createElement("template");
const style = `
  <style>
    ${resetCss}
    ${cssClasses}
    ${donutCss}

    ${styles}
  </style>
`;

template.innerHTML = /*html*/`
  ${style}
  ${htmlTemplate(mockData)}
`;

class QuickMetrics extends HTMLElement {
  constructor() {
    super();
    this.attachShadow({ mode: "open" });
  }

  connectedCallback() {
    this.shadowRoot.appendChild(template.content.cloneNode(true));
  }
}

customElements.define("c-qmetrics", QuickMetrics);
