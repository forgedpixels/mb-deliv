// {
//   status: 'starred',
//   type: 'store',
//   timeStamp: '1 min', // will be whatever format Salesforce uses
//   title: 'Store Rank Low',
//   msg: 'Watch your stock count, and double check for accuracy to avoid alerts, overall waste...',
//   actionTxt: 'Contact Planner',
//   actionLink: ''
// },


const createPriorities = (data = {priorities: []}) => {
  const isPriority = (isPriority) => {
    if (isPriority) {
      return `
        ${data.images.favStarIcon}
      `;
    }
    return '';
  };

  return data.priorities.map((val) => `
  <li>
    <div class="priority priority-status-${val.status}">
      <header class="content-to-ends content-base-vert">
        ${isPriority(val.status)}
        <strong class="priority-type-${val.type} ">${val.title}</strong>
        <span class="priority-time-ago">${val.timeStamp}</span>
      </header>
      <p>${val.msg}</p>
    </div>
  </li>
  `).join('');
};


export default (data = {}) => `
  <div class="component-bg fav-priority-bg">
    <header class="component-header content-to-ends content-base-vert">
      <span>
        Priorities
        <span class="priority-stats">
          <span>13 assigned</span> / <span class="unread">4 starred</span>
        </span>
      </span>
    </header>
    
    <div class="priorities-container">
      <c-priorities-list inFlipComponent="true" showAmt="3"></c-priorities-list>
    </div>

    <footer class="content-to-ends">
      <button>
        ${data.images.flipIcon}
      </button>

      <a href="" class="arrow-link">View all ${data.images.arrowIcon}</a>
    </footer>
  </div>
`;