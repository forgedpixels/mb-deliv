export default `
  .component-bg {
    position: relative;
    padding: 1.25rem;
    height: 100%;
    overflow: hidden;
  }

  .priority-stats {
    display: block;
    font-weight: normal;
  }

  .priorities-container {
    padding: 0 .75rem;
  }

  footer {
    position: absolute;
    bottom: 0;
    width: 100%;
    left: 0;
    padding: 1.25rem;
  }
`;