import resetCss from '../../assets/styles/reset.js';
import cssClasses from '../../assets/styles/classes.js';
import arrowIcon from '../../assets/images/icons/arrow-long.js';
import flipIcon from '../../assets/images/icons/flip.js';
import favStarIcon from '../../assets/images/icons/fav-star.js';
import styles from './styles.js';
import htmlTemplate from './template.js';

const mockData = {
  images: {
    favStarIcon,
    flipIcon,
    arrowIcon
  },
  priorities: [
    {
      status: 'starred',
      type: 'store',
      timeStamp: '1 min', // will be whatever format Salesforce uses
      title: 'Store Rank Low',
      msg: 'Watch your stock count, and double check for accuracy to avoid alerts, overall waste...',
      actionTxt: 'Contact Planner',
      actionLink: ''
    },
    {
      status: 'unassigned',
      type: 'spending',
      timeStamp: '20 min', // will be whatever format Salesforce uses
      title: 'High Spending',
      msg: 'Watch your stock count, and double check for accuracy to avoid alerts, overall waste.',
      actionTxt: 'Review Orders',
      actionLink: ''
    },
    {
      status: 'unassigned',
      type: 'claim',
      timeStamp: '47 min', // will be whatever format Salesforce uses
      title: 'CLAIM 123334 has been reviewed',
      msg: '',
      actionTxt: 'View updates',
      actionLink: ''
    },
    {
      status: 'starred',
      type: 'claim',
      timeStamp: '59 min', // will be whatever format Salesforce uses
      title: 'random',
      msg: 'random2',
      actionTxt: 'View updates',
      actionLink: ''
    }
  ]
};

const template = document.createElement("template");
const style = `
  <style>
    ${resetCss}
    ${cssClasses}

    ${styles}
  </style>
`;
const parseData = data => {
  return {
    ...data,
    priorities: data.priorities.slice(0, 3)
  }
}

template.innerHTML = /*html*/`
  ${style}
  ${htmlTemplate(parseData(mockData))}
`;

class Priorities extends HTMLElement {
  constructor() {
    super();
    this.attachShadow({ mode: "open" });
  }

  connectedCallback() {
    this.shadowRoot.appendChild(template.content.cloneNode(true));
    let buttons = this.shadowRoot.querySelector("footer button").onclick = () => this.toggleFlip();
  }

  toggleFlip() {
    window.dispatchEvent(new Event('flippanel'));
  }
}

customElements.define("c-priorities", Priorities);
