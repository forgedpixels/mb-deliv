import resetCss from '../../assets/styles/reset.js';
import cssClasses from '../../assets/styles/classes.js';
import breadcrumbCss from '../../assets/styles/breadcrumbs.js';
import closeIcon from '../../assets/images/icons/close.js';
import styles from './styles.js';
import htmlTemplate from './template.js';

const mockData = {
  images: {
    closeIcon
  },
};
const template = document.createElement("template");
const style = `
  <style>
    ${resetCss}
    ${cssClasses}
    ${breadcrumbCss}

    ${styles}
  </style>
`;

const parseData = data => {
  const urlParams = new URLSearchParams(window.location.search);
  
  return {
    ...data,
    showMsgDetailClass: urlParams.get('design') === '2' || urlParams.get('design') === '3' ? 'show-msg-detail' : '',
    newMsgClass: urlParams.get('design') === '3' ? 'new-msg' : ''
  };
}

template.innerHTML = /*html*/`
  ${style}
  ${htmlTemplate(parseData(mockData))}
`;

class MsgPgNav extends HTMLElement {
  constructor() {
    super();
    this.attachShadow({ mode: "open" });
  }

  connectedCallback() {
    this.shadowRoot.appendChild(template.content.cloneNode(true));
  }
}

customElements.define("c-msg-pg-nav", MsgPgNav);
