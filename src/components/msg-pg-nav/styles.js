export default `
  .msg-pg-nav {
    padding: 1.75rem 1.9375rem .2rem;
  }

  .msg-header-content {
    margin-bottom: 1.5rem;
  }

  .msg-header-content.content-max-and-center {
    padding-left: 0;
  }

  .msg-header-content.show-msg-detail,
  .mail-folder-content.show-msg-detail {
    display: none;
  }

  .mail-folder-content  {
    margin-bottom: .5rem;
  }

  .msg-header {
    max-width: calc(700px - 3rem);
  }

  .new-msg button svg {
    transform: rotate(45deg);
    width: 1.15rem;
    height: 1.15rem;
  }

  .new-msg button svg path {
    fill: #fff;
  }

  .search-bar {
    max-width: 150px;
  }

  .new-msg {
    margin-top: .5rem;
  }

  @media (min-width: 1290px) {

    .content-max-and-center {
      padding-left: 1rem;
    }

    .msg-header-content.show-msg-detail,
    .mail-folder-content.show-msg-detail {
      display: block;
    }

    .msg-header-content.new-msg .new-msg button,
    .mail-folder-content.new-msg .new-msg button {
      display: none;
    }

    .search-bar {
      max-width: 260px;
    }

    .msg-pg-nav {
      padding-top: 0;
    }
  }
`;