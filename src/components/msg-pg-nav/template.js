export default data => `
  <div class="msg-pg-nav">
    <c-breadcrumbs></c-breadcrumbs>

    <div class="msg-header-content content-max-and-center ${data.showMsgDetailClass} ${data.newMsgClass}">
      <header class="msg-header content-to-ends">
        <div class="search-bar">
          <c-msg-searchbar></c-msg-searchbar>
        </div>

        <div class="new-msg">
          <button>${data.images.closeIcon}</button>
        </div>
      </header>
    </div>

    <div class="mail-folder-content content-max-and-center ${data.showMsgDetailClass}">
      <c-select-mail-folder></c-select-mail-folder>
    </div>
  </div>
`;