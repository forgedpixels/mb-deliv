export default (data) => {
  return `
  <div class="feedback ${(
    data.data.isSelectSubject ||
    data.data.isFormDisplay ||
    data.data.isFormComplete ||
    data.data.isSelectSubjectOpen) ? 'expanded' : ''} ${data.data.isFormComplete ? 'complete' : '' }">
    <div>
      <header>
        <span>Tell us something </span>
        <button class="expand-btn">
          ${data.images.chevronIcon}
        </button>
        <button class="close-btn">
          ${data.images.closeIcon}
        </button>
      </header>
  
      <div class="feedback-incomplete">
        <div class="form">
          <p>The more we know the better we can plan. </p>
  
          ${(data.data.isSelectSubject || data.data.isSelectSubjectOpen) ? `
          <div class="select-topic">
            <div class="msg-type-select label-and-select-dropdown">
              <span class="font-light">Select a topic:</span>
              <div class="msg-select-btn">
                <button>
                  <span class="font-light">General Inquiry</span>
                  ${data.images.chevronIcon}
                </button>
  
                <div class="extra-opts ${data.data.isSelectSubjectOpen ? 'show': ''}">
                  <div class="menu-box-shadow-on-lg select-dropdown-styled ${data.data.isSelectSubjectOpen ? 'show': ''}">
                    <ul>
                      <li><button>General Concern</button></li>
                      <li><button>Contact your planner</button></li>
                      <li><button>Planned event/activity</button></li>
                      <li><button>Contact your field service</button></li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
          ` : ''}
  
          ${data.data.isFormDisplay ? `
          <div class="describe-topic">
            <textarea>Hello David, lorem ipsum dolor sit amet</textarea>
  
            <li>
              <span class="file-name">test.jph</span>
              <span class="file-size">1kb</span>
              <button class="outline-btn">Remove</button>
            </li>
        
            <button class="attach-btn">${data.images.paperClipIcon} Attached a file or photo</button>
          </div>
          ` : ''}
        </div>
  
        <footer>
          <button class="btn primary-cta-btn">Send</button>
          <button class="btn cancel-btn">Cancel</button>
        </footer>
      </div>
  
      <div class="feedback-complete">
        <h3>Thank you</h3>
        <p>We'll be in touch.</p>
      </div>
    </div>
  </div>
  `;
}