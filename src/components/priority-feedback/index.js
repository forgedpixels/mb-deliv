import resetCss from '../../assets/styles/reset.js';
import cssClasses from '../../assets/styles/classes.js';
import chevronIcon from '../../assets/images/icons/chevron.js';
import importantIcon from '../../assets/images/icons/important.js';
import closeThinIcon from '../../assets/images/icons/close-thin.js';
import closeIcon from '../../assets/images/icons/close.js';
import favStarIcon from '../../assets/images/icons/fav-star.js';
import paperClipIcon from '../../assets/images/icons/paperclip.js';
import dotMenuIcon from '../../assets/images/icons/dot-menu.js';
import extraOptsDropdown from '../../assets/styles/extra-opts-dropdown.js';
import selectInputCss from '../../assets/styles/select-input.js';
import styles from './styles.js';
import newMsgStyles from '../../assets/styles/new-msg.js';
import htmlTemplate from './template.js';


const urlParams = new URLSearchParams(window.location.search);
const mockData = {
  images: {
    chevronIcon,
    importantIcon,
    closeIcon,
    dotMenuIcon,
    closeThinIcon,
    paperClipIcon,
    favStarIcon
  },
  data: {
    isSelectSubjectOpen: urlParams.get('design') === '1',
    isSelectSubject: urlParams.get('design') === '2',
    isFormDisplay: urlParams.get('design') === '3',
    isFormComplete: urlParams.get('design') === '4'
  }
};

const template = document.createElement("template");
const style = `
  <style>
    ${resetCss}
    ${cssClasses}
    ${selectInputCss}
    ${extraOptsDropdown}
    ${newMsgStyles}

    ${styles}
  </style>
`;

template.innerHTML = /*html*/`
  ${style}
  ${htmlTemplate(mockData)}
`;

class PriorityFeedback extends HTMLElement {
  constructor() {
    super();
    this.attachShadow({ mode: "open" });
  }

  connectedCallback() {
    this.shadowRoot.appendChild(template.content.cloneNode(true));
  }
}

customElements.define("c-priority-feedback", PriorityFeedback);
