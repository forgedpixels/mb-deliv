export default `
a {
  color: #fff;
}

.feedback {
  position: absolute;
  bottom: 0;
  left: 0;
  width: 100%;
  background: #0099FF;
}

.feedback header button {
  position: absolute;
  top: 0;
  left: 0;
  height: 100%;
  width: 100%;
}

.feedback header button.close-btn {
  display: none;
}

.feedback header button svg {
  position: absolute;
  right: 2rem;
  margin-top: -.25rem;
}

.feedback > div {
  padding: 2rem;
}

.feedback header > span {
  font-size: 1.375rem;
  color: #fff;
}

.feedback .form,
.feedback footer {
  height: 0;
  min-height: 0;
  overflow: hidden;
}

.feedback .feedback-complete,
.feedback.expanded.complete .feedback-incomplete {
  display: none;
}

.feedback.expanded.complete .feedback-complete {
  display: block;
}

.feedback.expanded.complete header span {
  visibility: hidden;
}

.feedback.expanded header {
  display: flex;
  justify-content: space-between;
  flex-wrap: nowrap;
  margin-bottom: 1rem;
}

.feedback.expanded header > span:first-child {
  font-weight: bold;
}

.feedback.expanded header button {
  position: relative;
  top: inherit;
  left: inherit;
  width: auto;
  height: auto;
}

.feedback.expanded header button svg {
  position: relative;
  margin-top: 0;
  right: inherit;
}

.feedback.expanded header button.expand-btn {
  display: none;
}

.feedback.expanded header button.close-btn {
  display: block;
}

.feedback.expanded header button.close-btn svg path {
  fill: #fff;
}

.feedback.expanded footer,
.feedback.expanded .form {
  transition: min-height .5s, height .5s;
  overflow: inherit;
}

.feedback.expanded .form {
  min-height: 13rem;
  height: auto;
}

.feedback.expanded .form p {
  margin-bottom: .5rem;
}

.feedback.expanded footer {
  min-height: 2.5rem;
}

.feedback.expanded .select-topic {
  padding: 4rem 0 6rem;
}

.feedback.expanded .msg-type-select {
  display: flex;
  justify-content: space-between;
  width: 18rem;
}

.feedback.expanded .msg-select-btn > button {
  color: #fff;
  display: flex;
  align-items: center;
  justify-content: space-between;
  width: 9.5rem;
}

.feedback.expanded .msg-type-select,
.feedback.expanded .msg-select-btn > button span {
  font-size: 1.125rem;
}

.feedback.expanded footer {
  display: flex;
}

.feedback.expanded footer .btn {
  padding: .7rem 1rem;
}

.feedback.expanded footer .primary-cta-btn {
  max-width: 9rem;
  color: #0099FF;
  background: #fff;
}

.feedback.expanded footer .cancel-btn,
.describe-topic .outline-btn {
  width: auto;
  color: #fff;
}

.describe-topic .outline-btn {
  border-color: #fff;
}

.describe-topic textarea {
  background: transparent;
  border-color: #fff;
  color: #fff;
  margin-bottom: 1rem;
}

.describe-topic .attach-btn {
  color: #fff;
}

.describe-topic .attach-btn svg path {
  fill: #fff;
}

.feedback-complete {
  min-height: 13rem;
  padding-top: 3rem;
}

.feedback-complete h3 {
  margin-bottom: 1rem;
  font-size: 1.375rem;
}
`;