export default (data = {}) => `
<div class="qc-data">
  <span class="store-no bright-blue">Store ${data.storeNo}</span>
  <span class="store-location">${data.location}</span>
</div>
`;