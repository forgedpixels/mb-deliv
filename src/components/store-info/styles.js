export default `
.qc-data {
  font-size: 1.5rem;
  line-height: 1.5625rem;
  order: 1;
  padding: 1rem;
  width: 100%;
}

.store-no {
  font-weight: bold;
}

.store-location {
  display: block;
}
`;