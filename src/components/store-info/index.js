import resetCss from '../../assets/styles/reset.js';
import cssClasses from '../../assets/styles/classes.js';
import styles from './styles.js';
import htmlTemplate from './template.js';

const mockData = {
  storeNo: 305,
  location: 'Queensway',
};

const template = document.createElement("template");
const style = `
  <style>
    ${resetCss}
    ${cssClasses}

    ${styles}
  </style>
`;

template.innerHTML = /*html*/`
  ${style}
  ${htmlTemplate(mockData)}
`;

class StoreInfo extends HTMLElement {
  constructor() {
    super();
    this.attachShadow({ mode: "open" });
  }

  connectedCallback() {
    this.shadowRoot.appendChild(template.content.cloneNode(true));
  }
}

customElements.define("c-storeinfo", StoreInfo);
