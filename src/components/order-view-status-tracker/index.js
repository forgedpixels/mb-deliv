import resetCss from '../../assets/styles/reset.js';
import cssClasses from '../../assets/styles/classes.js';
import eyeIcon from '../../assets/images/icons/eye.js';
import styles from './styles.js';
import htmlTemplate from './template.js';

const mockData = {
  images: {
    eyeIcon
  },
  data: {
    order: {
      orderNo: 123
    }
  }
}

const template = document.createElement("template");

const getTemplate = mockData => {
  if (mockData.data.order.orderNo) {
    return htmlTemplate(mockData);
  } else {
    return ``;
  }
}

template.innerHTML = /*html*/`
  <style>
    ${resetCss}
    ${cssClasses}
    ${styles}
  </style>

  ${getTemplate(mockData)}
  `;

class OrderViewStatus extends HTMLElement {
  constructor() {
    super();
    this.attachShadow({ mode: "open" });
  }

  connectedCallback() {
    this.shadowRoot.appendChild(template.content.cloneNode(true));
  }
}

customElements.define("c-order-view-status", OrderViewStatus);
