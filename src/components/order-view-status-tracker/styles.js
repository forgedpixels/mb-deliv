export default `
  .status-icon {
    position: relative;
    display: inline-block;
    width: 1rem;
    height: 1rem;
    border-radius: 50%;
    border: 2px solid #B1FDD8;
  }

  .status-icon.status-filled {
    background: #B1FDD8;
  }

  .status-icon.status-current svg {
    top: -2.75rem;
    position: absolute;
    left: -1rem;
  }

  .status-tracker {
    display: flex;
    width: 100%;
    justify-content: space-between;
    max-width: 300px;
    margin-top: 2.25rem;
  }

  .text-progress {
    position: relative;
    display: flex;
    justify-content: center;
    align-items: center;
    text-align: center;
    font-size: .5rem;
    white-space: nowrap;
    text-transform: uppercase;
    font-weight: bold;
    color: #B1FDD8;
    width: calc(100% - 3rem);
  }

  .text-progress svg {
    top: -2.25rem;
    position: absolute;
  }

  .text-progress.text-filled:after,
  .text-progress.text-filled:before {
    content: '';
    border-top: 1px solid;
    margin: 0 5px 0 0;
    color: #fff;
    flex: 1 0 26px;
  }

  .text-progress.text-filled:after {
    margin: 0 0 0 5px;
  }
`;