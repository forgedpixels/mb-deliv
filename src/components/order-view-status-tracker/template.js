export default data => `
  <div class="status-tracker">
    <span class="status-icon"></span>
    <span class="text-progress text-filled">Ready for viewing ${data.images.eyeIcon}</span>
    <span class="status-icon status-filled status-current"></span>
  </div>
`;