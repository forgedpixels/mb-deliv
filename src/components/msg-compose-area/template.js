const getList = (data = {attached: []}) => {
  return data.attached.map((val, i) => `
    <li>
      <span class="file-name">${val.fileName}</span>
      <span class="file-size">${val.fileSize}</span>
      <button class="outline-btn">Remove</button>
    </li>
  `).join('');
};

const getAttachments = data => `
  <div class="attachments">
    <ul>
      ${getList(data)}
    </ul>
  </div>
`;

export default data => `
  <div class="reply-wyziwig">
    <textarea>Hello David, lorem ipsum dolor sit amet, consectetur adipiscing elit. Diam nisi cras ultricies non, mus eget. Pellentesque id amet, vestibulum, aenean lobortis turpis. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</textarea>

    ${getAttachments(data)}

    <button class="attach-btn">${data.images.paperClipIcon} Attached a file or photo</button>

    <div class="submit-opts">
      <input type="submit" value="Send" class="btn primary-cta-btn" />
      <input type="button" value="Cancel" class="btn cancel-btn" />
    </div>
  </div>
`;