import resetCss from '../../assets/styles/reset.js';
import cssClasses from '../../assets/styles/classes.js';
import paperClipIcon from '../../assets/images/icons/paperclip.js';
import newMsgStyles from '../../assets/styles/new-msg.js';
import styles from './styles.js';
import htmlTemplate from './template.js';

const mockData = {
  images: {
    paperClipIcon
  },
  attached: [
    {
      fileName: '1235.jpg',
      fileSize: '335k'
    },
    {
      fileName: '1235.pdf',
      fileSize: '1.1mb'
    }
  ]
}
const template = document.createElement("template");

template.innerHTML = /*html*/`
  <style>
    ${resetCss}
    ${cssClasses}
    ${newMsgStyles}

    ${styles}
  </style>
  
  ${htmlTemplate(mockData)}
`;

class MsgComposeArea extends HTMLElement {
  constructor() {
    super();
    this.attachShadow({ mode: "open" });
  }

  connectedCallback() {
    this.shadowRoot.appendChild(template.content.cloneNode(true));
  }
}

customElements.define("c-msg-compose-area", MsgComposeArea);
