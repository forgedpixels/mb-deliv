export default `
  .reply-wyziwig {
    border: 0.5px solid #000000;
    padding: 0.8125rem;
  }

  .reply-wyziwig textarea {
    border: none;
    font-size: .75rem;
    padding: 0 0 .6875rem;
    margin-bottom: 1rem;
    outline: none;
    min-height: 8rem;
    border-bottom: 0.3px solid rgba(0, 0, 0, 0.3);
  }

  .submit-opts {
    display: flex;
  }

  .submit-opts .primary-cta-btn {
    max-width: 9rem;
  }

  .submit-opts .cancel-btn {
    width: auto;
  }

  @media (min-width: 1290px) {
    .conversation-reply-content {
      padding-right: 4.875rem;
    }

    .reply-wyziwig {
      padding: 1rem;
    }

    .reply-wyziwig textarea {
      font-size: 1rem;
    }
  }
`;