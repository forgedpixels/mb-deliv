export default data => `
  <div class="header">
    <div class="header-inner">
      <h1><a><span class="bright-blue font-light">MB</span>Sync</a></h1>

      <ul class="main-nav">
        <li>
          <a href="">Orders</a>
          <div>
            <div class="orders-component hover-component">
              <c-multiple-orders></c-multiple-orders>
            </div>
          </div>
        </li>
        <li>
          <a href="">Calendar</a>
          <div>
            TEST AREA Calendars
          </div>
        </li>
        <li>
          <a href="">Ops Supply</a>
          <div>
            TEST AREA Supply
          </div>
        </li>
        <li>
          <a href="">Messages</a>
          <div>
            <div class="msgs-component hover-component">
              <c-header-msgs></c-header-msgs>
            </div>
          </div>
        </li>
        <li>
          <a href="">The Numbers</a>
          <div>
            TEST AREA Numbers
          </div>
        </li>
      </ul>
    </div>

    <div class="hamburger-container">
      <button class="btn nav-btn">
        <div class="donut-chart item css">
          <svg class="circle-chart" viewbox="0 0 33.83098862 33.83098862" width="31" height="31" xmlns="http://www.w3.org/2000/svg">
            <circle stroke="#EFCB9B" stroke-width="2" stroke-dasharray="100,100" stroke-linecap="solid" fill="none" cx="16.91549431" cy="16.91549431" r="15.91549431" />
            <circle class="circle-chart__circle" stroke="#00acc1" stroke-width="2" stroke-dasharray="75,100" stroke-linecap="solid" fill="none" cx="16.91549431" cy="16.91549431" r="15.91549431" />
          </svg>
          <span>12</span>
        </div>
        <div class="hamburger">
          <div class="bar1"></div>
          <div class="bar2"></div>
          <div class="bar3"></div>
        </div>
      </button>
      <div class="hamburger-overlay">
        <div class="hamburger-inner-content">
          <h2 class="content-to-ends content-center-vert">My Priorities ${data.images.closeIcon}</h2>

          <c-priorities-list-w-sort></c-priorities-list-w-sort>


          <c-priority-feedback></c-priority-feedback>

        </div>
      </div>
    </div>
  </div>
`;