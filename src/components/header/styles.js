export default `
  h1 {
    margin: 0;
    padding-top: 3.3125rem;
    text-align: center;
    font-size: 1.5rem;
  }

  a {
    color: #fff;
  }

  .header.hovering,
  .header.hovering.sticky {
    background: #fff;
  }

  .header.hovering a,
  .header.hovering.sticky a {
    color: #000;
  }

  .header .header-inner > h1 {
    transition: font-size .5s;
  }

  .header.sticky {
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    background: #041C2C;
    color: #fff;
  }

  .header > .header-inner {
    max-width: 1450px;
    margin: 0 auto;
  }

  .header.sticky h1 {
    padding: 1rem 0;
  }

  .header.sticky .hamburger-container {
    top: 1rem;
  }

  .hamburger-overlay {
    display: none;
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background: rgba(0,0,0,.5);
    max-width: none;
    backdrop-filter: blur(3px);
    opacity: 0;
  }

  .hamburger-overlay.show {
    display: block;
    opacity: 1;
    animation-name: fadeIn;
    animation-duration: .3s;
    animation-iteration-count: 1;
  }

  .hamburger-inner-content {
    position: fixed;
    top: 0;
    right: 0;
    width: 100%;
    height: 100%;
    background: #041C2C;
    overflow-x: hidden;
  }

  .hamburger-inner-content h2 {
    padding: 1.625rem 1.625rem;
    font-size: 1.5rem;
    text-transform: uppercase;
  }

  .hamburger-inner-content h2 svg {
    width: 22px;
    height: 22px;
  }

  .hamburger-inner-content h2 svg path {
    fill: #fff;
  }

  .hamburger-container {
    position: absolute;
    right: 1.875rem;
    top: 3rem;
  }

  .nav-btn {
    position: relative;
    display: flex;
    align-items: center;
  }

  .hamburger-container.change .donut-chart {
    display: none;
  }

  .hamburger-container.change .nav-btn .bar1,
  .hamburger-container.change .nav-btn .bar2,
  .hamburger-container.change .nav-btn .bar3 {
    background-color: #fff;
  }

  .alerts-num {
    position: relative;
    margin-right: 0.4375rem;
    color: #fff;
  }

  .bar1, .bar2, .bar3 {
    width: 20px;
    height: 2px;
    background-color: #fff;
    margin: 5px 0;
    transition: 0.4s;
  }

  .change .bar1 {
    -webkit-transform: rotate(-45deg) translate(-2px, 3px);
    transform: rotate(-45deg) translate(-2px, 3px);
  }

  .change .bar2 {opacity: 0;}

  .change .bar3 {
    -webkit-transform: rotate(45deg) translate(-8px, -8px);
    transform: rotate(45deg) translate(-7px, -8px);
  }

  .header-inner > ul {
    display: none;
  }

  .header-inner > ul li {
    color: #fff;
    font-size: 1.5rem;
    padding: 2rem 1.8rem;
  }

  .header-inner > ul li div {
    display: none; 
  }

  .header-inner > ul li:hover a {
    color: #0099FF;
  }

  .header-inner > ul li:hover div {
    display: block;
    position: fixed;
    top: 7rem;
    left: 0;
    height: calc(80% - 7rem);
    min-height: 42rem;
    width: 100%;
    background: #fff;
    z-index: 4;
    box-shadow: 10rem 2.5rem 2rem #000;
    color: #000;
  }

  .header.sticky .header-inner > ul li:hover div {
    top: 4rem;
  }

  ul li:hover div.orders-component {
    padding: 4rem 0 7.75rem;
  }

  ul li:hover div.msgs-component {
    padding: 4rem 6.25rem 7.75rem;
  }

  .hovering .hover-component {
       opacity: 1;
       animation-name: fadeIn;
       animation-duration: .75s;
       animation-iteration-count: 1;
  }
  @keyframes fadeIn {
    from { opacity: 0; }
    to { opacity: 1; }
  }

  .donut-chart {
    display: flex;
    border-radius: 50%;
    margin-right: 0.4375rem;
    width: 1.875rem;
    height: 1.875rem;
    align-items: center;
    justify-content: center;
  }

  .donut-type {
    display: inline-block;
    white-space: nowrap;
    padding: 0 .5rem;
    width: 100%;
    text-align: center;
  }

  .donut-chart span {
    font-weight: bold;
    font-size: 1rem;
    color: #fff;
  }

  .donut-chart svg {
    position: absolute;
    width: 1.875rem;
    height: 1.875rem;
  }

  .circle-chart__circle {
    animation: circle-chart-fill 1s reverse; /* 1 */ 
    transform: rotate(-90deg); /* 2, 3 */
    transform-origin: center; /* 4 */
  }
  
  .circle-chart__info {
    animation: circle-chart-appear 1s forwards;
    opacity: 0;
    transform: translateY(0.3em);
  }
  
  @keyframes circle-chart-fill {
    to { stroke-dasharray: 0 100; }
  }
  
  @keyframes circle-chart-appear {
    to {
      opacity: 1;
      transform: translateY(0);
    }
  }

  @media (min-width: 1290px) {
    .header.sticky h1 {
      font-size: 1.875rem;
    }

    .header > .header-inner {
      position: relative;
      display: flex;
      align-items: center;
      max-width: 1450px;
      padding: 4rem 3.75rem 2rem;
      justify-content: center;
      height: 7rem;
    }
    
    .header.sticky > .header-inner {
      padding-top: 0;
      padding-bottom: 0;
      height: 4rem;
    }

    .hamburger-container {
      position: absolute;
      right: 0;
    }
    

    .header.sticky .hamburger-container {
      top: .5rem;
    }

    .nav-btn {
      background: #fff;
      padding: 0.625rem;
      border-radius: 25px 0 0 25px;
    }

    .donut-chart span {
      color: #041C2C;
    }

    .bar1, .bar2, .bar3 {
      background-color: #041C2C;
    }

    .header-inner > ul {
      display: flex;
      max-width: 840px;
      width: 100%;
    }

    h1 {
      font-size: 2.625rem;
      position: absolute;
      left: 2rem;
      padding-top: 0;
      text-align: left;
    }
  }













  @media (min-width: 1290px) {
    .hamburger-inner-content {
      max-width: 32rem;
    }
  }







  @media (min-width: 1290px) {
     .hamburger-inner-content h2 {
       padding: 2.6252rem 3.33rem 1.33rem;
     }
  }












  
`;