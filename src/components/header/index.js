import resetCss from '../../assets/styles/reset.js';
import cssClasses from '../../assets/styles/classes.js';
import chevronIcon from '../../assets/images/icons/chevron.js';
import importantIcon from '../../assets/images/icons/important.js';
import closeThinIcon from '../../assets/images/icons/close-thin.js';
import closeIcon from '../../assets/images/icons/close.js';
import paperClipIcon from '../../assets/images/icons/paperclip.js';
import dotMenuIcon from '../../assets/images/icons/dot-menu.js';
import extraOptsDropdown from '../../assets/styles/extra-opts-dropdown.js';
import selectInputCss from '../../assets/styles/select-input.js';
import styles from './styles.js';
import newMsgStyles from '../../assets/styles/new-msg.js';
import htmlTemplate from './template.js';

const template = document.createElement("template");

const mockData = {
  images: {
    chevronIcon,
    importantIcon,
    closeIcon,
    dotMenuIcon,
    closeThinIcon,
    paperClipIcon
  }
};

template.innerHTML = /*html*/`
  <style>
    ${resetCss}
    ${cssClasses}
    ${selectInputCss}
    ${extraOptsDropdown}
    ${newMsgStyles}

    ${styles}
  </style>
  
  ${htmlTemplate(mockData)}
`;

class Header extends HTMLElement {
  constructor() {
    super();
    this.attachShadow({ mode: "open" });
  }

  connectedCallback() {
    this.shadowRoot.appendChild(template.content.cloneNode(true));
    this.shadowRoot.querySelector(".nav-btn").onclick = () => this.toggleClass();
    
    const header = this.shadowRoot.querySelector(".header");
    let buttons = this.shadowRoot.querySelectorAll("ul.main-nav li");

    buttons.forEach((elem) => {
      elem.onmouseover = () => {
        header.classList.add('hovering');
        // window.dispatchEvent(new Event('menu-hover'));
      };
      elem.onmouseout = () => {
        header.classList.remove('hovering');
        // window.dispatchEvent(new Event('menu-hover-out'));
      }
    });

    window.onscroll = function() {myFunction()};

    // Get the offset position of the navbar
    var sticky = header.offsetTop;

    // Add the sticky class to the header when you reach its scroll position. Remove "sticky" when you leave the scroll position
    function myFunction() {
      if (window.pageYOffset > sticky) {
        header.classList.add("sticky");
      } else {
        header.classList.remove("sticky");
      }
    }
  }

  toggleClass() {
    this.shadowRoot.querySelector(".hamburger-container").classList.toggle('change');
    this.shadowRoot.querySelector(".hamburger-overlay").classList.toggle('show');
  }
}

customElements.define("c-header", Header);
