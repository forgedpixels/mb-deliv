
export default (data = {}) => `
  <div class="messages-container">
    <div class="msgs-unread">
      <h2>Unread (${data.messages.unread.length})</h2>
      <c-mailmsgs inheader="true"></c-mailmsgs>
    </div>
    
    <div class="msgs-starred">
      <h2>Starred</h2>
      <c-mailmsgs inheader="true"></c-mailmsgs>
    </div>
    
    <div class="msgs-recent">
      <h2>Recent</h2>
      <c-mailmsgs inheader="true"></c-mailmsgs>
    </div>
  </div>
`;