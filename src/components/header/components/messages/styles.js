export default `
  .messages-container {
    display: flex;
    flex-wrap: nowrap;
    justify-content: space-between;
  }

  .messages-container > div {
    width: 29%;
  }

  .messages-container > div h2 {
    font-size: 1.375rem;
    text-align: center;
  }
`;