import resetCss from '../../../../assets/styles/reset.js';
import cssClasses from '../../../../assets/styles/classes.js';
import arrowIcon from '../../../../assets/images/icons/arrow.js';
import styles from './styles.js';
import htmlTemplate from './template.js';


const mockData = {
  images: {
    arrowIcon
  },
  // NOTE: Unread would an array that is passed into Mail Messages component, 6 here is for example. Starred and Recent would be the same, passed in as props into the Mail Messages component.
  messages: {
    unread: [{}, {}, {}, {}],
    starred: [{},{},{}],
    recent: [{},{}]
  }
};

const template = document.createElement("template");
const style = `
  <style>
    ${resetCss}
    ${cssClasses}

    ${styles}
  </style>
`;

template.innerHTML = /*html*/`
  ${style}
  ${htmlTemplate(mockData)}
`;

class HeaderSnapshotMsgs extends HTMLElement {
  constructor() {
    super();
    this.attachShadow({ mode: "open" });
  }

  connectedCallback() {
    this.shadowRoot.appendChild(template.content.cloneNode(true));
  }
}

customElements.define("c-header-msgs", HeaderSnapshotMsgs);
