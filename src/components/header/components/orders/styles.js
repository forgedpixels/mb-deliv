export default `
  .orders-container {
    overflow-x: auto;
  }

  ol {
    display: flex;
    flex-wrap: nowrap;
    overflow: auto;
    padding: 0 5rem;
    float: left;
  }

  ol li {
    min-width: 400px;
    border-right: 1px solid #E5E5E5;
  }

  ol li:last-child {
    border-right: none;
  }
`;