import resetCss from '../../../../assets/styles/reset.js';
import cssClasses from '../../../../assets/styles/classes.js';
import arrowIcon from '../../../../assets/images/icons/arrow.js';
import styles from './styles.js';
import htmlTemplate from './template.js';


const mockData = {
  images: {
    arrowIcon
  },
  // SHOULD BE ACTUAL DATA that will be passed into each component. To be determined in LWC data structure, for now just itierating over array to display orders
  orders: [
    {
      order: 1,
    },
    {
      order: 2
    },
    {
      order: 3
    },
    {
      order: 4
    },
  ]
};

const template = document.createElement("template");
const style = `
  <style>
    ${resetCss}
    ${cssClasses}

    ${styles}
  </style>
`;

template.innerHTML = /*html*/`
  ${style}
  ${htmlTemplate(mockData)}
`;

class MultipleOrders extends HTMLElement {
  constructor() {
    super();
    this.attachShadow({ mode: "open" });
  }

  connectedCallback() {
    this.shadowRoot.appendChild(template.content.cloneNode(true));
  }
}

customElements.define("c-multiple-orders", MultipleOrders);
