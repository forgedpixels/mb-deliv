const showOrders = (data = {alerts: []}) => {
  return data.orders.map((val, i) => `
    <li class="component-bg">
      <single-order bgStyle="#fff" headerUse="true"></single-order>
    </li>
  `).join('');
};

export default (data = {}) => `
  <div class="orders-container">
    <ol>
      ${showOrders(data)}
    </ol>
  </div>
`;