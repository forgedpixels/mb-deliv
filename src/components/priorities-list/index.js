import resetCss from '../../assets/styles/reset.js';
import cssClasses from '../../assets/styles/classes.js';
import chevronIcon from '../../assets/images/icons/chevron.js';
import importantIcon from '../../assets/images/icons/important.js';
import medImportantIcon from '../../assets/images/icons/med-important.js';
import closeThinIcon from '../../assets/images/icons/close-thin.js';
import closeIcon from '../../assets/images/icons/close.js';
import favStarIcon from '../../assets/images/icons/fav-star.js';
import paperClipIcon from '../../assets/images/icons/paperclip.js';
import dotMenuIcon from '../../assets/images/icons/dot-menu.js';
import mailIcon from '../../assets/images/icons/mail.js';
import calendarIcon from '../../assets/images/icons/calendar.js';
import extraOptsDropdown from '../../assets/styles/extra-opts-dropdown.js';
import selectInputCss from '../../assets/styles/select-input.js';
import styles from './styles.js';
import newMsgStyles from '../../assets/styles/new-msg.js';
import htmlTemplate from './template.js';

const urlParams = new URLSearchParams(window.location.search);

class PrioritiesList extends HTMLElement {
  constructor() {
    super();
  
    // NOTE: set as property by parent, or if you're able to control without using shadow-dom, can just be done in CSS
    // this.barePadding = 'true';
    this.inFlipComponent = this.getAttribute("inFlipComponent");
    this.showAmt = this.getAttribute("showAmt");
    this.attachShadow({ mode: "open" });


    this.template = document.createElement("template");
    this.template.innerHTML = /*html*/`
      ${this.getStyles()}
      ${htmlTemplate(this.getMockData())}
    `;
  }

  getStyles() {
    return `
      <style>
      ${resetCss}
      ${cssClasses}
      ${selectInputCss}
      ${extraOptsDropdown}
      ${newMsgStyles}

      ${styles}
      </style>
    `;
  }

  getMockData = () => {
    const showAmt = this.getAttribute("showAmt");
    const inFlipComponent = this.getAttribute("inFlipComponent");

    let mockData = {
      images: {
        chevronIcon,
        importantIcon,
        closeIcon,
        dotMenuIcon,
        medImportantIcon,
        closeThinIcon,
        paperClipIcon,
        favStarIcon,
        mailIcon,
        calendarIcon
      },
      data: {
        isShowingSort: urlParams.get('design') === '5',
        showUndoBtn: urlParams.get('design') === '6',
        isShowingPriorOpts: urlParams.get('design') === '7'
      },
      priorities: [
        {
          starred: true,
          priorityType: 'high',
          name: 'High Spending',
          desc: 'Watch your stock count, and double check for accuracy to avoid alerts, overall waste.',
          time: '9 mins ago',
          cta: 'Review Orders',
          ctaLink: 'http://google.com'
        },
        {
          starred: false,
          priorityType: 'med',
          name: 'Store Rank low',
          desc: 'Watch your stock count, and double check for accuracy to avoid alerts, overall waste.',
          time: '12 mins ago',
          cta: 'Review Orders',
          ctaLink: 'http://google.com'
        },
        {
          starred: false,
          priorityType: 'low',
          name: 'High Spending',
          desc: 'Watch your stock count, and double check for accuracy to avoid alerts, overall waste.',
          time: '23 mins ago',
          cta: 'Review Orders',
          ctaLink: 'http://google.com'
        },
        {
          starred: false,
          priorityType: 'none',
          name: 'High Spending',
          desc: 'Watch your stock count, and double check for accuracy to avoid alerts, overall waste.',
          time: '31 mins ago',
          cta: 'Review Orders',
          ctaLink: 'http://google.com'
        },
        {
          starred: true,
          priorityType: 'none',
          name: 'Quality Claim 12',
          desc: 'Watch your stock count, and double check for accuracy to avoid alerts, overall waste.',
          time: '36 mins ago',
          cta: 'Review Orders',
          ctaLink: 'http://google.com'
        },
        {
          starred: false,
          priorityType: 'none',
          name: 'Quality Claim 3',
          desc: 'Watch your stock count, and double check for accuracy to avoid alerts, overall waste.',
          time: '45 mins ago',
          cta: 'Review Orders',
          ctaLink: 'http://google.com'
        },
      ],
      inFlipComponentClass: inFlipComponent ? 'inflip-component' : ''
    };


    // NOTE to be passed in as a prop to Single order Component to display dark styles of order tracker
    if (showAmt) {
      mockData.priorities = mockData.priorities.slice(0, 3);
    }
  
    return mockData;
  }

  connectedCallback() {
    this.shadowRoot.appendChild(this.template.content.cloneNode(true));
  }
}

customElements.define("c-priorities-list", PrioritiesList);
