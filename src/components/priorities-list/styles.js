export default `
a {
  color: #fff;
}

.priority-drop-opts {
  position: fixed;
  bottom: 0;
  left: 0;
  width: 100%;
  background: #F5F5F5;
  z-index: 2;
}

.priority-drop-opts,
.editing .msg-received .priority-opts-dots > button,
.editing footer .btn-dismiss {
  display: none;
}

.priority-drop-opts li button {
  display: flex;
  align-items: center;
}

.editing .priority-drop-opts.show {
  display: block;
}

.priority-drop-opts.show {
  z-index: 2;
}

.priority-drop-opts svg {
  width: 1rem;
  height: 1rem;
  margin-right: .5rem;
}

.priority-drop-opts svg path {
  fill: none;
}

.high-priority-icon svg path{
  fill: #f19d9e;
}

.med-priority-icon svg path {
  fill: #ff9a2e;
}

.path-fill-icon svg path {
  stroke: #0099FF;
}

.circle-icon {
  display: inline-block;
  margin-right: .5rem;
  background: #0099FF;
  width: 1rem;
  height: 1rem;
  border-radius: 50%;
}

.priority-list {
  height: 100%;
}

.priority-list > ul > li {
  position: relative;
  border-bottom: 1px solid #949494;
}

.priority-list > ul > li:last-child {
  border-bottom: none;
  padding-bottom: 3.25rem;
}

.priority-list h3 {
  position: relative;
  text-transform: uppercase;
}

.priority-list h3.high-priority {
  color: #F19D9D;
}

.priority-list h3.high-priority svg,
.priority-list h3 .starred-icon svg {
  width: 11px;
  height: 11px;
}

.priority-list h3.high-priority svg path {
  fill: #F19D9D
}

.priority-list h3.med-priority {
  color: #ff9a2e;
}

.priority-list h3.low-priority {
  color: #029aff;
}

.priority-list h3 .starred-icon {
  position: absolute;
  left: -1rem;
}

.priority-list h3 .starred-icon svg path {
  fill: #EFCB9B;
  fill-opacity: 1;
}

.priority-content {
  padding: 1.75rem;
  color: #CFCFCF;
}

.priority-content header {
  display: flex;
  flex-wrap: nowrap;
  justify-content: space-between;
  height: 2rem;
}

.priority-content header .msg-received .priority-opts-dots > button:first-child {
  cursor: pointer;
  width: 2rem;
  height: 2rem;
  margin-right: -1rem;
}

.editing .priority-content header .msg-received > span,
.priority-content header .msg-received .priority-opts-dots,
li:hover .priority-content header .msg-received > span {
  display: none;
}


.editing .priority-content header .msg-received .priority-opts-dots,
li:hover .priority-content header .msg-received .priority-opts-dots {
  display: block;
}

.editing .priority-content header .msg-received .priority-opts-dots > button:first-child
li:hover .priority-content header .msg-received .priority-opts-dots > button:first-child {
  transform: rotate(90deg);
  cursor: pointer;
}

.editing .priority-content header .msg-received .priority-opts-dots > button:first-child svg path,
li:hover .priority-content header .msg-received .priority-opts-dots > button:first-child svg path {
  fill: #fff;
}

.priority-content .msg-received {
  color: #D0DAE2;
}


.priority-content p {
  margin-top: 0;
  margin-bottom: 1rem;
  max-width: 75%;
}

.priority-content a {
  text-decoration: underline;
}

.priority-content footer {
  display: flex;
  flex-wrap: nowrap;
  justify-content: space-between;
  align-items:center;
}

.priority-content .btn-dismiss {
  visibility: hidden;
  text-decoration: none;
  border: 0.5px solid #A2A2A2;
  border-radius: 68px;
  padding: .2rem .5rem;
}

.priority-content:hover .btn-dismiss,
.editing .btn-dismiss {
  visibility: visible;
}

.priority-opts {
  display: flex;
  align-content: stretch;
  position: absolute;
  top: 0;
  left: calc(100% + 2.5rem);
  width: 16.5rem;
  height: 100%;
  transition: left .5s;
}

.priority-opts .icon {
  display: flex;
  margin: 0 auto;
  width: 40px;
  height: 40px;
  align-items: center;
}

.priority-opts .icon-more svg {
  transform: rotate(90deg);
}

.priority-opts .icon-more svg path {
  fill: #fff;
}

.priority-opts button {
  height: 100%;
  width: 5.5rem;
  background: #0099FF;
  color: #fff;
}

.priority-opts button svg {
  display: block;
  margin: 0 auto 16px;
}

.priority-opts button:nth-child(2) {
  background: #0280d5;
}

.priority-list li.editing .priority-content {
  position: relative;
  left: -16.5rem;
}

.priority-list li.editing .priority-opts {
  left: calc(100% - 16.5rem);
}



@media (min-width: 1290px) {

.priority-drop-opts {
  position: absolute;
  bottom: inherit;
  left: auto;
  top: inherit;
  right: inherit;
  width: 10rem;
  background: #F5F5F5;
  z-index: 2;
  padding: .75rem 1.5rem;
}

.priority-drop-opts.show {
  right: 0;
  width: 14rem;
  margin-top: -.5rem;
}

.editing .msg-received,
.editing footer .btn-dismiss {
  display: block;
}

.priority-drop-opts:before {
  display: none;
}

 .priority-list .priority-content {
   padding-left: 0;
   padding-right: 0;
 }

 .priority-content p,
 .priority-list h3,
 .priority-content .msg-received,
 .priority-content a {
   font-size: .75rem;
 }

 .priority-content p {
   margin-top: -1rem;
 }

 .priority-list li.editing .priority-content {
   left: inherit;
 }

 .priority-list li .priority-opts {
   display: none;
 }
}






/* STYLES FOR WHEN IN FLIP COMPONENT*/


.inflip-component .priority-content {
  padding: 1.6rem 0;
}

.inflip-component .priority-content p {
  margin-top: -.5rem;
  margin-bottom: .5rem;
}

.inflip-component .priority-content .msg-received {
  font-size: .75rem;
}

.inflip-component .priority-content h3.high-priority svg,
.inflip-component .priority-content h3 .starred-icon svg{
  width: 13px;
  height: 13px;
}

.priority-list > ul > li:last-child .priority-content {
  margin-bottom: 3.25rem;
  padding-bottom: 0;
}

@media (min-width: 1290px) {
  .inflip-component .priority-content h3,
  .inflip-component .priority-content p,
  .inflip-component .priority-content a.cta {
    font-size: 1rem;
  }
}
`;