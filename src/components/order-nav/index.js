import resetCss from '../../assets/styles/reset.js';
import cssClasses from '../../assets/styles/classes.js';
import breadcrumbCss from '../../assets/styles/breadcrumbs.js';
import styles from './styles.js';
import htmlTemplate from './template.js';

const mockData = {
  showExtrasDropdown: window.location.href.indexOf('orders-selected.html') >= 1
}

const template = document.createElement("template");
const style = `
  <style>
    ${resetCss}
    ${cssClasses}
    ${breadcrumbCss}

    ${styles}
  </style>
`;

template.innerHTML = /*html*/`
  ${style}
  ${htmlTemplate(mockData)}
`;

class OrderNav extends HTMLElement {
  constructor() {
    super();
    this.attachShadow({ mode: "open" });

    this.setTableView();
  }

  setTableView() {
    // default value of table
    localStorage.setItem('table-view', 'All');
  }

  connectedCallback() {
    this.shadowRoot.appendChild(template.content.cloneNode(true));
  }
}

customElements.define("c-ordernav", OrderNav);
