export default `
  .order-nav {
    padding: 1.75rem 1.9375rem .2rem;
  }

  .c-order-view-status {
    margin-bottom: 1.5rem;
  }

  .search-bar-and-opts > div {
    max-width: calc(740px - 2.9375rem);
  }

  @media (min-width: 1290px) {
    .order-nav {
      padding-top: 0;
    }
  
    .search-bar-and-opts,
    .c-order-view-status,
    .c-select-order-type {
      padding-left: 1rem;
    }
  }
`;