export default data => `
  <div class="order-nav">
    <c-breadcrumbs></c-breadcrumbs>

    <div class="search-bar-and-opts content-max-and-center">
      <div class="content-to-ends">
        <div class="search-bar">
          <c-searchbar></c-searchbar>
        </div>

        ${ data.showExtrasDropdown ? `
        <div class="order-opts">
          <c-orderopts></c-orderopts>
        </div>
        ` : ``}
      </div>
    </div>
  </div>
`;