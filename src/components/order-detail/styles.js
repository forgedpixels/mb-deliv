export default `
  .order-empty {
    background: #F5F5F5;
    display: flex;
    align-items: center;
    height: 100%;
    justify-content: center;
    color: #D8D8D8;
    text-align: center;
  }

  .order-empty p {
    font-size: 2.25rem;
    font-weight: bold;
    max-width: 13rem;
    padding: 2rem 0;
  }

  .close-btn {
    position: absolute;
    top: 4rem;
    right: .85rem;
  }

  .awaiting-approval {
    font-size: .875rem;
    display: flex;
    align-items: center;
  }

  .order-detail {
    position: relative;
    background: #fff;
    height: 100%;
    padding: 4rem 1.375rem;
    color: #041C2C;
    text-align: left;
  }

  .order-detail-stock {
    font-size: 0.625rem;
    font-weight: bold;
    text-transform: uppercase;
  }

  .order-detail-stock span {
    color: #000;
  }

  h1 {
    font-size: 1.5rem;
    margin-bottom: 1rem;
  }

  .order-stock-detail {
    position: relative;
    border-radius: 20px;
    padding: 0.875rem 1.5625rem;
    overflow-x: auto;
    margin: 0 -1.375rem;
  }

  .order-stock-detail > button {
    display: none;
  }

  .order-stock-detail > button svg {
    transform: rotate(180deg);
  }

  .order-stock-detail.expand > button svg {
    transform: none;
  }

  .order-stock-detail > button svg path {
    fill: black;
  }

  .order-stock-detail > div {
    white-space: nowrap;
    font-size: 0;
  }

  .stock-detail-indiv {
    display: inline-block;
    font-size: 1.5rem;
    padding: 1rem;
    min-width: 4.5rem;
    background: #f8f8f8;
  }

  .stock-detail-indiv:first-child {
    padding-left: 2rem;
    border-radius: 20px 0 0 20px
  }

  .stock-detail-indiv:last-child {
    margin-right: 1.375rem;
    padding-right: 1.5rem;
    border-radius: 0 20px 20px 0;
  }

  .stock-detail-indiv span {
    display: block;
    font-weight: bold;
    font-size: 0.875rem;
    white-space: nowrap;
  }

  .order-detail > .btn {
    float: left;
    margin-top: 0.625rem;
    border:  0.5px solid #041C2C;
    border-radius: 68px;
    padding: 0.125rem 0.375rem; 
  }

  .donut-metric {
    float: right;
  }

  .order-product-details {
    clear: both;
  }

  .donut-type {
    max-width: 3.875rem;
    order: 0;
    font-size: .67rem;
  }

  .donut-chart {
    order: 1;
  }

  figcaption {
    margin-bottom: 1rem;
  }

  figcaption p {
    margin-bottom: 1rem;
  }

  figure img {
    margin: -1.5rem 0 1.875rem;
  }

  .product-nutrition .btn {
    display: flex;
    font-size: 1.5rem;
    width: 100%;
  }

  .product-nutrition svg {
    transform: rotate(180deg);
  }

  .product-nutrition .toggle-nutrition-btn.enabled svg {
    transform: none;
  }

  .product-nutrition svg path {
    fill: #041C2C;
  }

  .product-nutrition .nutrition-info {
    display: none;
  }

  .product-nutrition .nutrition-info.show {
    display: block;
  }

  .nutrition-info h3 {
    font-size: 1.5rem;
    padding: 1rem 0;
  }

  .nutrition-info tr,
  .nutrition-info th {
    padding-right: .5rem;
  }

  .nutrition-info table th {
    text-transform: uppercase;
    text-align: left;
  }

  td {
    cursor: pointer;
  }

  @media (min-width: 1290px) {
    .close-btn {
      top: 1rem;
      right: 1rem;
    }

    .order-detail header {
      display: flex;
      justify-content: space-between;
    }

    .awaiting-approval {
      
    }

    .order-detail {
      padding-top: 1.375rem;
      padding-bottom: 1.375rem;
    }

    .order-detail .btn {
      clear: both;
    }

    .order-stock-detail {
      max-width: 350px;
      padding: 0;
      float: left;
      overflow: hidden;
      width: 100%;
    }

    .order-stock-detail > div {
      white-space: normal;
      max-width: 350px;
      display: flex;
      flex-wrap: wrap;
      background: #f8f8f8;
      border-radius: 20px;
    }

    .order-stock-detail > button {
      position: absolute;
      top: 1rem;
      right: 1rem;
      display: block;
    }

    .stock-detail-indiv {
      /*display: none;*/
      width: 33%;
      max-height: 0;
      transition: max-height .5s, padding .5s;
    }

    .stock-detail-indiv:last-child {
      max-height: 0;
      margin-right: 0;
    }

    .stock-detail-indiv:nth-child(4),
    .stock-detail-indiv:nth-child(5),
    .stock-detail-indiv:nth-child(6) {
      padding: 0;
    }

    .stock-detail-indiv:nth-child(1),
    .stock-detail-indiv:nth-child(2),
    .stock-detail-indiv:nth-child(3),
    .order-stock-detail.expand .stock-detail-indiv {
      max-height: 4.875rem;
    }

    .order-stock-detail.expand .stock-detail-indiv:nth-child(4),
    .order-stock-detail.expand .stock-detail-indiv:nth-child(5),
    .order-stock-detail.expand .stock-detail-indiv:nth-child(6) {
      padding: 1rem;
    }

    .stock-detail-indiv:nth-child(3) {
      padding-right: 3.5rem;
    }

    .stock-detail-indiv:nth-child(3) {
      padding-right: 1.5625rem;
    }

    .order-stock-detail.expand .stock-detail-indiv:nth-child(4),
    .stock-detail-indiv:nth-child(3n+1) {
      padding-left: 2rem;
    }

    .order-product-details figure,
    .nutrition-info table{
      font-size: 0.875rem;
    }

    .nutrition-info table {
      width: 100%;
    }

    .order-product-details figure img {
      margin-top: 0;
    }

    .order-product-details figure p {
      max-width: 22.5rem;
    }

    .donut-type {
      background: #fff;
      position: absolute;
      top: 0;
      left: -2rem;
      max-width: 3rem;
      z-index: 1;
    }
  }






  .edit-order {
    display: none;
    position: absolute;
    top: 0;
    left: 0;
    background: rgba(0,0,0,.7);
    padding-top: 30%;
    opacity: 0;
    width: 100%;
    height: 100%;
  }

  .edit-order.show {
    display: block;
    opacity: 1;
    transition: opacity 1s;
  }

  @media (min-width: 1290px) {
    .edit-order {
      margin-top: 2rem;
      padding-top: 0;
      background: #fff;
    }
  }
`;