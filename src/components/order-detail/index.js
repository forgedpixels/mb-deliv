import resetCss from '../../assets/styles/reset.js';
import cssClasses from '../../assets/styles/classes.js';
import donutCss from '../../assets/styles/donut.js';
import styles from './styles.js';
import dataTemplate from './data-template.js';
import defaultTemplate from './default-template.js';
import chevronIcon from '../../assets/images/icons/chevron.js';
import arrowIcon from '../../assets/images/icons/arrow-long.js';
import checkIcon from '../../assets/images/icons/check.js';
import closeIcon from '../../assets/images/icons/close.js';

localStorage.removeItem('editing-order');

const mockData = {
  images: {
    arrowIcon,
    chevronIcon,
    checkIcon,
    closeIcon
  },
  order: {
    orderNo: 10018,
    stockDetail: {
      onHand: '3 CS',
      proposed: '1 CS',
      promo: true,
      inTransit: '1 CS',
      confirmed: '4 CS',
      buildTo: '4 CS',
      proposalPercent: 99,
    },
    product: {
      name: 'Bacon Roll',
      imgPath: './src/assets/images/stock/baconRoll.png',
      desc: 'It’s a classic – delicious bacon with a choice of ketchup or brown sauce, served on a soft, white roll.',
      nutrition: [
        {
          type: 'kj',
          amt: 1478,
          percent: 18
        },
        {
          type: 'kcal',
          amt: 351,
          percent: 18
        },
        {
          type: 'fat',
          amt: 11,
          percent: 16
        },
      ],
    },
  }
};

const parseData = (data) => {
  const orderDetailsShow = localStorage.getItem('order-selected');
  const editingOrder = localStorage.getItem('editing-order');
  const optSelected = true; // NOTE: to be changed by whichever state logic is used in LWC
  const kj = data.order.product.nutrition.filter(val => val.type ==='kj');
  const kcal = data.order.product.nutrition.filter(val => val.type ==='kcal');
  const parsed = {
    ...data,
    order: {
      ...data.order,
      kj: kj[0],
      kcal: kcal[0]
    },
    orderDetailsShow,
    editingOrder,
    optSelected
  };
  return parsed;
};

const template = document.createElement("template");
const style = `
  <style>
    ${resetCss}
    ${cssClasses}
    ${donutCss}

    ${styles}
  </style>
`;

const getTemplate = data => {
  const parsed = parseData(data);

  if (parsed.orderDetailsShow) {
    return dataTemplate(parsed);
  } else {
    return defaultTemplate;
  }
}

template.innerHTML = /*html*/`
  ${style}
  ${getTemplate(mockData)}
`;

class OrderDetail extends HTMLElement {
  constructor() {
    super();
    this.attachShadow({ mode: "open" });

    this.orderDetailShow = localStorage.getItem('order-selected');
    this.editingOrder = localStorage.getItem('editing-order');

    window.addEventListener('storage', () => {
      const orderDetailShow = localStorage.getItem('order-selected');
      const editingOrder = localStorage.getItem('editing-order');
      // When local storage changes, dump the list to
      // the console.
      if (this.orderDetailShow !== orderDetailShow || this.editingOrder !== editingOrder) {
        this.orderDetailShow = orderDetailShow;
        this.resetListeners();
      }
    }, false);
  }

  resetListeners() {
    this.removeListeners();
    this.render();
    this.setListeners();
  }

  connectedCallback() {
    this.shadowRoot.appendChild(template.content.cloneNode(true));
  }

  toggleContent() {
    this.shadowRoot.querySelector(".toggle-nutrition-btn").classList.toggle('enabled');
    this.shadowRoot.querySelector(".nutrition-info").classList.toggle('show')
  }

  render() {
    this.shadowRoot.innerHTML = `
      ${style}
      ${getTemplate(mockData)}
    `;
  }

  removeListeners() {
    const toggleNutritionBtn = this.shadowRoot.querySelector(".toggle-nutrition-btn");
    if (toggleNutritionBtn) {
      toggleNutritionBtn.onclick = null;
    }
  }

  setListeners() {
    this.shadowRoot.querySelector(".toggle-nutrition-btn").onclick = () => this.toggleContent();
    this.shadowRoot.querySelector(".toggle-stockdetail-btn").onclick = () => this.toggleStockDetail();
    this.shadowRoot.querySelector(".order-detail > .edit-btn").onclick = () => this.goToEdit();
  }

  toggleStockDetail() {
    this.shadowRoot.querySelector(".order-stock-detail").classList.toggle('expand')
  }

  goToEdit() {
    localStorage.setItem('editing-order', true);
    window.dispatchEvent( new Event('storage') );
  }
}

customElements.define("c-order-detail", OrderDetail);
