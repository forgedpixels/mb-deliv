const createRows = (orders = []) => {
  return orders.map((val) => `
    <tr>
      <td>${val.type}</td>
      <td>${val.amt}</td>
      <td>${val.percent} RI</td>
    </tr>
  `).join('');
};

export default data => `
  <div class="order-detail">
    <button class="close-btn">${data.images.closeIcon}</button>

    <header>
      <div class="order-name">
        <span class="order-detail-stock bright-blue"><span>${data.order.orderNo}</span> in stock</span>
        <h1>${data.order.product.name}</h1>
      </div>
      <div class="awaiting-approval">
        <span class="icon-circle icon-circle-small icon-circle-alert"></span> Awaiting Approval
      </div>
    </header>

    <section class="order-stock-detail font-light">
      <div>
        <div class="on-hand stock-detail-indiv">
          <span>On hand</span>
          ${data.order.stockDetail.onHand}
        </div>
        <div class="proposed stock-detail-indiv">
          <span>Proposed</span>
          ${data.order.stockDetail.proposed}
        </div>
        <div class="promo stock-detail-indiv">
          <span>Promo</span>
          ${data.order.stockDetail.promo ? data.images.checkIcon : ``}
        </div>
        <div class="build-to stock-detail-indiv">
          <span>In transit</span>
          ${data.order.stockDetail.buildTo}
        </div>
        <div class="in-transit stock-detail-indiv">
          <span>Confirmed</span>
          ${data.order.stockDetail.inTransit}
        </div>
        <div class="confirmed stock-detail-indiv">
          <span>Build to</span>
          ${data.order.stockDetail.confirmed}
        </div>
      </div>
      <button class="toggle-stockdetail-btn">${data.images.chevronIcon}</button>
    </section>

    <div class="donut-metric content-center-vert">
      <div class="donut-chart item css">
        <svg class="circle-chart" viewbox="0 0 33.83098862 33.83098862" width="200" height="200" xmlns="http://www.w3.org/2000/svg">
          <circle class="circle-chart__circle" stroke="#0099FF" stroke-width="2" stroke-dasharray="99,100" stroke-linecap="solid" fill="none" cx="16.91549431" cy="16.91549431" r="15.91549431" />
        </svg>
        <span class="bright-blue">99%</span>
      </div>
      <span class="donut-type bright-blue">Proposal Accuracy</span>
    </div>

    <button class="btn edit-btn">Edit Item</button>

    <section class="order-product-details">
      <figure>
        <img src="${data.order.product.imgPath}" alt="${data.order.product.name}"/>
        <figcaption><p>${data.order.product.desc}</p> <span>${data.order.kj.amt} kJ / ${data.order.kcal.amt} kCal</span></figcaption>
      </figure>
      
      <section class="product-nutrition">
        <button class="toggle-nutrition-btn btn content-to-ends content-center-vert">Ingredients + Allergens ${data.images.chevronIcon}</button>

        <div class="nutrition-info">
          <h3>Nutrition Summary</h3>
          <table>
            <tr>
              <th>Information</th>
              <th>Per</th>
              <th>%RI (Adult)</th>
            </tr>
            ${createRows(data.order.product.nutrition)}
          </table>
        </div>
      </section>
    </section>




    <div class="edit-order ${data.editingOrder ? 'show' : ''}">
      <c-editorder></c-editorder>
    </div>
  </div>
`;