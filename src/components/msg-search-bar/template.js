export default (data = {}) => `
  <div class="mail-search">
    <h1>Messages</h1>
    
    <div class="form__group field">
      <span class="form__icon">${data.images.searchIcon}</span>
      <input type="input" class="form__field" placeholder="Name" name="name" required />
      <label for="name" class="form__label font-light">Search</label>
    </div>
  </div>
`;