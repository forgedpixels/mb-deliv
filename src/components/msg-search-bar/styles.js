export default `
  .mail-search {
    position: relative;
    padding-left: 1rem;
    padding-top: .5rem;
  }

  .mail-search:before {
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    content: ' ';
    background: url(src/assets/images/icons/mail.svg) 0 0/36px no-repeat;
    z-index: -1;
    opacity: .4;
  }

  .mail-search h1 {
    font-size: 1.5rem;
  }

  @media (min-width: 1290px) {
    .mail-search h1 {
      font-size: 3rem;
    }

    .mail-search:before {
      background-size: 50px;
    }
  }
`;