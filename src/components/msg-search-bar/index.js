import resetCss from '../../assets/styles/reset.js';
import cssClasses from '../../assets/styles/classes.js';
import searchBarCss from '../../assets/styles/search-bar.js';
import searchIcon from '../../assets/images/icons/search.js';
import styles from './styles.js';
import htmlTemplate from './template.js';

const data = {
  images: {
    searchIcon
  },
  orderNo: 7281169
};

const template = document.createElement("template");
const style = `
  <style>
    ${resetCss}
    ${cssClasses}
    ${searchBarCss}

    ${styles}
  </style>
`;

template.innerHTML = /*html*/`
  ${style}
  ${htmlTemplate(data)}
`;

class MsgSearchBar extends HTMLElement {
  constructor() {
    super();
    this.attachShadow({ mode: "open" });
  }

  connectedCallback() {
    this.shadowRoot.appendChild(template.content.cloneNode(true));
  }
}

customElements.define("c-msg-searchbar", MsgSearchBar);
