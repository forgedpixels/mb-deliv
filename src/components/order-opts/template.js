export default (data) => `
  <div class="msg-opts-menu">
    <div class="quick-opts">
      <button class="dots">${data.images.dotIcon}</button>
    </div>
    <div class="extra-opts ${data.showExtraOpts}">
      <div class="menu-box-shadow-on-lg select-dropdown-styled">
        <ul>
          <li>
          <button class="forward">${data.images.replyIcon} <span>Forward</span></button>
          </li>
          <li>
          <button>${data.images.printerIcon} <span>Print</span></button>
          </li>
          <li>
          <button class="mail">${data.images.mailIcon} <span>Mark as unread</span></button>
          </li>
          <li>
          <button>${data.images.archiveIcon} <span>Archive</span></button>
          </li>
          <li>
          <button>${data.images.shareIcon} <span>Share</span></button>
          </li>
          <li>
          <button class="calendar">${data.images.calendarIcon} <span>Add event</span></button>
          </li>
          <li>
          <button>${data.images.filterIcon} <span>Filter Similar</span></button>
          </li>
        </ul>
      </div>
    </div>
  </div>
`;