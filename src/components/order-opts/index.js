import resetCss from '../../assets/styles/reset.js';
import cssClasses from '../../assets/styles/classes.js';
import extraOptsDropdown from '../../assets/styles/extra-opts-dropdown.js';
import styles from './styles.js';
import htmlTemplate from './template.js';
import paperClipIcon from '../../assets/images/icons/paperclip.js';
import closeIcon from '../../assets/images/icons/close.js';
import closeThinIcon from '../../assets/images/icons/close-thin.js';
import chevronIcon from '../../assets/images/icons/chevron.js';
import replyIcon from '../../assets/images/icons/reply.js';
import heartIcon from '../../assets/images/icons/heart.js';
import dotIcon from '../../assets/images/icons/dot-menu.js';
import printerIcon from '../../assets/images/icons/printer.js';
import shareIcon from '../../assets/images/icons/share.js';
import archiveIcon from '../../assets/images/icons/archive.js';
import calendarIcon from '../../assets/images/icons/calendar.js';
import filterIcon from '../../assets/images/icons/filter.js';
import mailIcon from '../../assets/images/icons/mail.js';

const urlParams = new URLSearchParams(window.location.search);
const mockData = {
  images: {
    paperClipIcon,
    closeIcon,
    chevronIcon,
    closeThinIcon,
    replyIcon,
    heartIcon,
    dotIcon,
    printerIcon,
    shareIcon,
    archiveIcon,
    filterIcon,
    calendarIcon,
    mailIcon
  },
  showExtraOpts: urlParams.get('design') === '3' ? 'show' : ''
};

const template = document.createElement("template");
const style = `
  <style>
    ${resetCss}
    ${cssClasses}
    ${extraOptsDropdown}

    ${styles}
  </style>
`;

template.innerHTML = /*html*/`
  ${style}
  ${htmlTemplate(mockData)}
`;

class OrderOpts extends HTMLElement {
  constructor() {
    super();
    this.attachShadow({ mode: "open" });
  }

  connectedCallback() {
    this.shadowRoot.appendChild(template.content.cloneNode(true));
  }
}

customElements.define("c-orderopts", OrderOpts);
