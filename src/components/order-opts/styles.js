export default `
  .quick-opts button.dots {
    transform: rotate(90deg);
  }

  .quick-opts button.dots svg path {
    fill: #fff;
  }
`;