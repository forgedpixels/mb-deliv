export default `
  nav {
    background: #041C2C;
  }

  nav ul {
    display: flex;
    justify-content: space-around;
  }

  nav ul li {
    display: inlinine-block;
    max-width: 76px;
    width: 20%;
    font-size: 0.625rem;
    text-align: center;
  }

  nav ul a div {
    text-align: center;
    min-height: 35px;
  }

  nav ul a div svg {
    display: inline-block;
  }

  a {
    display: block;
    
    color: #fff;
    font-weight: bold;
    padding: 1.125rem 0;
  }

  svg {
    display: block;
  }

  @media (min-width: 1290px) {
    nav {
      display: none;
    }
  }
`;