import resetCss from '../../assets/styles/reset.js';
import cssClasses from '../../assets/styles/classes.js';
import broomIcon from '../../assets/images/icons/broom.js';
import ordersIcon from '../../assets/images/icons/orders.js';
import calendarIcon from '../../assets/images/icons/calendar.js';
import mailIcon from '../../assets/images/icons/mail.js';
import chartIcon from '../../assets/images/icons/chart.js';
import styles from './styles.js';
import htmlTemplate from './template.js';

const template = document.createElement("template");

const mockData = {
  images: {
    ordersIcon,
    calendarIcon,
    broomIcon,
    mailIcon,
    chartIcon
  }
};

template.innerHTML = /*html*/`
  <style>
    ${resetCss}
    ${cssClasses}

    ${styles}
  </style>
  
  ${htmlTemplate(mockData)}
`;

class NavBar extends HTMLElement {
  constructor() {
    super();
    this.attachShadow({ mode: "open" });
  }

  connectedCallback() {
    this.shadowRoot.appendChild(template.content.cloneNode(true));
  }
}

customElements.define("c-navbar", NavBar);
