export default (data) => `
  <nav>
    <ul>
      <li>
        <a href="">
        <div class="nav-icon">
          ${data.images.ordersIcon}
        </div>

        Orders
        </a>
      </li>
      <li>
        <a href="">
        <div class="nav-icon">
          ${data.images.calendarIcon}
        </div>

        Calendars
        </a>
      </li>
      <li>
        <a href="">
          <div class="nav-icon">
          ${data.images.broomIcon}
          </div>

        Ops Supply
        </a>
      </li>
      <li>
        <a href="">
        <div class="nav-icon">
          ${data.images.mailIcon}
        </div>

        Messages
        </a>
      </li>
      <li>
        <a href="">
        <div class="nav-icon">
          ${data.images.chartIcon}
        </div>

        Numbers
        </a>
      </li>
    </ul>
  </nav>
`;