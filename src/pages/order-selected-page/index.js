import styles from './styles.js';
import cssClasses from '../../assets/styles/classes.js';
import htmlTemplate from './templates.js';

const template = document.createElement("template");
const data = {
  orderDetailsShow: false
};
const style = `
  <style>
    
  ${cssClasses}
  ${styles}
  </style>
`;
template.innerHTML = /*html*/`
  ${style}
  
  ${htmlTemplate(data)}
`;



const getNewData = () => {
  const orderDetailsShow = localStorage.getItem('order-selected');

  const hm = {
    orderDetailsShow
  }

  return hm;
};

class OrderPage extends HTMLElement {
  constructor() {
    super();
    this.attachShadow({ mode: "open" });

    this.orderDetailShow = null;

    window.addEventListener('storage', () => {
      const orderDetailShow = localStorage.getItem('order-selected');
      // When local storage changes, dump the list to
      // the console.
      if (this.orderDetailShow !== orderDetailShow) {
        this.orderDetailShow = orderDetailShow;
        this.render();
      }
    }, false);

    this.setBodyClass();
  }



  setBodyClass() {
    document.body.classList.add('solid-partial-bg');
  }

  connectedCallback() {
    this.shadowRoot.appendChild(template.content.cloneNode(true));
  }

  render() {
    this.shadowRoot.innerHTML = `
      ${style}
      ${htmlTemplate(getNewData())}
    `;
  }
}

customElements.define("order-selected-view", OrderPage);
