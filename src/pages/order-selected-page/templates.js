export default (data) => `
<div class="page">
  <header>
    <slot name="header"></slot>
  </header>

  <div class="order-nav">
    <slot name="ordernav"></slot>
  </div>

  <div class="c-order-view-status content-max-and-center">
    <c-order-view-status></c-order-view-status>
  </div>

  <div class="c-select-order-type content-max-and-center">
    <c-select-order-type></c-select-order-type>
  </div>

  <div class="order-details content-max-and-center">
    <section class="order-table">
      <slot name="ordertable"></slot>
    </section>
    <section class="order-detail ${data.orderDetailsShow ? 'show': ''}">
      <slot name="orderdetail"></slot>
    </section>
  </div>
</div>
`;