import styles from './styles.js';
import cssClasses from '../../assets/styles/classes.js';
import htmlTemplate from './templates.js';

const template = document.createElement("template");
const data = {
  orderDetailsShow: false
};
const style = `
  <style>
    
  ${cssClasses}
  ${styles}
  </style>
`;
template.innerHTML = /*html*/`
  ${style}
  
  ${htmlTemplate(data)}
`;


class OrderPage extends HTMLElement {
  constructor() {
    super();
    this.attachShadow({ mode: "open" });
    this.setBodyClass();

    window.addEventListener('menu-hover', () => {
      this.shadowRoot.querySelector("header").classList.add('hovering')
    }, false);
    window.addEventListener('menu-hover-out', () => {
      this.shadowRoot.querySelector("header").classList.remove('hovering')
    }, false);
  }


  setBodyClass() {
    document.body.classList.add('solid-partial-bg');
  }

  connectedCallback() {
    this.shadowRoot.appendChild(template.content.cloneNode(true));
  }
}

customElements.define("orders-view", OrderPage);
