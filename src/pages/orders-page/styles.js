export default `

  .order-detail {
    display: none;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
  }

  .order-detail.show {
    display: block;
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    z-index: 2;
    background: #fff;
  }

  .order-nav {
    position: relative;
    margin-bottom: 2rem;
  }

  .c-select-orders-tab {
    padding: 0 1rem;
    box-sizing: border-box;
    margin-bottom: .5rem;
  }

  @media (min-width: 1290px) {
    .order-nav {
      margin-top: 3.25rem;
    }

    .order-details {
      display: flex;
      flex-wrap: nowrap;
      align-items: stretch;
      align-content: stretch;
    }
  
    .order-table {
      width: 100%;
      max-width: 740px;
      min-width: 740px;
    }

    .order-detail {
      display: block;
      height: auto;
    }

    .order-detail.show {
      position: relative;
      top: auto;
      left: auto;
      height: auto;
      z-index: 2;
      background: #F5F5F5;
      font-size: 2rem;
      text-align: center;
      max-width: 550px;
    }
  }
`;