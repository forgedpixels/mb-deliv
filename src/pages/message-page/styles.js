export default `
  .mail-folders {
    padding: 0 1.9375rem;
  }

  .mail-messages {
    position: relative;
  }

  .mail-detail {
    display: none;
  }

  .show-msg-detail .mail-detail {
    display: block;
  }

  .show-msg-detail .mail-message-list {
    display: none;
  }

  @media (min-width: 1290px) {
    .msg-nav {
      margin-top: 3.25rem;
    }

    .mail-messages {
      display: flex;
      flex-wrap: nowrap;
      align-items: stretch;
      align-content: stretch;
    }
  
    .mail-message-list {
      width: 100%;
      width: 710px;
    }

    .show-msg-detail .mail-message-list {
      display: block;
    }
  
    .mail-detail {
      display: block;
      height: auto;
      position: relative;
      top: auto;
      left: auto;
      height: auto;
      z-index: 2;
      background: #F5F5F5;
      font-size: 2rem;
      text-align: center;
      width: 580px;
    }

    .show-msg-detail .mail-detail {
      display: block;
    }
  }
`;