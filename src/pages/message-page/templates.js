export default (data) => `
<div class="page">
  <header>
    <slot name="header"></slot>
  </header>

  <div class="msg-nav">
    <slot name="msgpgnav"></slot>
  </div>

  <div class="mail-messages content-max-and-center ${data.showMsgDetailClass}">
    <section class="mail-message-list">
      <slot name="mailmsgs"></slot>
    </section>
    <section class="mail-detail">
      <slot name="msgdetail"></slot>
    </section>
  </div>
</div>
`;