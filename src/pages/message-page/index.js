import styles from './styles.js';
import cssClasses from '../../assets/styles/classes.js';
import htmlTemplate from './templates.js';

const mockData = {};

const parseData = data => {
  const urlParams = new URLSearchParams(window.location.search);
  
  return {
    ...data,
    showMsgDetailClass: urlParams.get('design') === '2' || urlParams.get('design') === '3' ? 'show-msg-detail' : ''
  };
}

const template = document.createElement("template");
const style = `
  <style>
  ${cssClasses}
  ${styles}
  </style>
`;
template.innerHTML = /*html*/`
  ${style}
  
  ${htmlTemplate(parseData(mockData))}
`;
class MessagePage extends HTMLElement {
  constructor() {
    super();
    this.attachShadow({ mode: "open" });

    this.setBodyClass();
  }

  setBodyClass() {
    document.body.classList.add('linear-gradient');
  }

  connectedCallback() {
    this.shadowRoot.appendChild(template.content.cloneNode(true));
  }
}

customElements.define("msg-page", MessagePage);
