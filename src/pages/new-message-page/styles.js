export default `
  .msg-pg-nav {
    padding: 1.75rem 1.9375rem .2rem;
  }

  .new-msg-container {
    padding: 1.875rem 1.875rem 5rem;
    background: #fff;
    max-width: 44rem;
    position: relative;
  }

  .new-msg-container .close {
    position: absolute;
    right: 1rem;
    top: -2.5rem;
  }

  .new-msg-container .close svg {
    width: 2rem;
    height: 2rem;
  }

  .new-msg-container .close svg path {
    fill: #fff;
  }

  .subject-and-addressees {
    margin-bottom: 2.5rem;
  }

  .form__field,
  .form__label,
  .form__field:focus {
    color: #000;
    font-size: 1.125rem;
  }

  .form__field {
    border-bottom: .5px solid #000;
    padding: 0 0 1rem 0;
  }

  .form__field:focus {
    border-width: 1px;
    border-color: #0099FF;
    color: #000;
  }

  .form__group {
    max-width: none;
  }

  .msg-type-container {
    display: flex;
    padding-bottom: 1rem;
    border-bottom: .5px solid #000;
    margin-top: 1rem;
    justify-content: space-between;
    color: #000;
  }

  .msg-type-select > span {
    margin-right: .67rem;
    font-size: 1.125rem;
  }

  .msg-select-btn > button {
    width: 6rem;
    display: flex;
    align-items: center;
    justify-content: space-between;
    font-size: 1rem;
  }

  .msg-send-to-opts button {
    cursor: pointer;
  }

  .msg-type-select svg path {
    fill: #000;
  }

  @media (min-width: 1290px) {
    .msg-pg-nav {
      margin-top: 3.25rem;
    }

    .form__group {
      margin-top: 0;
    }

    .msg-select-btn > button {
      width: 10rem;
    }

    .msg-pg-nav {
      padding-top: 0;
    }

    .current-order-no,
    .form__field,
    .form__label,
    .form__field:focus ~ .form__label,
    .form__field:placeholder-shown ~ .form__label {
      font-size: 1.125rem;
    }

    .new-msg-container {
      padding-top: 4rem;
      padding-bottom: 5rem;
      padding-right: 3.125rem;
    }
    
    .subject-and-addressees {
      margin-bottom: 4rem;
    }

    .new-msg-container .close {
      position: absolute;
      right: 1rem;
      top: 1rem;
    }

    .new-msg-container .close svg {
      width: 2rem;
      height: 2rem;
    }

    .msg-type-dropdown-opts {
      margin-left: 0;
    }

    .new-msg-container .close svg path {
      fill: #0099FF;
    }
  }
`;