import styles from './styles.js';
import resetCss from '../../assets/styles/reset.js';
import cssClasses from '../../assets/styles/classes.js';
import htmlTemplate from './templates.js';
import closeIcon from '../../assets/images/icons/close-thin.js';
import cheveronIcon from '../../assets/images/icons/chevron.js'
import searchBarCss from '../../assets/styles/search-bar.js';
import selectInputCss from '../../assets/styles/select-input.js';


const mockData = {
  images: {
    closeIcon,
    cheveronIcon
  }
};

const parseData = data => {
  const urlParams = new URLSearchParams(window.location.search);
  
  return {
    ...data,
  };
}

const template = document.createElement("template");
const style = `
  <style>
  ${resetCss}
  ${cssClasses}
  ${searchBarCss}
  ${selectInputCss}

  ${styles}
  </style>
`;
template.innerHTML = /*html*/`
  ${style}
  
  ${htmlTemplate(parseData(mockData))}
`;
class NewMsgPg extends HTMLElement {
  constructor() {
    super();
    this.attachShadow({ mode: "open" });

    this.setBodyClass();
  }

  setBodyClass() {
    document.body.classList.add('linear-gradient');
  }

  connectedCallback() {
    this.shadowRoot.appendChild(template.content.cloneNode(true));
  }
}

customElements.define("new-msg-page", NewMsgPg);
