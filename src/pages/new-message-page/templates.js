export default (data) => `
<div class="page">
  <header>
    <slot name="header"></slot>
  </header>

  <div class="msg-pg-nav">
    <c-breadcrumbs></c-breadcrumbs>
  </div>

  <div class="content-max-and-center">
    <div class="new-msg-container">
      <button class="close">${data.images.closeIcon}</button>
      <div class="subject-and-addressees">
        <div class="form__group field">
          <input type="input" class="form__field" name="name" required />
          <label for="name" class="form__label font-light">Subject:</label>
        </div>

        <div class="msg-type-container">
          <div class="msg-type-select label-and-select-dropdown">
            <span class="font-light">Message type:</span>
            <div class="msg-select-btn">
              <button>
                <span class="font-light">Select</span>
                ${data.images.cheveronIcon}
              </button>

              <div class="msg-type-dropdown-opts menu-box-shadow-on-lg show">
                <ul>
                  <li><button>General Concern</button></li>
                  <li><button>Contact your planner</button></li>
                  <li><button>Planned event/activity</button></li>
                  <li><button>Contact your field service</button></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="msg-send-to-opts">
            <span><button>To</button>/<button>Cc</button>/<button>Bcc</button></span>
          </div>
        </div>
      </div>

      <c-msg-compose-area></c-msg-compose-area>
    </div>
  </div>
</div>
`;