export default `
  .page {
    padding-bottom: 85px;
  }

  .page > header {
    position: absolute;
    z-index: 1;
  }

  .nav-bar {
    position: fixed;
    bottom: 0;
    left: 0;
    width: 100%;
  }

  .col-2,
  .col-3 {
    margin: 0 1.75rem;
  }

  .col-2 {
    margin-bottom: .75rem;
  }

  .col-2 > div {
    margin-bottom: .75rem;
  }

  .col-2 > div:last-child {
    margin-bottom: 0;
  }

  .quick-metrics {
    margin: .75rem 0 .75rem;
  }

  @media (min-width: 1290px) {
    .page {
      padding-bottom: 0;
    }

    .page > header {
      position: relative;
    }

    header {
      position: relative;
    }

    .data-dashboard {
      display: flex;
      align-items: stretch;
      max-width: 1290px;
      margin: 0 auto;
      align-content: stretch;
      justify-content: space-between;
      padding-bottom: 7rem;
      margin-top: 6.25rem;
    }

    .col-1 {
      width: 47.5%;
      max-width: 608px;
    }

    .col-2,
    .col-3 {
      max-width: 292px;
      margin: 0;
      width: 100%;
    }

    .col-2 {
      display: flex;
      flex-wrap: wrap;
      margin-bottom: 0;
      align-items: stretch;
    }

    .col-2 > div {
      margin-bottom: 1.4375rem;
    }

    .delivered {
      order: 0;
    }

    .qc-data.component-bg {
      display: none;
    }

    .alerts {
      order: 1;
    }

    .news {
      order: 2
    }

    .quick-metrics {
      width: 100%;
      margin: 0;
      bottom: 0;
      left: 0;
    }

    .quick-metrics.fixed {
      position: fixed;
    }
  }

`;