import styles from './styles.js';
import cssClasses from '../../assets/styles/classes.js';
import htmlTemplate from './templates.js';

const template = document.createElement("template");
template.innerHTML = /*html*/`
  <style>
    ${cssClasses}
    ${styles}
  </style>
  
  ${htmlTemplate()}
`;

class StoreView extends HTMLElement {
  constructor() {
    super();
    this.attachShadow({ mode: "open" });

    this.setBodyClass();
  }

  connectedCallback() {
    this.shadowRoot.appendChild(template.content.cloneNode(true));
  }

  setBodyClass() {
    document.body.classList.add('fixed-header');
  }
}

customElements.define("store-view", StoreView);
