export default () => `
<div class="page">
  <header>
    <slot name="header"></slot>
  </header>


  <div class="data-dashboard">
    <div class="col-1">
      <slot name="currentorder"></slot>
    </div>
    <div class="col-2">
      <div class="alerts">
        <slot name="alerts"></slot>
      </div>
      
      <div class="delivered">
        <slot name="delivered"></slot>
      </div>

      <div class="news">
        <slot name="news"></slot>
      </div>
    </div>
    <div class="col-3">
      <slot name="flipcontent"></slot>
    </div>
  </div>

  <div class="quick-metrics fixed">
    <slot name="qmetrics"></slot>
    <div class="qc-data component-bg">
      <c-storeinfo></c-storeinfo>
    </div>
  </div>

  <div class="nav-bar">
    <slot name="navbar"></slot>
  </div>
</div>
`;